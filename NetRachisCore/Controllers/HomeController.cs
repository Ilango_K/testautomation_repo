﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NetKathirBaseCore;
using NetKathirBaseCore.Class;
using NetRachisCore.Helpers;
using NetRachisCore.Models;
using NetRachisCore.Models.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NetRachisCore.Controllers
{
    [System.Web.Mvc.Authorize]
    [SessionExpire]
    public class HomeController : BaseController
    {
        #region Declaration 
        NetkathirBaseClass dataAccess = new NetkathirBaseClass();
        List<string> lstColumns = new List<string>();
        List<string> lstValues = new List<string>();
        #endregion
        public IActionResult Index()
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewList = new List<TestCaseResult>();

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Home", "Index", "" };
                InsertErrorAppLog(lstValues, 2);

                string qry = "";
                qry = qry + "SELECT tcr.TestCaseResultId,tcr.TestCaseResultNo,tcr.MatchCondition,tcr.SourceValue,tcr.TargetValue,tcr.MatchResult, tcb.TestCaseBuildName, tct.TestCaseType ";
                qry = qry + "FROM TestCaseResult tcr INNER JOIN TestCaseBuildDevChild tcbdc ON tcbdc.TestCaseBuildDevChildId = tcr.TestcaseBuildDevChildId ";
                qry = qry + "INNER JOIN TestCaseBuildDev tcbd ON tcbd.TestCaseBuildDevId = tcbdc.TestCaseBuildDevId ";
                qry = qry + "INNER JOIN TestCaseBuild tcb ON tcb.TestCaseBuildId = tcbd.TestCaseBuildId ";
                qry = qry + "INNER JOIN TestCaseType tct ON tct.TestCaseTypeId = tcbdc.TestCaseTypeId ";

                Result<DataTable> testResult = dataAccess.SelectByQuery(qry);
                if (testResult.isSuccessful)
                {
                    viewList = (from DataRow row in testResult.responseData.Rows
                                select new TestCaseResult
                                {
                                    TestCaseResultId = Convert.ToInt32(row["TestCaseResultId"]),
                                    TestCaseResultNo = Convert.ToString(row["TestCaseResultNo"]),
                                    MatchCondition = Convert.ToString(row["MatchCondition"]),
                                    SourceValue = Convert.ToString(row["SourceValue"]),
                                    TargetValue = Convert.ToString(row["TargetValue"]),
                                    MatchResult = Convert.ToString(Convert.ToString(row["MatchResult"])),
                                    TestCaseBuildName = Convert.ToString(row["TestCaseBuildName"]),
                                    TestCaseType = Convert.ToString(row["TestCaseType"]),
                                }).ToList();
                }
                else
                {
                    ErrorMessage = testResult.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Home", "Index", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Home", "Index", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(viewList);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
