﻿#region using

 
using System; 
using NetKathirBaseCore;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using NetRachisCore.Helpers;
using NetKathirBaseCore.Class;
using NetRachisCore.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http; 
using Microsoft.AspNetCore.Mvc;
using NetRachisCore.Models.BusinessObjects;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;

#endregion

namespace NetRachisCore.Controllers
{
    [System.Web.Mvc.Authorize]
    [SessionExpire]
    public class TestCaseBuildDevController : BaseController
    {

        #region Declaration
        NetkathirBaseClass dataAccess = new NetkathirBaseClass();
        List<string> lstColumnsAdd = new List<string>() { "ProjectId", "ModuleId", "TestCaseBuildId", "CreatedBy" };
        List<string> lstColumnEdit = new List<string>() { "TestCaseBuildDevId", "ProjectId", "ModuleId", "TestCaseBuildId", "ModifiedBy" };
        List<string> lstColumns = new List<string>();
        List<string> lstValues = new List<string>();
        List<string> parameterNameList = new List<string>();
        List<string> parameterValueList = new List<string>();
        string strTableName = "TestCaseBuildDev";
        string strTableRefTableName = "TestCaseBuildDevChild";
        #endregion

        #region public ActionResult Index()
        /// <summary>
        /// Action/View returs the details of TestCaseBuildDev
        /// </summary> 
        /// <returns>List the details</returns>
        public ActionResult Index()
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewList = new List<TestCaseBuildDev>();

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuildDev", "Index", "" };
                InsertErrorAppLog(lstValues, 2);

                Result<DataTable> spResult = dataAccess.ExecuteSp("USP_SelectAllTestCaseBuildDev");
                if (spResult.isSuccessful)
                {
                    viewList = (from DataRow row in spResult.responseData.Rows
                                select new TestCaseBuildDev
                                {
                                    TestCaseBuildDevId = Convert.ToInt32(row["TestCaseBuildDevId"]),
                                    TestCaseBuildId = Convert.ToInt32(row["TestCaseBuildId"]),
                                    TestCaseBuildName = Convert.ToString(row["TestCaseBuildName"]),
                                    ProjectId = Convert.ToInt32(Convert.ToString(row["ProjectId"])),
                                    ProjectName = Convert.ToString(row["ProjectName"]),
                                    ModuleId = Convert.ToInt32(row["ModuleId"]),
                                    ModuleName = Convert.ToString(row["ModuleName"]),
                                }).ToList();

                }
                else
                {
                    ErrorMessage = spResult.message;
                    //To insert the error log  
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuildDev", "Index", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuildDev", "Index", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(Views.Index, viewList);
        }
        #endregion

        #region public ActionResult AddEdit(int testCaseBuildDevId)
        /// <summary>
        /// Save/Edit the TestCaseBuildDev
        /// </summary>
        /// <param name="testCaseBuildDevId">The TestCaseBuildDev  id</param>
        /// <returns></returns>
        public ActionResult AddEdit(int testCaseBuildDevId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewData = new TestCaseBuildDev();
            viewData.ProjectList = new SelectList("");
            viewData.ModuleList = new SelectList("");
            viewData.TestCaseBuildList = new SelectList("");
            viewData.TestCaseTypeList = new SelectList("");
            viewData.TableList = new SelectList("");
            viewData.ColumnList = new SelectList("");
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuildDev", "AddEdit-Get", "Id=" + testCaseBuildDevId };
                InsertErrorAppLog(lstValues, 2);

                viewData.TestCaseTypeList = GetDropdownRecordsByWhere(Convert.ToString(0), true, "SELECT TestCaseTypeId,TestCaseType FROM testcasetype WHERE IsDeleted = 0");

                if (testCaseBuildDevId > 0)
                {
                    Result<DataTable> result = dataAccess.SelectById(testCaseBuildDevId, lstColumnEdit, strTableName);

                    if (!result.isSuccessful)
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuildDev", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }
                    else
                    {
                        if (result.responseData.Rows.Count > 0)
                        {
                            viewData.TestCaseBuildDevId = Convert.ToInt32(result.responseData.Rows[0]["TestCaseBuildDevId"]);
                            viewData.TestCaseBuildId = Convert.ToInt32(result.responseData.Rows[0]["TestCaseBuildId"]);
                            viewData.ProjectId = Convert.ToInt32(result.responseData.Rows[0]["ProjectId"]);
                            viewData.ModuleId = Convert.ToInt32(result.responseData.Rows[0]["ModuleId"]);
                        }

                        Result<DataTable> testCastResult = dataAccess.SelectByQuery("SELECT TestCaseTypeId, TableName,ColumnName,TargetTableName,TargetColumnName,MatchCondition FROM testcasebuilddevchild WHERE TestCaseBuildDevId = " + testCaseBuildDevId);
                        if (testCastResult.isSuccessful)
                        {
                            viewData.ChildItemRefList = new List<TestCaseBuildDevChild>();
                            viewData.ChildItemRefList = (from DataRow row in testCastResult.responseData.Rows
                                                         select new TestCaseBuildDevChild
                                                         {
                                                             TestCaseTypeId = Convert.ToInt32(row["TestCaseTypeId"]),
                                                             TableName = Convert.ToString(row["TableName"]),
                                                             ColumnName = Convert.ToString(row["ColumnName"]),
                                                             TargetTableName = Convert.ToString(row["TargetTableName"]),
                                                             TargetColumnName = Convert.ToString(row["TargetColumnName"]),
                                                             MatchCondition = Convert.ToString(row["MatchCondition"]),
                                                             TestCaseTypeList = GetDropdownRecordsByWhere(Convert.ToString(row["TestCaseTypeId"]), false, "SELECT TestCaseTypeId,TestCaseType FROM testcasetype WHERE IsDeleted = 0"),
                                                             TableList = GetDropdownRecordsByWhere(Convert.ToString(row["TableName"]), false, "SELECT DISTINCT TableName, TableName FROM filetypeconfiguration WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId + " AND ModuleId = " + viewData.ModuleId),
                                                             ColumnList = GetDropdownRecordsByWhere(Convert.ToString(row["ColumnName"]), false, "SELECT DISTINCT ColumnName, ColumnName FROM filecolumn WHERE IsDeleted = 0 AND FileTypeConfigurationId = (SELECT TOP 1 FileTypeConfigurationId FROM filetypeconfiguration WHERE IsDeleted = 0 AND TableName = '" + Convert.ToString(row["TableName"]) + "' ORDER BY FileTypeConfigurationId DESC)"),
                                                             TargetTableList = GetDropdownRecordsByWhere(Convert.ToString(row["TargetTableName"]), false, "SELECT DISTINCT TableName, TableName FROM filetypeconfiguration WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId + " AND ModuleId = " + viewData.ModuleId),
                                                             TargetColumnList = GetDropdownRecordsByWhere(Convert.ToString(row["TargetColumnName"]), false, "SELECT DISTINCT ColumnName, ColumnName FROM filecolumn WHERE IsDeleted = 0 AND FileTypeConfigurationId = (SELECT TOP 1 FileTypeConfigurationId FROM filetypeconfiguration WHERE IsDeleted = 0 AND TableName = '" + Convert.ToString(row["TargetTableName"]) + "' ORDER BY FileTypeConfigurationId DESC)"),
                                                         }).ToList();
                        }
                        else
                        {
                            ErrorMessage = testCastResult.message;
                            //To insert the error log 
                            lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuildDev", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                            InsertErrorAppLog(lstValues, 1);
                        }
                    }
                }
                viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId);
                viewData.TestCaseBuildList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TestCaseBuildId), true, "SELECT TestCaseBuildId,TestCaseBuildName FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId + " AND ModuleId=" + viewData.ModuleId);
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuildDev", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(Views.AddEdit, viewData);
        }

        #endregion

        #region public ActionResult AddEdit(TestCaseBuildDev viewData)
        /// <summary>
        ///  Save 
        /// </summary>
        /// <param name="viewData">Sending the TestCaseBuildDev values</param>
        /// <returns></returns>
        [HttpPost]
        [System.Web.Mvc.ValidateInput(false)]
        public ActionResult AddEdit(TestCaseBuildDev viewData)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                string objString = JsonConvert.SerializeObject(viewData);
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuildDev", "AddEdit-Post", objString.Replace("\"", "").Replace("{", "").Replace("}", "") };
                InsertErrorAppLog(lstValues, 2);

                int testCaseDevId = viewData.TestCaseBuildDevId;
                if (ModelState.IsValid)
                {
                    var result = new Result<Int64>
                    {
                        isSuccessful = true,
                        message = default(string),
                        responseData = default(Int64)
                    };

                    //Check TestCaseBuildDev already exist
                    string qry = "";
                    if (viewData.TestCaseBuildDevId > 0)
                    {
                        qry = "SELECT TestCaseBuildDevId FROM testcasebuilddev WHERE IsDeleted = 0 AND ProjectId = " + viewData.ProjectId + " AND ModuleId = " + viewData.ModuleId + " AND TestCaseBuildId = " + viewData.TestCaseBuildId + " AND TestCaseBuildDevId <> " + viewData.TestCaseBuildDevId + "";
                    }
                    else
                    {
                        qry = "SELECT TestCaseBuildDevId FROM testcasebuilddev WHERE IsDeleted = 0 AND ProjectId = " + viewData.ProjectId + " AND ModuleId = " + viewData.ModuleId + " AND TestCaseBuildId = " + viewData.TestCaseBuildId + "";
                    }
                    int chkResult = dataAccess.SelectIntValueByQuery(qry);
                    if (chkResult < 0)
                    {
                        ErrorMessage = UserMessages.COMMON_ERROR_MESSAGE;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuildDev", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);

                        viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                        viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId);
                        viewData.TestCaseBuildList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TestCaseBuildId), true, "SELECT TestCaseBuildId,TestCaseBuildName FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId + " AND ModuleId=" + viewData.ModuleId);
                        viewData.TestCaseTypeList = GetDropdownRecordsByWhere(Convert.ToString(0), true, "SELECT TestCaseTypeId,TestCaseType FROM testcasetype WHERE IsDeleted = 0");
                        viewData.TableList = new SelectList("");
                        viewData.ColumnList = new SelectList("");

                        return View(viewData);
                    }
                    else if (chkResult > 0 && chkResult != viewData.TestCaseBuildDevId)
                    {
                        ErrorMessage = "TestCaseBuildDev for same input is already exists. Duplicate entries not allowed.";
                        viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                        viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId);
                        viewData.TestCaseBuildList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TestCaseBuildId), true, "SELECT TestCaseBuildId,TestCaseBuildName FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId + " AND ModuleId=" + viewData.ModuleId);
                        viewData.TestCaseTypeList = GetDropdownRecordsByWhere(Convert.ToString(0), true, "SELECT TestCaseTypeId,TestCaseType FROM testcasetype WHERE IsDeleted = 0");
                        viewData.TableList = new SelectList("");
                        viewData.ColumnList = new SelectList("");

                        return View(viewData);
                    }

                    if (viewData.TestCaseBuildDevId > 0)
                    {
                        lstValues = new List<string>() { Convert.ToString(viewData.TestCaseBuildDevId), Convert.ToString(viewData.ProjectId), Convert.ToString(viewData.ModuleId), Convert.ToString(viewData.TestCaseBuildId), Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Update(lstColumnEdit, lstValues, strTableName);
                    }
                    else
                    {
                        lstValues = new List<string>() { Convert.ToString(viewData.ProjectId), Convert.ToString(viewData.ModuleId), Convert.ToString(viewData.TestCaseBuildId), Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Insert(lstColumnsAdd, lstValues, strTableName);

                        if (result.isSuccessful)
                            testCaseDevId = Convert.ToInt32(result.responseData);
                    }

                    CanClearMessage = false;
                    if (result.isSuccessful)
                    {

                        //To save the child records
                        if (testCaseDevId > 0)
                        {
                            lstColumns = new List<string>() { "TestCaseBuildDevId" };
                            lstValues = new List<string>() { Convert.ToString(testCaseDevId) };
                            Result<Int64> testCaseRefDelete = dataAccess.Delete(false, lstColumns, lstValues, strTableRefTableName);
                            if (!testCaseRefDelete.isSuccessful)
                            {
                                ErrorMessage = testCaseRefDelete.message;
                                return RedirectToAction(Actions.Index);
                            }

                            viewData.ChildItemRefList = new List<TestCaseBuildDevChild>();
                            if (!string.IsNullOrEmpty(viewData.strChildItemRefList))
                            {
                                viewData.ChildItemRefList = new List<TestCaseBuildDevChild>(Newtonsoft.Json.JsonConvert.DeserializeObject<TestCaseBuildDevChild[]>(viewData.strChildItemRefList));
                            }

                            foreach (var item in viewData.ChildItemRefList)
                            {
                                lstColumns = new List<string>() { "TestCaseBuildDevId", "TestCaseTypeId", "TableName", "ColumnName", "TargetTableName", "TargetColumnName", "MatchCondition" };
                                lstValues = new List<string>() { Convert.ToString(testCaseDevId), Convert.ToString(item.TestCaseTypeId), item.TableName, item.ColumnName, item.TargetTableName, item.TargetColumnName, item.MatchCondition };
                                result = dataAccess.Insert(lstColumns, lstValues, strTableRefTableName);
                                if (!result.isSuccessful)
                                {
                                    ErrorMessage = result.message;
                                    return RedirectToAction(Actions.Index);
                                }
                            }
                        }

                        if (viewData.TestCaseBuildDevId > 0)
                        {
                            SuccessMessage = UserMessages.UPDATE_SUCCESS;
                        }
                        else
                        {
                            SuccessMessage = UserMessages.SAVE_SUCCESS;
                        }
                    }
                    else
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuildDev", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }

                    return RedirectToAction(Actions.Index);
                }

                var modalStateError = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                ErrorMessage = modalStateError.ToString();
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuildDev", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }
            viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
            viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId);
            viewData.TestCaseBuildList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TestCaseBuildId), true, "SELECT TestCaseBuildId,TestCaseBuildName FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId + " AND ModuleId=" + viewData.ModuleId);
            viewData.TestCaseTypeList = GetDropdownRecordsByWhere(Convert.ToString(0), true, "SELECT TestCaseTypeId,TestCaseType FROM testcasetype WHERE IsDeleted = 0");
            viewData.TableList = new SelectList("");
            viewData.ColumnList = new SelectList("");

            return View(viewData);
        }
        #endregion

        #region public ActionResult Delete(int testCaseBuildDevId)
        /// <summary>
        /// Delete the TestCaseBuildDev
        /// </summary>
        /// <param name="testCaseBuildDevId">The TestCaseBuildDev Id</param>
        /// <returns></returns>
        public ActionResult Delete(int testCaseBuildDevId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuildDev", "Delete", "Id=" + testCaseBuildDevId };
                InsertErrorAppLog(lstValues, 2);

                //Soft Delete
                lstColumns = new List<string>() { "TestCaseBuildDevId", "IsDeleted", "ModifiedBy" };
                lstValues = new List<string>() { Convert.ToString(testCaseBuildDevId), "1", Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                Result<Int64> result = dataAccess.Delete(true, lstColumns, lstValues, strTableName);

                CanClearMessage = false;
                if (!result.isSuccessful)
                {
                    ErrorMessage = result.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuildDev", "Delete", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
                else
                {
                    SuccessMessage = UserMessages.DELETE_SUCCESS;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuildDev", "Delete", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return RedirectToAction(Actions.Index);
        }

        #endregion

        #region public JsonResult GetModulesByProjectId(int projectId)
        /// <summary>
        /// To select the modules based on the selected project Id
        /// </summary>
        /// <param name="projectId">The selected project</param>
        /// <returns></returns>
        [System.Web.Mvc.AllowAnonymous]
        [HttpPost]
        public System.Web.Mvc.JsonResult GetModulesByProjectId(int projectId)
        {
            System.Web.Mvc.JsonResult res = new System.Web.Mvc.JsonResult();

            Result<DataTable> result = dataAccess.SelectByQuery("SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + projectId);
            if (result.isSuccessful)
            {
                Dictionary<string, string> forms = result.responseData.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());

                res.Data = forms;
            }
            res.JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet;
            return res;
        }
        #endregion

        #region public JsonResult GetTestCaseBuildByFilter(int projectId, int moduleId)
        /// <summary>
        /// To select the test case build based on the selected project and module Id
        /// </summary>
        /// <param name="projectId">The selected project</param>
        /// <param name="moduleId">The selected module</param>
        /// <returns></returns>
        [System.Web.Mvc.AllowAnonymous]
        [HttpPost]
        public System.Web.Mvc.JsonResult GetTestCaseBuildByFilter(int projectId, int moduleId)
        {
            System.Web.Mvc.JsonResult res = new System.Web.Mvc.JsonResult();

            Result<DataTable> result = dataAccess.SelectByQuery("SELECT TestCaseBuildId,TestCaseBuildName FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId=" + projectId + " AND ModuleId = " + moduleId);
            if (result.isSuccessful)
            {
                Dictionary<string, string> forms = result.responseData.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());

                res.Data = forms;
            }
            res.JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet;
            return res;
        }
        #endregion

        #region public JsonResult GetTables(int projectId, int moduleId)
        /// <summary>
        /// To select the tables based on the selected project and module Id
        /// </summary>
        /// <param name="projectId">The selected project</param>
        /// <param name="moduleId">The selected module</param>
        /// <returns></returns>
        [System.Web.Mvc.AllowAnonymous]
        [HttpPost]
        public System.Web.Mvc.JsonResult GetTables(int projectId, int moduleId)
        {
            System.Web.Mvc.JsonResult res = new System.Web.Mvc.JsonResult();

            Result<DataTable> result = dataAccess.SelectByQuery("SELECT DISTINCT TableName, TableName FROM filetypeconfiguration WHERE IsDeleted = 0 AND ProjectId=" + projectId + " AND ModuleId = " + moduleId);
            if (result.isSuccessful)
            {
                Dictionary<string, string> forms = result.responseData.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());

                res.Data = forms;
            }
            res.JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet;
            return res;
        }
        #endregion

        #region public JsonResult GetColumnsByTable(string tableName)
        /// <summary>
        /// To select the columns based on the selected table
        /// </summary>
        /// <param name="tableName">The selected tableName</param> 
        /// <returns></returns>
        [System.Web.Mvc.AllowAnonymous]
        [HttpPost]
        public System.Web.Mvc.JsonResult GetColumnsByTable(string tableName)
        {
            System.Web.Mvc.JsonResult res = new System.Web.Mvc.JsonResult();

            Result<DataTable> result = dataAccess.SelectByQuery("SELECT DISTINCT ColumnName, ColumnName FROM filecolumn WHERE IsDeleted = 0 AND FileTypeConfigurationId = (SELECT TOP 1 FileTypeConfigurationId FROM filetypeconfiguration WHERE IsDeleted = 0 AND TableName = '" + tableName + "' ORDER BY FileTypeConfigurationId DESC)");
            if (result.isSuccessful)
            {
                Dictionary<string, string> forms = result.responseData.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());

                res.Data = forms;
            }
            res.JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet;
            return res;
        }
        #endregion 

        #region public ActionResult GetDetailsForClone(int projectId,int moduleId, int testCaseId)
        /// <summary>
        /// To get the test case details based on the selected creteria
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="moduleId"></param>
        /// <param name="testCaseId"></param>
        /// <returns></returns>   
        public ActionResult GetDetailsForClone(int projectId, int moduleId, int testCaseId)
        {
            var viewData = new List<TestCaseBuildDevChild>();
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuildDev", "GetDetailsForClone", "ProjectId=" + projectId + "&ModuleId=" + moduleId + "&TestCaseId=" + testCaseId };
                InsertErrorAppLog(lstValues, 2);


                Result<DataTable> testCastResult = dataAccess.SelectByQuery("SELECT TestCaseTypeId, TableName,ColumnName,TargetTableName,TargetColumnName,MatchCondition FROM testcasebuilddevchild WHERE TestCaseBuildDevId = (SELECT TestCaseBuildDevId FROM testcasebuilddev WHERE IsDeleted = 0 AND ProjectId = " + projectId + " AND ModuleId = " + moduleId + " AND TestCaseBuildId = " + testCaseId + " ORDER BY TestCaseBuildDevId DESC LIMIT 1)");
                if (testCastResult.isSuccessful)
                {

                    viewData = (from DataRow row in testCastResult.responseData.Rows
                                select new TestCaseBuildDevChild
                                {
                                    TestCaseTypeId = Convert.ToInt32(row["TestCaseTypeId"]),
                                    TableName = Convert.ToString(row["TableName"]),
                                    ColumnName = Convert.ToString(row["ColumnName"]),
                                    TargetTableName = Convert.ToString(row["TargetTableName"]),
                                    TargetColumnName = Convert.ToString(row["TargetColumnName"]),
                                    MatchCondition = Convert.ToString(row["MatchCondition"]),
                                    TestCaseTypeList = GetDropdownRecordsByWhere(Convert.ToString(row["TestCaseTypeId"]), false, "SELECT TestCaseTypeId,TestCaseType FROM testcasetype WHERE IsDeleted = 0"),
                                    TableList = GetDropdownRecordsByWhere(Convert.ToString(row["TableName"]), false, "SELECT DISTINCT TableName, TableName FROM filetypeconfiguration WHERE IsDeleted = 0 AND ProjectId=" + projectId + " AND ModuleId = " + moduleId),
                                    ColumnList = GetDropdownRecordsByWhere(Convert.ToString(row["ColumnName"]), false, "SELECT DISTINCT ColumnName, ColumnName FROM filecolumn WHERE IsDeleted = 0 AND FileTypeConfigurationId = (SELECT FileTypeConfigurationId FROM filetypeconfiguration WHERE IsDeleted = 0 AND TableName = '" + Convert.ToString(row["TableName"]) + "' ORDER BY FileTypeConfigurationId DESC LIMIT 1)"),
                                    TargetTableList = GetDropdownRecordsByWhere(Convert.ToString(row["TargetTableName"]), false, "SELECT DISTINCT TableName, TableName FROM filetypeconfiguration WHERE IsDeleted = 0 AND ProjectId=" + projectId + " AND ModuleId = " + moduleId),
                                    TargetColumnList = GetDropdownRecordsByWhere(Convert.ToString(row["TargetColumnName"]), false, "SELECT DISTINCT ColumnName, ColumnName FROM filecolumn WHERE IsDeleted = 0 AND FileTypeConfigurationId = (SELECT FileTypeConfigurationId FROM filetypeconfiguration WHERE IsDeleted = 0 AND TableName = '" + Convert.ToString(row["TargetTableName"]) + "' ORDER BY FileTypeConfigurationId DESC LIMIT 1)"),
                                }).ToList();
                }
                else
                {
                    ErrorMessage = testCastResult.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuildDev", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuildDev", "GetDetailsForClone", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }
            return PartialView("_TestCaseDetail", viewData);
        }
        #endregion

    }
}
