﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NetKathirBaseCore;
using NetRachisCore.Models.BusinessObjects;
using Microsoft.AspNetCore.Authorization;
using NetRachisCore.Helpers;
using NetKathirBaseCore.Class;
using System.Data;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using NetRachisCore.Models;
using System.Configuration;

namespace NetRachisCore.Controllers
{
    public class LoginController : BaseController
    {
        #region Declaration
        NetkathirBaseClass dataAccess = new NetkathirBaseClass();
        List<string> parameterNameList = new List<string>();
        List<string> parameterValueList = new List<string>();
        List<string> lstColumns = new List<string>();
        List<string> lstValues = new List<string>();
        private string strTableName = "AppUser";
        #endregion

        private readonly IMailService mailService;
        public LoginController(IMailService mailService)
        {
            this.mailService = mailService;
        }

        #region public ActionResult UserLogin(string returnUrl)
        /// <summary>
        /// User login method
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        public ActionResult UserLogin(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;
            var UserLogin = new User();
            try
            {
                bool isValid = CheckAuthLogOn();
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                bool isSessionValid = sessionHelper.IsSessionValid();

                if (isValid && isSessionValid)
                {
                    return RedirectToAction(Actions.Index, Helpers.Controllers.Home);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnUserId()), "Login", "UserLogin-Get", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }
            return View(UserLogin);
        }
        #endregion

        #region public ActionResult UserLogin(User user,string returnUrl)
        /// <summary>
        /// Action/View returs the details of Login
        /// </summary> 
        /// <returns>List the details</returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult UserLogin(User user, string returnUrl)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = false;

            try
            { 
                if (user == null) throw new ArgumentNullException("user");

                NetkathirBaseClass dataAccess = new NetkathirBaseClass();
                user.Password = CommonHelper.Encryption(user.Password);
                var loggedUser = new User();

                Result<DataTable> result = dataAccess.SelectByQuery("SELECT * FROM " + strTableName + " WHERE UserEmailId = '" + user.EmailId + "' AND UserPassword ='" + user.Password + "' AND IsDeleted=0");
                if (result.isSuccessful)
                {
                    if (result.responseData.Rows.Count > 0)
                    {
                        loggedUser.UserId = Convert.ToInt32(result.responseData.Rows[0]["AppUserId"]);
                        loggedUser.UserName = Convert.ToString(result.responseData.Rows[0]["UserName"]);
                        loggedUser.EmailId = Convert.ToString(result.responseData.Rows[0]["UserEmailId"]);
                        loggedUser.ActiveStatus = Convert.ToInt16(result.responseData.Rows[0]["ActiveStatus"]) == 1 ? true : false;

                        if (loggedUser.ActiveStatus)
                        {
                            loggedUser.MenuAccessList = new List<Access>();
                            loggedUser.FormAccessList = new List<Access>();

                            List<string> parameterNameList = new List<string>() { "userId" };
                            List<string> parameterValueList = new List<string>() { loggedUser.UserId.ToString() };
                            Result<DataTable> menuresult = dataAccess.ExecuteSpWithParameter("USP_SelectAccessMenuByUser", parameterNameList, parameterValueList);
                            if (menuresult.isSuccessful)
                            {
                                List<Access> menuList = new List<Access>();
                                foreach (DataRow row in menuresult.responseData.Rows)
                                {
                                    var menuAccess = new Access();
                                    menuAccess.Name = row[0].ToString();
                                    menuAccess.IsWriteAccess = row[1].ToString();
                                    menuList.Add(menuAccess);
                                }
                                loggedUser.MenuAccessList = menuList;
                            }

                            Result<DataTable> formresult = dataAccess.ExecuteSpWithParameter("USP_SelectAccessFormByUser", parameterNameList, parameterValueList);
                            if (formresult.isSuccessful)
                            {
                                List<Access> formList = new List<Access>();
                                foreach (DataRow row in formresult.responseData.Rows)
                                {
                                    var formAccess = new Access();
                                    formAccess.Name = row[0].ToString();
                                    formAccess.IsWriteAccess = row[1].ToString();
                                    formList.Add(formAccess);
                                }
                                loggedUser.FormAccessList = formList;
                            }

                            var sessionHelper = new SessionHelper(new HttpContextAccessor());
                            sessionHelper.ClearSessionStack();
                            sessionHelper.NewSession(); 
                            sessionHelper.SetObjectAsJson("Session_UserInfo", loggedUser);
                            //Set cookie for remember me
                            CommonHelper.SetCookieInfo(user.RememberMe, user.EmailId, user.Password);
                            //Manually set the authentication cookie
                            //FormsAuthentication.SetAuthCookie(loggedUser.EmailId, false);

                            //To insert the application log  
                            string objString = JsonConvert.SerializeObject(user); 
                            lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnUserId()), "Login", "UserLogin-Post", objString.Replace("\"", "").Replace("{", "").Replace("}", "") };
                            InsertErrorAppLog(lstValues, 2);

                            if (!String.IsNullOrEmpty(returnUrl))
                            {
                                return Redirect(returnUrl);
                            }
                            else
                            {
                                return RedirectToAction(Actions.Index, Helpers.Controllers.Home);
                            }
                        }
                        else
                        {
                            ErrorMessage = UserMessages.USER_INACTIVE_STATUS;
                        }
                    }
                    else
                    {
                        ErrorMessage = UserMessages.USER_NOT_EXIST;
                    }
                }
                else
                {
                    ErrorMessage = result.message;
                    //To insert the error log
                    var sessionHelper = new SessionHelper(new HttpContextAccessor());
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnUserId()), "Login", "UserLogin-Post", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnUserId()), "Login", "UserLogin-Post", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(Views.UserLogin, user);
        }
        #endregion

        #region public ActionResult Logout
        /// <summary>
        /// Logout
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            try
            {
                ClearMessage();
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                //To insert the application log
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnUserId()), "Login", "Logout", "" };
                InsertErrorAppLog(lstValues, 2);

                CommonHelper.SetCookieInfo(false, string.Empty, string.Empty);
                sessionHelper.ClearSessionStack();
                HttpContext.Session.Clear(); 
                //FormsAuthentication.SignOut();
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnUserId()), "Login", "Logout", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }
            return RedirectToAction(Actions.UserLogin, Helpers.Controllers.Login);
        }
        #endregion

        #region public ActionResult ForgotPassword()

        /// <summary>
        /// Forgot Password.
        /// </summary>
        /// <returns></returns>
        public ActionResult ForgotPassword()
        {
            ClearMessage();
            return View();
        }

        #endregion

        #region public ActionResult ForgotPassword(User user)

        /// <summary>
        /// Forgot password post method.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        [System.Web.Mvc.ValidateInput(false)]
        [HttpPost]
        public ActionResult ForgotPassword(User user)
        {
            ClearMessage();
            CanClearMessage = false;
            int userId = 0;
            try
            {
                if (ModelState.IsValid)
                {
                    if (user == null) throw new ArgumentNullException("user");

                    user.Password = CommonHelper.Encryption(CreateRandomNumber(7));

                    Result<DataTable> result = dataAccess.SelectByQuery("SELECT * FROM " + strTableName + " WHERE UserEmailId = '" + user.EmailId + "' AND IsDeleted=0");
                    if (result.isSuccessful)
                    {
                        if (result.responseData.Rows.Count > 0)
                        {
                            userId = Convert.ToInt32(result.responseData.Rows[0]["AppUserId"]);

                            if (Convert.ToInt16(result.responseData.Rows[0]["ActiveStatus"]) == 0)
                            {
                                ErrorMessage = UserMessages.USER_INACTIVE_STATUS;
                                return View();
                            }

                            //To update the new password into the database
                            lstColumns = new List<string>() { "AppUserId", "UserPassword" };
                            lstValues = new List<string>() { Convert.ToString(userId), user.Password };
                            Result<Int64> updateUserResult = dataAccess.Update(lstColumns, lstValues, strTableName);
                            if (!updateUserResult.isSuccessful)
                            {
                                ErrorMessage = result.message;
                                //To insert the error log
                                lstValues = new List<string>() { Convert.ToString(userId), "Login", "ForgotPassword-Post", ErrorMessage.Replace("'", "\"") };
                                InsertErrorAppLog(lstValues, 1);
                                return View();
                            }
                            else
                            {
                                // Send email to the user 

                                var email = new MailRequest
                                {
                                    EmailTo = user.EmailId,
                                    EmailFrom = ConfigurationManager.AppSettings["EmailFrom"],
                                    EmailSubject = ConfigurationManager.AppSettings["RecoverPasswordSubject"],
                                    //Body = "",
                                    Body = "Hi,<br /><br /> We would like to inform that your password has been reset as per the request and your new password is as below.<br /> New password: " + CommonHelper.Decryption(user.Password) + ".<br /><br /> Thanks,<br /> Support Team",
                                    ViewName = "RecoverPasswordMail",
                                    Value = CommonHelper.Decryption(user.Password),
                                };
                                try
                                { 
                                    mailService.SendEmailAsync(email); 
                                    SuccessMessage = UserMessages.RECOVER_PASSWORD_SUCCESS;
                                }
                                catch (Exception ex)
                                {
                                    ErrorMessage = "Error occured while sending email." + ex.Message;
                                    //To insert the error log
                                    lstValues = new List<string>() { Convert.ToString(userId), "Login", "ForgotPassword-Post", ErrorMessage.Replace("'", "\"") };
                                    InsertErrorAppLog(lstValues, 1);
                                    return View();
                                }

                                //To insert the application log
                                string objString = JsonConvert.SerializeObject(user);
                                lstValues = new List<string>() { Convert.ToString(userId), "Login", "ForgotPassword-Post", objString.Replace("\"", "").Replace("{", "").Replace("}", "") };
                                InsertErrorAppLog(lstValues, 2);
                            }

                        }
                        else
                        {
                            ErrorMessage = UserMessages.EMAILID_NOT_EXISTS;
                            return View();
                        }
                    }
                    else
                    {
                        ErrorMessage = result.message;
                        //To insert the error log
                        lstValues = new List<string>() { Convert.ToString(userId), "Login", "ForgotPassword-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                        return View();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log
                lstValues = new List<string>() { Convert.ToString(userId), "Login", "ForgotPassword-Post", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }
            return RedirectToAction(Actions.UserLogin, Helpers.Controllers.Login);
        }

        #endregion

        #region public ActionResult ChangePassword()

        /// <summary>
        /// Change Password.
        /// </summary>
        /// <returns></returns>  
        public ActionResult ChangePassword()
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            return View(new ChangePasswordModel());
        }

        #endregion

        #region public ActionResult ChangePassword(ChangePasswordModel changePasswordModel)

        /// <summary>
        /// Change password post method.
        /// </summary>
        /// <param name="changePasswordModel">The changePasswordModel.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel changePasswordModel)
        {
            ClearMessage();
            CanClearMessage = false;
            int userId = 0;
            try
            {
                if (ModelState.IsValid)
                {
                    if (changePasswordModel == null) throw new ArgumentNullException("changePasswordModel");

                    if (changePasswordModel.NewPassword != changePasswordModel.ConfirmPassword)
                    {
                        ErrorMessage = UserMessages.USER_CONFIRMPWD_MISMATCH;
                        return View(changePasswordModel);
                    }

                    string currentPassword = string.Empty;

                    Result<DataTable> result = dataAccess.SelectByQuery("SELECT * FROM " + strTableName + " WHERE UserEmailId = '" + changePasswordModel.EmailId + "' AND IsDeleted=0");
                    if (result.isSuccessful)
                    {
                        if (result.responseData.Rows.Count > 0)
                        {
                            userId = Convert.ToInt32(result.responseData.Rows[0]["AppUserId"]);
                            currentPassword = CommonHelper.Decryption(Convert.ToString(result.responseData.Rows[0]["UserPassword"]));

                            if (Convert.ToInt16(result.responseData.Rows[0]["ActiveStatus"]) == 0)
                            {
                                ErrorMessage = UserMessages.USER_INACTIVE_STATUS;
                                return View(changePasswordModel);
                            }

                            if (changePasswordModel.OldPassword != currentPassword)
                            {
                                ErrorMessage = UserMessages.USER_INCORRECT_OLD_PWD;
                                return View(changePasswordModel);
                            }
                            else
                            {
                                if (changePasswordModel.NewPassword == changePasswordModel.OldPassword)
                                {
                                    ErrorMessage = UserMessages.USER_PWD_NEWPWD_SAME;
                                    return View(changePasswordModel);
                                }

                                //To update the new password into the database
                                lstColumns = new List<string>() { "AppUserId", "UserPassword" };
                                lstValues = new List<string>() { Convert.ToString(userId), CommonHelper.Encryption(changePasswordModel.NewPassword) };
                                Result<Int64> updateUserResult = dataAccess.Update(lstColumns, lstValues, strTableName);
                                if (!updateUserResult.isSuccessful)
                                {
                                    ErrorMessage = result.message;
                                    //To insert the error log
                                    lstValues = new List<string>() { Convert.ToString(userId), "Login", "ChangePassword-Post", ErrorMessage.Replace("'", "\"") };
                                    InsertErrorAppLog(lstValues, 1);
                                    return View(changePasswordModel);
                                }
                                else
                                {
                                    // Send email to the user 

                                    var email = new MailRequest
                                    {
                                        EmailTo = changePasswordModel.EmailId,
                                        EmailFrom = ConfigurationManager.AppSettings["EmailFrom"],
                                        EmailSubject = ConfigurationManager.AppSettings["ResetPasswordSubject"],
                                        //Body = "",
                                        Body = "Hi,<br /><br /> We would like to inform that your password has been changed as per the request and your new password is as below.<br /> New password: " + changePasswordModel.NewPassword + ".<br /><br /> Thanks,<br /> Support Team",
                                        ViewName = "RecoverPasswordMail",
                                        Value = changePasswordModel.NewPassword,
                                    };
                                    try
                                    {
                                        mailService.SendEmailAsync(email);
                                        SuccessMessage = UserMessages.RESET_PASSWORD_SUCCESS;
                                    }
                                    catch (Exception ex)
                                    {
                                        ErrorMessage = "Error occured while sending email." + ex.Message;
                                        //To insert the error log
                                        lstValues = new List<string>() { Convert.ToString(userId), "Login", "ChangePassword-Post", ErrorMessage.Replace("'", "\"") };
                                        InsertErrorAppLog(lstValues, 1);
                                        return View(changePasswordModel);
                                    }

                                    //To insert the application log
                                    string objString = JsonConvert.SerializeObject(changePasswordModel);
                                    lstValues = new List<string>() { Convert.ToString(userId), "Login", "ChangePassword-Post", objString.Replace("\"", "").Replace("{", "").Replace("}", "") };
                                    InsertErrorAppLog(lstValues, 2);
                                }
                            }
                        }
                        else
                        {
                            ErrorMessage = UserMessages.EMAILID_NOT_EXISTS;
                            return View(changePasswordModel);
                        }
                    }
                    else
                    {
                        ErrorMessage = result.message;
                        //To insert the error log
                        lstValues = new List<string>() { Convert.ToString(userId), "Login", "ChangePassword-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                        return View(changePasswordModel);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log
                lstValues = new List<string>() { Convert.ToString(userId), "Login", "ChangePassword-Post", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return RedirectToAction(Actions.UserLogin, Helpers.Controllers.Login);
        }

        #endregion

        #region private bool CheckAuthLogOn()

        /// <summary>
        /// Checks the auth login.
        /// </summary>
        /// <returns>return bool Value.</returns>
        private bool CheckAuthLogOn()
        {
            bool isValid = false; 
            var obj = new CookieHelper(new HttpContextAccessor());
            try
            {
                if (obj.GetCookie("FFLoggedIn") == "Y")
                {
                    string email = obj.GetCookie("FFEmail");
                    string password = obj.GetCookie("FFPassword");
                    var loggedUser = new User();
                    Result<DataTable> result = dataAccess.SelectByQuery("SELECT * FROM " + strTableName + " WHERE UserEmailId = '" + email + "' AND UserPassword ='" + password + "' AND ActiveStatus=1 AND IsDeleted=0");
                    if (result.isSuccessful)
                    {
                        if (result.responseData.Rows.Count > 0)
                        {
                            loggedUser.UserId = Convert.ToInt32(result.responseData.Rows[0]["AppUserId"]);
                            loggedUser.UserName = Convert.ToString(result.responseData.Rows[0]["UserName"]);
                            loggedUser.EmailId = Convert.ToString(result.responseData.Rows[0]["UserEmailId"]);

                            loggedUser.MenuAccessList = new List<Access>();
                            loggedUser.FormAccessList = new List<Access>();

                            List<string> parameterNameList = new List<string>() { "userId" };
                            List<string> parameterValueList = new List<string>() { loggedUser.UserId.ToString() };
                            Result<DataTable> menuresult = dataAccess.ExecuteSpWithParameter("USP_SelectAccessMenuByUser", parameterNameList, parameterValueList);
                            if (menuresult.isSuccessful)
                            {
                                List<Access> menuList = new List<Access>();
                                foreach (DataRow row in menuresult.responseData.Rows)
                                {
                                    var menuAccess = new Access();
                                    menuAccess.Name = row[0].ToString();
                                    menuAccess.IsWriteAccess = row[1].ToString();
                                    menuList.Add(menuAccess);
                                }
                                loggedUser.MenuAccessList = menuList;
                            }

                            Result<DataTable> formresult = dataAccess.ExecuteSpWithParameter("USP_SelectAccessFormByUser", parameterNameList, parameterValueList);
                            if (formresult.isSuccessful)
                            {
                                List<Access> formList = new List<Access>();
                                foreach (DataRow row in formresult.responseData.Rows)
                                {
                                    var formAccess = new Access();
                                    formAccess.Name = row[0].ToString();
                                    formAccess.IsWriteAccess = row[1].ToString();
                                    formList.Add(formAccess);
                                }
                                loggedUser.FormAccessList = formList;
                            }

                            var sessionHelper = new SessionHelper(new HttpContextAccessor());
                            sessionHelper.ClearSessionStack();
                            sessionHelper.NewSession(); 
                            sessionHelper.SetObjectAsJson("Session_UserInfo", loggedUser);
                            //User user = SessionHelper.GetObjectFromJson<User>(HttpContext.Session, "Session_UserInfo");

                            //Set cookie for remember me
                            CommonHelper.SetCookieInfo(true, email, password);
                            //Manually set the authentication cookie
                            //FormsAuthentication.SetAuthCookie(loggedUser.EmailId, false);
                            isValid = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log
                lstValues = new List<string>() { Convert.ToString(0), "Login", "CheckAuthLogOn", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }
            return isValid;
        }

        #endregion

    }
}
