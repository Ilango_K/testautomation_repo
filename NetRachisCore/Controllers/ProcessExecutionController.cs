﻿#region using

 
using System; 
using NetKathirBaseCore;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using NetRachisCore.Helpers;
using NetKathirBaseCore.Class;
using NetRachisCore.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http; 
using Microsoft.AspNetCore.Mvc;
using NetRachisCore.Models.BusinessObjects;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;

#endregion

namespace NetRachisCore.Controllers
{
    [System.Web.Mvc.Authorize]
    [SessionExpire]
    public class ProcessExecutionController : BaseController
    {

        #region Declaration

        NetkathirBaseClass dataAccess = new NetkathirBaseClass();
        List<string> lstColumnsAdd = new List<string>() { "ProjectId", "ModuleId", "CreatedBy" };
        List<string> lstColumnEdit = new List<string>() { "ProcessExecutionId", "ProjectId", "ModuleId", "ModifiedBy" };
        List<string> lstColumns = new List<string>();
        List<string> lstValues = new List<string>();
        List<string> parameterNameList = new List<string>();
        List<string> parameterValueList = new List<string>();
        string strTableName = "ProcessExecution";
        string strTableRefTableName = "ProcessExecutionChild";

        List<string> lstColTestResult = new List<string>() { "TestCaseResultNo", "TestcaseBuildDevChildId", "MatchCondition", "SourceValue", "TargetValue", "MatchResult" };

        #endregion

        #region public ActionResult Index()
        /// <summary>
        /// Action/View returs the details of ProcessExecution
        /// </summary> 
        /// <returns>List the details</returns>
        public ActionResult Index()
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewList = new List<ProcessExecution>();

            try
            {
                //To insert the application log
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "Index", "" };
                InsertErrorAppLog(lstValues, 2);

                Result<DataTable> spResult = dataAccess.ExecuteSp("USP_SelectAllProcessExecution");
                if (spResult.isSuccessful)
                {
                    viewList = (from DataRow row in spResult.responseData.Rows
                                select new ProcessExecution
                                {
                                    ProcessExecutionId = Convert.ToInt32(row["ProcessExecutionId"]),
                                    ProjectId = Convert.ToInt32(Convert.ToString(row["ProjectId"])),
                                    ProjectName = Convert.ToString(row["ProjectName"]),
                                    ModuleId = Convert.ToInt32(row["ModuleId"]),
                                    ModuleName = Convert.ToString(row["ModuleName"]),
                                }).ToList();

                }
                else
                {
                    ErrorMessage = spResult.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "Index", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "Index", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(Views.Index, viewList);
        }
        #endregion

        #region public ActionResult AddEdit(int processExecutionId)
        /// <summary>
        /// Save/Edit the ProcessExecution
        /// </summary>
        /// <param name="processExecutionId">The ProcessExecution  id</param>
        /// <returns></returns>
        public ActionResult AddEdit(int processExecutionId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewData = new ProcessExecution();
            viewData.ProjectList = new SelectList("");
            viewData.ModuleList = new SelectList("");
            viewData.TestCaseBuildList = new SelectList("");
            viewData.CommandList = new SelectList("");
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "AddEdit-Get", "Id=" + processExecutionId };
                InsertErrorAppLog(lstValues, 2);

                if (processExecutionId > 0)
                {
                    Result<DataTable> result = dataAccess.SelectById(processExecutionId, lstColumnEdit, strTableName);

                    if (!result.isSuccessful)
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }
                    else
                    {
                        if (result.responseData.Rows.Count > 0)
                        {
                            viewData.ProcessExecutionId = Convert.ToInt32(result.responseData.Rows[0]["ProcessExecutionId"]);
                            viewData.ProjectId = Convert.ToInt32(result.responseData.Rows[0]["ProjectId"]);
                            viewData.ModuleId = Convert.ToInt32(result.responseData.Rows[0]["ModuleId"]);
                        }

                        Result<DataTable> childResult = dataAccess.SelectByQuery("SELECT TestCaseBuildId, Command, ProcessStatus FROM processexecutionchild WHERE ProcessExecutionId = " + processExecutionId);
                        if (childResult.isSuccessful)
                        {
                            viewData.ProcessExecutionChildRefList = new List<ProcessExecutionChild>();
                            viewData.ProcessExecutionChildRefList = (from DataRow row in childResult.responseData.Rows
                                                                     select new ProcessExecutionChild
                                                                     {
                                                                         TestCaseBuildId = Convert.ToInt32(row["TestCaseBuildId"]),
                                                                         Command = Convert.ToString(row["Command"]),
                                                                         ProcessStatus = Convert.ToString(row["ProcessStatus"]),
                                                                         TestCaseBuildList = GetDropdownRecordsByWhere(Convert.ToString(row["TestCaseBuildId"]), true, "SELECT TestCaseBuildId,TestCaseBuildName FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId + " AND ModuleId=" + viewData.ModuleId),
                                                                         CommandList = GetDropdownRecordsByWhere(Convert.ToString(row["Command"]), false, "SELECT DISTINCT Command, Command FROM commands WHERE IsDeleted = 0"),
                                                                     }).ToList();
                        }
                        else
                        {
                            ErrorMessage = childResult.message;
                            //To insert the error log 
                            lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                            InsertErrorAppLog(lstValues, 1);
                        }
                    }
                }
                viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId);
                viewData.TestCaseBuildList = GetDropdownRecordsByWhere("", true, "SELECT TestCaseBuildId,TestCaseBuildName FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId + " AND ModuleId=" + viewData.ModuleId);
                viewData.CommandList = GetDropdownRecordsByWhere("", false, "SELECT DISTINCT Command, Command FROM commands WHERE IsDeleted = 0");
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(Views.AddEdit, viewData);
        }

        #endregion

        #region public ActionResult AddEdit(ProcessExecution viewData)
        /// <summary>
        ///  Save 
        /// </summary>
        /// <param name="viewData">Sending the ProcessExecution values</param>
        /// <returns></returns>
        [HttpPost]
        [System.Web.Mvc.ValidateInput(false)]
        public ActionResult AddEdit(ProcessExecution viewData)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                string objString = JsonConvert.SerializeObject(viewData);
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "AddEdit-Post", objString.Replace("\"", "").Replace("{", "").Replace("}", "") };
                InsertErrorAppLog(lstValues, 2);

                int processExecId = viewData.ProcessExecutionId;
                if (ModelState.IsValid)
                {
                    var result = new Result<Int64>
                    {
                        isSuccessful = true,
                        message = default(string),
                        responseData = default(Int64)
                    };

                    //Check ProcessExecution already exist
                    string qry = "";
                    if (viewData.ProcessExecutionId > 0)
                    {
                        qry = "SELECT ProcessExecutionId FROM processexecution WHERE IsDeleted = 0 AND ProjectId = " + viewData.ProjectId + " AND ModuleId = " + viewData.ModuleId + " AND ProcessExecutionId <> " + viewData.ProcessExecutionId + "";
                    }
                    else
                    {
                        qry = "SELECT ProcessExecutionId FROM processexecution WHERE IsDeleted = 0 AND ProjectId = " + viewData.ProjectId + " AND ModuleId = " + viewData.ModuleId + "";
                    }
                    int chkResult = dataAccess.SelectIntValueByQuery(qry);
                    if (chkResult < 0)
                    {
                        ErrorMessage = UserMessages.COMMON_ERROR_MESSAGE;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);

                        viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                        viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId);
                        viewData.TestCaseBuildList = GetDropdownRecordsByWhere("", true, "SELECT TestCaseBuildId,TestCaseBuildName FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId + " AND ModuleId=" + viewData.ModuleId);
                        viewData.CommandList = GetDropdownRecordsByWhere("", false, "SELECT DISTINCT Command, Command FROM commands WHERE IsDeleted = 0");

                        return View(viewData);
                    }
                    else if (chkResult > 0 && chkResult != viewData.ProcessExecutionId)
                    {
                        ErrorMessage = "Process Execution for same input is already exists. Duplicate entries not allowed.";
                        viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                        viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId);
                        viewData.TestCaseBuildList = GetDropdownRecordsByWhere("", true, "SELECT TestCaseBuildId,TestCaseBuildName FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId + " AND ModuleId=" + viewData.ModuleId);
                        viewData.CommandList = GetDropdownRecordsByWhere("", false, "SELECT DISTINCT Command, Command FROM commands WHERE IsDeleted = 0");

                        return View(viewData);
                    }

                    if (viewData.ProcessExecutionId > 0)
                    {
                        lstValues = new List<string>() { Convert.ToString(viewData.ProcessExecutionId), Convert.ToString(viewData.ProjectId), Convert.ToString(viewData.ModuleId), Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Update(lstColumnEdit, lstValues, strTableName);
                    }
                    else
                    {
                        lstValues = new List<string>() { Convert.ToString(viewData.ProjectId), Convert.ToString(viewData.ModuleId), Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Insert(lstColumnsAdd, lstValues, strTableName);

                        if (result.isSuccessful)
                            processExecId = Convert.ToInt32(result.responseData);
                    }

                    CanClearMessage = false;
                    if (result.isSuccessful)
                    {

                        //To save the child records
                        if (processExecId > 0)
                        {
                            lstColumns = new List<string>() { "ProcessExecutionId" };
                            lstValues = new List<string>() { Convert.ToString(processExecId) };
                            Result<Int64> processExecRefDelete = dataAccess.Delete(false, lstColumns, lstValues, strTableRefTableName);
                            if (!processExecRefDelete.isSuccessful)
                            {
                                ErrorMessage = processExecRefDelete.message;
                                return RedirectToAction(Actions.Index);
                            }

                            viewData.ProcessExecutionChildRefList = new List<ProcessExecutionChild>();
                            if (!string.IsNullOrEmpty(viewData.strProcessExecutionChildRefList))
                            {
                                viewData.ProcessExecutionChildRefList = new List<ProcessExecutionChild>(Newtonsoft.Json.JsonConvert.DeserializeObject<ProcessExecutionChild[]>(viewData.strProcessExecutionChildRefList));
                            }

                            foreach (var item in viewData.ProcessExecutionChildRefList)
                            {
                                lstColumns = new List<string>() { "ProcessExecutionId", "TestCaseBuildId", "Command", "ProcessStatus" };
                                lstValues = new List<string>() { Convert.ToString(processExecId), Convert.ToString(item.TestCaseBuildId), item.Command, item.ProcessStatus };
                                result = dataAccess.Insert(lstColumns, lstValues, strTableRefTableName);
                                if (!result.isSuccessful)
                                {
                                    ErrorMessage = result.message;
                                    return RedirectToAction(Actions.Index);
                                }
                            }
                        }

                        if (viewData.ProcessExecutionId > 0)
                        {
                            SuccessMessage = UserMessages.UPDATE_SUCCESS;
                        }
                        else
                        {
                            SuccessMessage = UserMessages.SAVE_SUCCESS;
                        }
                    }
                    else
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }

                    return RedirectToAction(Actions.Index);
                }

                var modalStateError = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                ErrorMessage = modalStateError.ToString();
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }
            viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
            viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId);
            viewData.TestCaseBuildList = GetDropdownRecordsByWhere("", true, "SELECT TestCaseBuildId,TestCaseBuildName FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId + " AND ModuleId=" + viewData.ModuleId);
            viewData.CommandList = GetDropdownRecordsByWhere("", false, "SELECT DISTINCT Command, Command FROM commands WHERE IsDeleted = 0");

            return View(viewData);
        }
        #endregion

        #region public ActionResult Delete(int processExecutionId)
        /// <summary>
        /// Delete the ProcessExecution
        /// </summary>
        /// <param name="processExecutionId">The ProcessExecution Id</param>
        /// <returns></returns>
        public ActionResult Delete(int processExecutionId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "Delete", "Id=" + processExecutionId };
                InsertErrorAppLog(lstValues, 2);

                //Soft Delete
                lstColumns = new List<string>() { "ProcessExecutionId", "IsDeleted", "ModifiedBy" };
                lstValues = new List<string>() { Convert.ToString(processExecutionId), "1", Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                Result<Int64> result = dataAccess.Delete(true, lstColumns, lstValues, strTableName);

                CanClearMessage = false;
                if (!result.isSuccessful)
                {
                    ErrorMessage = result.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "Delete", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
                else
                {
                    SuccessMessage = UserMessages.DELETE_SUCCESS;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "Delete", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return RedirectToAction(Actions.Index);
        }

        #endregion

        #region public System.Web.Mvc.JsonResult GetModulesByProjectId(int projectId)
        /// <summary>
        /// To select the modules based on the selected project Id
        /// </summary>
        /// <param name="projectId">The selected project</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public System.Web.Mvc.JsonResult GetModulesByProjectId(int projectId)
        {
            System.Web.Mvc.JsonResult res = new System.Web.Mvc.JsonResult();

            Result<DataTable> result = dataAccess.SelectByQuery("SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + projectId);
            if (result.isSuccessful)
            {
                Dictionary<string, string> forms = result.responseData.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());

                res.Data = forms;
            }
            res.JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet;
            return res;
        }
        #endregion

        #region public System.Web.Mvc.JsonResult RunTestCaseBuilder(int testCaseBuilderId)
        /// <summary>
        /// To run the test case builder based on the selected Id
        /// </summary>
        /// <param name="testCaseBuilderId">The selected Testcase Builder</param> 
        /// <returns></returns>
        [System.Web.Mvc.AllowAnonymous]
        [HttpPost]
        public System.Web.Mvc.JsonResult RunTestCaseBuilder(int testCaseBuilderId)
        {
            System.Web.Mvc.JsonResult res = new System.Web.Mvc.JsonResult();

            string result = RunTestCasesById(testCaseBuilderId);
            res.Data = result;

            res.JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet;
            return res;
        }
        #endregion

        #region public System.Web.Mvc.JsonResult GetTestCaseBuildByFilter(int projectId, int moduleId)
        /// <summary>
        /// To select the test case build based on the selected project and module Id
        /// </summary>
        /// <param name="projectId">The selected project</param>
        /// <param name="moduleId">The selected module</param>
        /// <returns></returns>
        [System.Web.Mvc.AllowAnonymous]
        [HttpPost]
        public System.Web.Mvc.JsonResult GetTestCaseBuildByFilter(int projectId, int moduleId)
        {
            System.Web.Mvc.JsonResult res = new System.Web.Mvc.JsonResult();

            Result<DataTable> result = dataAccess.SelectByQuery("SELECT TestCaseBuildId,TestCaseBuildName FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId=" + projectId + " AND ModuleId = " + moduleId);
            if (result.isSuccessful)
            {
                Dictionary<string, string> forms = result.responseData.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());

                res.Data = forms;
            }
            res.JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet;
            return res;
        }
        #endregion

        //#region public string RunTestCasesById(int tcbId)
        ///// <summary>
        ///// To run the test case and update the result
        ///// </summary>
        ///// <param name="tcbId"></param>
        ///// <returns></returns>
        //public string RunTestCasesById(int tcbId)
        //{
        //    string returnResult = "Success";
        //    DataTable sourceTab = new DataTable();
        //    DataTable targetTab = new DataTable();

        //    //Get all testcases by Id
        //    Result<DataTable> tcResult = dataAccess.SelectByQuery("SELECT TestcaseBuildDevChildId, TableName, ColumnName, TargetTableName, TargetColumnName, MatchCondition FROM testcasebuilddevchild WHERE TestCaseBuildDevId = (SELECT TestCaseBuildDevId FROM testcasebuilddev WHERE TestCaseBuildId = " + tcbId + ")");
        //    if (tcResult.isSuccessful)
        //    {
        //        if(tcResult.responseData.Rows.Count > 0){

        //            foreach (DataRow tcrow in tcResult.responseData.Rows)
        //            {
        //                // Get the Table details
        //                Result<DataTable> sourceResult = dataAccess.SelectByQuery("SELECT " + Convert.ToString(tcrow["ColumnName"]) + " FROM " + Convert.ToString(tcrow["TableName"]));
        //                if (sourceResult.isSuccessful)
        //                {
        //                    if (sourceResult.responseData.Rows.Count > 0)
        //                    {
        //                        sourceTab = sourceResult.responseData;
        //                    } 
        //                    else
        //                    { 
        //                        returnResult = "No records in source table to validate";
        //                        return returnResult;
        //                    }
        //                }
        //                else
        //                {
        //                    ErrorMessage = sourceResult.message;
        //                    //To insert the error log 
        //                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "RunTestCasesById", ErrorMessage.Replace("'", "\"") };
        //                    InsertErrorAppLog(lstValues, 1);

        //                    returnResult = "Failed";
        //                    return returnResult;
        //                }


        //                //Get the Target table details
        //                Result<DataTable> targetResult = dataAccess.SelectByQuery("SELECT " + Convert.ToString(tcrow["TargetColumnName"]) + " FROM " + Convert.ToString(tcrow["TargetTableName"]));
        //                if (targetResult.isSuccessful)
        //                {
        //                    if (targetResult.responseData.Rows.Count > 0)
        //                    {
        //                        targetTab = targetResult.responseData;
        //                    }
        //                    else
        //                    {
        //                        returnResult = "No records in target table to validate";
        //                        return returnResult;
        //                    }
        //                }
        //                else
        //                {
        //                    ErrorMessage = targetResult.message;
        //                    //To insert the error log 
        //                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "RunTestCasesById", ErrorMessage.Replace("'", "\"") };
        //                    InsertErrorAppLog(lstValues, 1);

        //                    returnResult = "Failed";
        //                    return returnResult;
        //                }

        //                //Validate the source and target table 
        //                foreach (DataRow srcrow in sourceTab.Rows)
        //                {
        //                    int index = sourceTab.Rows.IndexOf(srcrow); 
        //                    DataRow tarRow = targetTab.Rows[index];

        //                    //Validate the condition and insert the result into the table 
        //                    if (Convert.ToString(srcrow[0]) == Convert.ToString(tarRow[0]))
        //                    {
        //                        lstValues = new List<string>() { "", Convert.ToString(tcrow["TestcaseBuildDevChildId"]), Convert.ToString(tcrow["MatchCondition"]), Convert.ToString(srcrow[0]), Convert.ToString(tarRow[0]), "Success" };
        //                    }
        //                    else
        //                    {
        //                        lstValues = new List<string>() { "", Convert.ToString(tcrow["TestcaseBuildDevChildId"]), Convert.ToString(tcrow["MatchCondition"]), Convert.ToString(srcrow[0]), Convert.ToString(tarRow[0]), "Failed" };
        //                    } 
        //                    Result<Int64> result = dataAccess.Insert(lstColTestResult, lstValues, "testcaseresult");
        //                    if (!result.isSuccessful)
        //                    {
        //                        ErrorMessage = targetResult.message;
        //                        //To insert the error log 
        //                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "RunTestCasesById", ErrorMessage.Replace("'", "\"") };
        //                        InsertErrorAppLog(lstValues, 1);

        //                        returnResult = "Failed";
        //                        return returnResult;
        //                    }
        //                }

        //            }

        //        } 
        //    }
        //    else
        //    {
        //        ErrorMessage = tcResult.message;
        //        //To insert the error log 
        //        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "RunTestCasesById", ErrorMessage.Replace("'", "\"") };
        //        InsertErrorAppLog(lstValues, 1);

        //        returnResult = "Failed";
        //    }

        //    return returnResult;
        //}
        //#endregion 

        #region public string RunTestCasesById(int tcbId)
        /// <summary>
        /// To run the test case and update the result
        /// </summary>
        /// <param name="tcbId"></param>
        /// <returns></returns>
        public string RunTestCasesById(int testCaseBuildId)
        {
            string returnResult = "Success";

            List<string> parameterNameList = new List<string>() { "tcBuildId" };
            List<string> parameterValueList = new List<string>() { testCaseBuildId.ToString() };
            Result<DataTable> testCaseResult = dataAccess.ExecuteSpWithParameter("USP_ExecuteTestCase", parameterNameList, parameterValueList);
            if (!testCaseResult.isSuccessful)
            {
                ErrorMessage = testCaseResult.message;
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "RunTestCasesById", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);

                returnResult = "Failed";
            }
            return returnResult;
        }
        #endregion

        #region public ActionResult TestResultIndex()
        /// <summary>
        /// Action/View returs the details of TestResult 
        /// </summary> 
        /// <returns>List the details</returns>
        public ActionResult TestResultIndex()
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewList = new List<TestCaseResult>();

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "TestResultIndex", "" };
                InsertErrorAppLog(lstValues, 2);

                Result<DataTable> testResult = dataAccess.SelectByQuery("SELECT TestCaseResultId,MatchCondition,SourceValue,TargetValue,MatchResult FROM testcaseresult");
                if (testResult.isSuccessful)
                {
                    viewList = (from DataRow row in testResult.responseData.Rows
                                select new TestCaseResult
                                {
                                    TestCaseResultId = Convert.ToInt32(row["TestCaseResultId"]),
                                    MatchCondition = Convert.ToString(row["MatchCondition"]),
                                    SourceValue = Convert.ToString(row["SourceValue"]),
                                    TargetValue = Convert.ToString(row["TargetValue"]),
                                    MatchResult = Convert.ToString(Convert.ToString(row["MatchResult"])),
                                }).ToList();

                }
                else
                {
                    ErrorMessage = testResult.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "TestResultIndex", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "ProcessExecution", "TestResultIndex", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(viewList);
        }
        #endregion

    }
}
