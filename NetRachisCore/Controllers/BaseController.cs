﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NetRachisCore.Helpers;
using System.Globalization;
using System.Reflection;
using System.Data;
using NetKathirBaseCore; 
using System.IO;
using NetKathirBaseCore.Class;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Hosting;

namespace NetRachisCore.Controllers
{
    public class BaseController : Controller
    {
        
        #region Properties

        #region  public string SuccessMessage

        /// <summary>
        /// Property to get and set the success message
        /// </summary>
        public string SuccessMessage
        {
            get { return TempData[CommonHelper.SuccessMessageKey] as string; }
            set
            {
                if (TempData.ContainsKey(CommonHelper.SuccessMessageKey))
                    TempData[CommonHelper.SuccessMessageKey] = value;
                else
                    TempData.Add(CommonHelper.SuccessMessageKey, value);

                TempData.Remove(CommonHelper.ErrorMessageKey);
            }
        }

        #endregion

        #region  public string ErrorMessage
        /// <summary>
        /// Property to get and set the error message
        /// </summary>
        public string ErrorMessage
        {
            get { return TempData[CommonHelper.ErrorMessageKey] as string; }
            set
            {
                if (TempData.ContainsKey(CommonHelper.ErrorMessageKey))
                    TempData[CommonHelper.ErrorMessageKey] = value;
                else
                    TempData.Add(CommonHelper.ErrorMessageKey, value);

                TempData.Remove(CommonHelper.SuccessMessageKey);
            }
        }

        #endregion

        #region public bool CanClearMessage
        /// <summary>
        /// Property to get and set if the error message can be cleared
        /// </summary>
        public bool CanClearMessage
        {
            get
            {
                object objClearMessage = TempData[CommonHelper.CanClearMessageKey];
                return objClearMessage == null || Convert.ToBoolean(objClearMessage, CultureInfo.CurrentCulture);
            }
            set
            {
                if (TempData.ContainsKey(CommonHelper.CanClearMessageKey))
                    TempData[CommonHelper.CanClearMessageKey] = value;
                else
                    TempData.Add(CommonHelper.CanClearMessageKey, value);
            }
        }
        #endregion

        #endregion

        #region Methods

        #region  public void ClearMessage()
        /// <summary>
        /// Method to clear both success and error message
        /// </summary>
        public void ClearMessage()
        {
            TempData.Remove(CommonHelper.SuccessMessageKey);
            TempData.Remove(CommonHelper.ErrorMessageKey);
        }
        #endregion

        #region public static string CreateRandomNumber(int LengthofNum)

        public static string CreateRandomNumber(int LengthofNum)
        {
            string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
            Random randNum = new Random();
            char[] chars = new char[LengthofNum];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < LengthofNum; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(chars);
        }

        #endregion

        #region public DataTable ToDataTable<T>(List<T> items)
        /// <summary>
        /// To convert list to datatable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }

            return dataTable;
        }
        #endregion

        #region public static SelectList GetDropdownRecordsByWhere(string selected, bool addDefault, string query)

        /// <summary>
        /// Gets the dropdown details.
        /// </summary>
        /// <param name="selected">The selected.</param>
        /// <param name="addDefault">if set to <c>true</c> [add default].</param>
        /// <param name="query">query</param>
        /// <returns></returns>
        public static SelectList GetDropdownRecordsByWhere(string selected, bool addDefault, string query)
        {
            var NetkathirBaseClass = new NetkathirBaseClass();

            var retResult = NetkathirBaseClass.SelectByQuery(query);

            if (!retResult.isSuccessful)
                throw new Exception(retResult.message);

            Dictionary<string, string> recordDetails = NetkathirBaseClass.SelectByQuery(query).responseData.AsEnumerable().ToDictionary<System.Data.DataRow, string, string>(row => row[0].ToString(),
                                        row => row[1].ToString());

            //if (addDefault)
            //    itemType.Add("0", "Select Race");

            //var result = new SelectList(
            //    from pair in recordDetails orderby pair.Key ascending select pair,
            //    "Key",
            //    "Value",
            //    selected
            //    );
            var result = new SelectList(
                from pair in recordDetails select pair,
                "Key",
                "Value",
                selected
                );
            return result;
        }

        #endregion

        #region public static void InsertErrorAppLog(List<string> lstValue, int isErrorOrApplication)

        /// <summary>
        /// Method to insert the values in tables
        /// </summary> 
        /// <param name="lstValue"></param>
        /// <param name="isErrorOrApplication"></param>
        /// <returns></returns>
        public void InsertErrorAppLog(List<string> lstValue, int isErrorOrApplication)
        {
            try
            {
                NetkathirBaseClass netkathirBaseClass = new NetkathirBaseClass();
                var result = new Result<Int64>
                {
                    isSuccessful = true,
                    message = default(string),
                    responseData = default(Int64)
                };

                List<string> lstCols = new List<string>();
                string strTableName = "";
                if (isErrorOrApplication == 1)
                {
                    lstCols = new List<string>() { "UserId", "Controller", "Action", "ErrorMsg" };
                    strTableName = "ErrorLog";
                }
                else
                {
                    lstCols = new List<string>() { "UserId", "Controller", "Action", "Parameter" };
                    strTableName = "ApplicationLog";
                }

                result = netkathirBaseClass.Insert(lstCols, lstValue, strTableName);
                if (!result.isSuccessful)
                {
                    WriteErrorLog(DateTime.Now.ToString() + ": " + result.message);
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog(DateTime.Now.ToString() + ": " + ex.Source.ToString().Trim() + "; " + ex.Message.ToString().Trim());
            }
        }

        #endregion 

        #region public static void WriteErrorLog(string Message)
        /// <summary>
        /// To write the error message in text file
        /// </summary>
        /// <param name="Message"></param>
        public void WriteErrorLog(string Message)
        {
            
            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Logs\\"); 
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = path + "ErrorLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!System.IO.File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = System.IO.File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
        #endregion

        #region public ActionResult DownloadFile(string file)
        /// <summary>
        /// To download the file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public ActionResult DownloadFile(string file)
        {
            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + file); 
            if (!System.IO.File.Exists(path))
            {
                return NotFound();
            }

            var fileBytes = System.IO.File.ReadAllBytes(path);
            var response = new FileContentResult(fileBytes, "application/octet-stream")
            {
                FileDownloadName = Path.GetFileName(file)
            };
            return response;
        }
        #endregion

        #endregion
    }
}
