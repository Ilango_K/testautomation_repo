﻿#region using


using System;
using NetKathirBaseCore;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using NetRachisCore.Helpers;
using NetKathirBaseCore.Class;
using NetRachisCore.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NetRachisCore.Models.BusinessObjects;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.XmlDiffPatch;
using System.Xml;
using Renci.SshNet;

#endregion

namespace NetRachisCore.Controllers
{
    [System.Web.Mvc.Authorize]
    //[SessionExpire]
    public class XmlPullController : BaseController
    {

        #region Declaration
        NetkathirBaseClass dataAccess = new NetkathirBaseClass();
        List<string> lstColumns = new List<string>();
        List<string> lstValues = new List<string>();
        List<string> parameterNameList = new List<string>();
        List<string> parameterValueList = new List<string>();
        #endregion

        #region public ActionResult Index()
        /// <summary>
        /// Action/View returs the view
        /// </summary> 
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(); 
        }
        #endregion

        #region public ActionResult PullXml()
        /// <summary>
        /// Action to compare the trade
        /// </summary> 
        /// <returns></returns>
        [HttpPost]
        public ActionResult PullXml()
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            try
            {
                CanClearMessage = false;
                if (ModelState.IsValid)
                {
                    var result = new Result<Int64>
                    {
                        isSuccessful = true,
                        message = default(string),
                        responseData = default(Int64)
                    };

                    //To insert the application log   
                    var sessionHelper = new SessionHelper(new HttpContextAccessor()); 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "XmlPull", "PullXml-Post", "" };
                    InsertErrorAppLog(lstValues, 2);

                    //string sourcePath = "D:\\GPG\\Projects\\NetRachisCore\\Documents\\SourceTrade.xml";
                    //string destinationPath = "D:\\GPG\\Projects\\NetRachisCore\\Documents\\DestinationTrade.xml";

                    using (ScpClient client = new ScpClient("13.82.184.194", "summitadmin", "Misys123456"))
                    {
                        client.Connect();

                        using (Stream localFile = System.IO.File.Create("D:\\GPG\\Projects\\NetRachisCore\\Documents\\ImportFromServer"))
                        {
                            client.Download("G:\\dev\\Test\\DestinationTrade.xml", localFile);
                        }
                    }


                    return RedirectToAction("Index");
                }

                var modalStateError = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                ErrorMessage = modalStateError.ToString();

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "XmlPull", "PullXml-Post", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View();
        }
        #endregion


    }
}
