﻿#region using

 
using System; 
using NetKathirBaseCore;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using NetRachisCore.Helpers;
using NetKathirBaseCore.Class;
using NetRachisCore.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http; 
using Microsoft.AspNetCore.Mvc;
using NetRachisCore.Models.BusinessObjects;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;

#endregion

namespace NetRachisCore.Controllers
{
    [System.Web.Mvc.Authorize]
    [SessionExpire]
    public class TradeBookingConfigController : BaseController
    {

        #region Declaration
        NetkathirBaseClass dataAccess = new NetkathirBaseClass();
        List<string> lstColumnsAdd = new List<string>() { "ProjectId", "ModuleId", "TestCaseBuildId", "SchedulerDate", "SchedulerTime", "SchedulerFrequency", "SchedulerSet", "TradeInputPath", "FileNameFormat", "TargetSystemId", "TradeInputSet", "CommandTypeId", "CommandLine", "AdditionalInfo", "TargetPath", "CommandSet", "CreatedBy" };
        List<string> lstColumnEdit = new List<string>() { "TradeBookingConfigId", "ProjectId", "ModuleId", "TestCaseBuildId", "SchedulerDate", "SchedulerTime", "SchedulerFrequency", "SchedulerSet", "TradeInputPath", "FileNameFormat", "TargetSystemId", "TradeInputSet", "CommandTypeId", "CommandLine", "AdditionalInfo", "TargetPath", "CommandSet", "ModifiedBy" };
        List<string> lstColumns = new List<string>();
        List<string> lstValues = new List<string>();
        List<string> parameterNameList = new List<string>();
        List<string> parameterValueList = new List<string>();
        string strTableName = "TradeBookingConfig";
        #endregion

        #region public ActionResult Index()
        /// <summary>
        /// Action/View returs the details of TradeBookingConfig
        /// </summary> 
        /// <returns>List the details</returns>
        public ActionResult Index()
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewList = new List<TradeBookingConfig>();

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TradeBookingConfig", "Index", "" };
                InsertErrorAppLog(lstValues, 2);

                Result<DataTable> spResult = dataAccess.ExecuteSp("USP_SelectAllTradeBookingConfig");
                if (spResult.isSuccessful)
                {
                    viewList = (from DataRow row in spResult.responseData.Rows
                                select new TradeBookingConfig
                                {
                                    TradeBookingConfigId = Convert.ToInt32(row["TradeBookingConfigId"]),
                                    ProjectId = Convert.ToInt32(row["ProjectId"]),
                                    ProjectName = Convert.ToString(row["ProjectName"]),
                                    ModuleId = Convert.ToInt32(row["ModuleId"]),
                                    ModuleName = Convert.ToString(row["ModuleName"]),
                                    TestCaseBuildId = Convert.ToInt32(row["TestCaseBuildId"]),
                                    TestCaseBuildName = Convert.ToString(row["TestCaseBuildName"])
                                }).ToList();
                }
                else
                {
                    ErrorMessage = spResult.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TradeBookingConfig", "Index", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TradeBookingConfig", "Index", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(Views.Index, viewList);
        }
        #endregion

        #region public ActionResult AddEdit(int tradeBookingConfigId)
        /// <summary>
        /// Save/Edit the TradeBookingConfig
        /// </summary>
        /// <param name="tradeBookingConfigId">The TradeBookingConfig  id</param>
        /// <returns></returns>
        public ActionResult AddEdit(int tradeBookingConfigId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewData = new TradeBookingConfig();
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TradeBookingConfig", "AddEdit-Get", "Id=" + tradeBookingConfigId };
                InsertErrorAppLog(lstValues, 2);

                viewData.SchedulerDate = DateTime.Now;
                viewData.SchedulerFrequency = "10";

                if (tradeBookingConfigId > 0)
                {
                    Result<DataTable> result = dataAccess.SelectById(tradeBookingConfigId, lstColumnEdit, strTableName);

                    if (!result.isSuccessful)
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TradeBookingConfig", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }
                    else
                    {
                        if (result.responseData.Rows.Count > 0)
                        {
                            viewData.TradeBookingConfigId = Convert.ToInt32(result.responseData.Rows[0]["TradeBookingConfigId"]);
                            viewData.ProjectId = Convert.ToInt32(result.responseData.Rows[0]["ProjectId"]);
                            viewData.ModuleId = Convert.ToInt32(result.responseData.Rows[0]["ModuleId"]);
                            viewData.TestCaseBuildId = Convert.ToInt32(result.responseData.Rows[0]["TestCaseBuildId"]);
                            viewData.SchedulerDate = Convert.ToDateTime(result.responseData.Rows[0]["SchedulerDate"]);
                            viewData.SchedulerTime = Convert.ToString(result.responseData.Rows[0]["SchedulerTime"]);
                            viewData.SchedulerFrequency = Convert.ToString(result.responseData.Rows[0]["SchedulerFrequency"]);
                            viewData.SchedulerSet = Convert.ToBoolean(result.responseData.Rows[0]["SchedulerSet"]);
                            viewData.TradeInputPath = Convert.ToString(result.responseData.Rows[0]["TradeInputPath"]);
                            viewData.FileNameFormat = Convert.ToString(result.responseData.Rows[0]["FileNameFormat"]);
                            viewData.TargetSystemId = Convert.ToInt32(result.responseData.Rows[0]["TargetSystemId"]);
                            viewData.TradeInputSet = Convert.ToBoolean(result.responseData.Rows[0]["TradeInputSet"]);
                            viewData.CommandTypeId = Convert.ToInt32(result.responseData.Rows[0]["CommandTypeId"]);
                            viewData.CommandLine = Convert.ToString(result.responseData.Rows[0]["CommandLine"]);
                            viewData.AdditionalInfo = Convert.ToString(result.responseData.Rows[0]["AdditionalInfo"]);
                            viewData.TargetPath = Convert.ToString(result.responseData.Rows[0]["TargetPath"]);
                            viewData.CommandSet = Convert.ToBoolean(result.responseData.Rows[0]["CommandSet"]);
                        }
                    }
                }

                viewData.TargetSystemList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TargetSystemId), true, "SELECT TargetSystemId,TargetSystem FROM targetsystems WHERE IsDeleted = 0");
                viewData.CommandTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.CommandTypeId), true, "SELECT CommandTypeId,CommandType FROM commandtypes WHERE IsDeleted = 0");
                viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId);
                viewData.TestCaseBuildList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TestCaseBuildId), true, "SELECT TestCaseBuildId,TestCaseBuildName FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId + " AND ModuleId=" + viewData.ModuleId);

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TradeBookingConfig", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);

                viewData.TargetSystemList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TargetSystemId), true, "SELECT TargetSystemId,TargetSystem FROM targetsystems WHERE IsDeleted = 0");
                viewData.CommandTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.CommandTypeId), true, "SELECT CommandTypeId,CommandType FROM commandtypes WHERE IsDeleted = 0");
                viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId);
                viewData.TestCaseBuildList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TestCaseBuildId), true, "SELECT TestCaseBuildId,TestCaseBuildName FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId + " AND ModuleId=" + viewData.ModuleId);

            }

            return View(Views.AddEdit, viewData);
        }

        #endregion

        #region public ActionResult AddEdit(TradeBookingConfig viewData)
        /// <summary>
        ///  Save 
        /// </summary>
        /// <param name="viewData">Sending the TradeBookingConfig values</param>
        /// <returns></returns>
        [HttpPost]
        [System.Web.Mvc.ValidateInput(false)]
        public ActionResult AddEdit(TradeBookingConfig viewData)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                string objString = JsonConvert.SerializeObject(viewData);
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TradeBookingConfig", "AddEdit-Post", objString.Replace("\"", "").Replace("{", "").Replace("}", "") };
                InsertErrorAppLog(lstValues, 2);

                int configId = viewData.TradeBookingConfigId;
                if (ModelState.IsValid)
                {

                    CanClearMessage = false;
                    var result = new Result<Int64>
                    {
                        isSuccessful = true,
                        message = default(string),
                        responseData = default(Int64)
                    };

                    //Check TradeBookingConfig already exist
                    string qry = "";
                    if (viewData.TradeBookingConfigId > 0)
                    {
                        qry = "SELECT TradeBookingConfigId FROM tradebookingconfig WHERE IsDeleted = 0 AND TradeBookingConfigId <> " + viewData.TradeBookingConfigId + " AND ProjectId = " + viewData.ProjectId + " AND ModuleId = " + viewData.ModuleId + " AND TestCaseBuildId = " + viewData.TestCaseBuildId;
                    }
                    else
                    {
                        qry = "SELECT TradeBookingConfigId FROM tradebookingconfig WHERE IsDeleted = 0 AND ProjectId = " + viewData.ProjectId + " AND ModuleId = " + viewData.ModuleId + " AND TestCaseBuildId = " + viewData.TestCaseBuildId;
                    }
                    int chkResult = dataAccess.SelectIntValueByQuery(qry);
                    if (chkResult < 0)
                    {

                        viewData.TargetSystemList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TargetSystemId), true, "SELECT TargetSystemId,TargetSystem FROM targetsystems WHERE IsDeleted = 0");
                        viewData.CommandTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.CommandTypeId), true, "SELECT CommandTypeId,CommandType FROM commandtypes WHERE IsDeleted = 0");
                        viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                        viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId);
                        viewData.TestCaseBuildList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TestCaseBuildId), true, "SELECT TestCaseBuildId,TestCaseBuildName FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId + " AND ModuleId=" + viewData.ModuleId);


                        ErrorMessage = UserMessages.COMMON_ERROR_MESSAGE;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TradeBookingConfig", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);

                        return View(viewData);
                    }
                    else if (chkResult > 0 && chkResult != viewData.TradeBookingConfigId)
                    {
                        ErrorMessage = "Trade Booking Configuration already exists for the selected entries. Duplicate entries not allowed.";

                        viewData.TargetSystemList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TargetSystemId), true, "SELECT TargetSystemId,TargetSystem FROM targetsystems WHERE IsDeleted = 0");
                        viewData.CommandTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.CommandTypeId), true, "SELECT CommandTypeId,CommandType FROM commandtypes WHERE IsDeleted = 0");
                        viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                        viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId);
                        viewData.TestCaseBuildList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TestCaseBuildId), true, "SELECT TestCaseBuildId,TestCaseBuildName FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId + " AND ModuleId=" + viewData.ModuleId);

                        return View(viewData);
                    }

                    if (viewData.TradeBookingConfigId > 0)
                    {
                        lstValues = new List<string>() { Convert.ToString(viewData.TradeBookingConfigId), Convert.ToString(viewData.ProjectId), Convert.ToString(viewData.ModuleId), Convert.ToString(viewData.TestCaseBuildId), viewData.SchedulerDate.ToString("yyyy-MM-dd"), viewData.SchedulerTime, viewData.SchedulerFrequency, (viewData.SchedulerSet == true ? "1" : "0"), viewData.TradeInputPath, viewData.FileNameFormat, Convert.ToString(viewData.TargetSystemId), (viewData.TradeInputSet == true ? "1" : "0"), Convert.ToString(viewData.CommandTypeId), viewData.CommandLine, viewData.AdditionalInfo, viewData.TargetPath, (viewData.CommandSet == true ? "1" : "0"), Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Update(lstColumnEdit, lstValues, strTableName);
                    }
                    else
                    {
                        lstValues = new List<string>() { Convert.ToString(viewData.ProjectId), Convert.ToString(viewData.ModuleId), Convert.ToString(viewData.TestCaseBuildId), viewData.SchedulerDate.ToString("yyyy-MM-dd"), viewData.SchedulerTime, viewData.SchedulerFrequency, (viewData.SchedulerSet == true ? "1" : "0"), viewData.TradeInputPath, viewData.FileNameFormat, Convert.ToString(viewData.TargetSystemId), (viewData.TradeInputSet == true ? "1" : "0"), Convert.ToString(viewData.CommandTypeId), viewData.CommandLine, viewData.AdditionalInfo, viewData.TargetPath, (viewData.CommandSet == true ? "1" : "0"), Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Insert(lstColumnsAdd, lstValues, strTableName);

                    }

                    if (result.isSuccessful)
                    {
                        if (viewData.TradeBookingConfigId > 0)
                        {
                            SuccessMessage = UserMessages.UPDATE_SUCCESS;
                        }
                        else
                        {
                            SuccessMessage = UserMessages.SAVE_SUCCESS;
                        }
                    }
                    else
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TradeBookingConfig", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }

                    return RedirectToAction(Actions.Index);
                }

                var modalStateError = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                ErrorMessage = modalStateError.ToString();

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TradeBookingConfig", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }
            viewData.TargetSystemList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TargetSystemId), true, "SELECT TargetSystemId,TargetSystem FROM targetsystems WHERE IsDeleted = 0");
            viewData.CommandTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.CommandTypeId), true, "SELECT CommandTypeId,CommandType FROM commandtypes WHERE IsDeleted = 0");
            viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
            viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId);
            viewData.TestCaseBuildList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TestCaseBuildId), true, "SELECT TestCaseBuildId,TestCaseBuildName FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId + " AND ModuleId=" + viewData.ModuleId);

            return View(viewData);
        }
        #endregion

        #region public ActionResult Delete(int tradeBookingConfigId)
        /// <summary>
        /// Delete the configurationId
        /// </summary>
        /// <param name="tradeBookingConfigId">The configurationId</param>
        /// <returns></returns>
        public ActionResult Delete(int tradeBookingConfigId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TradeBookingConfig", "Delete", "Id=" + tradeBookingConfigId };
                InsertErrorAppLog(lstValues, 2);

                //Soft Delete
                lstColumns = new List<string>() { "TradeBookingConfigId", "IsDeleted", "ModifiedBy" };
                lstValues = new List<string>() { Convert.ToString(tradeBookingConfigId), "1", Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                Result<Int64> result = dataAccess.Delete(true, lstColumns, lstValues, strTableName);

                CanClearMessage = false;
                if (!result.isSuccessful)
                {
                    ErrorMessage = result.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TradeBookingConfig", "Delete", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
                else
                {
                    SuccessMessage = UserMessages.DELETE_SUCCESS;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TradeBookingConfig", "Delete", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return RedirectToAction(Actions.Index);
        }

        #endregion

        #region public JsonResult GetModulesByProjectId(int projectId)
        /// <summary>
        /// To select the modules based on the selected project Id
        /// </summary>
        /// <param name="projectId">The selected project</param>
        /// <returns></returns>
        [System.Web.Mvc.AllowAnonymous]
        [HttpPost]
        public System.Web.Mvc.JsonResult GetModulesByProjectId(int projectId)
        {
            System.Web.Mvc.JsonResult res = new System.Web.Mvc.JsonResult();

            Result<DataTable> result = dataAccess.SelectByQuery("SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + projectId);
            if (result.isSuccessful)
            {
                Dictionary<string, string> forms = result.responseData.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());

                res.Data = forms;
            }
            res.JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet;
            return res;
        }
        #endregion

        #region public JsonResult GetTestCaseBuildByFilter(int projectId, int moduleId)
        /// <summary>
        /// To select the test case build based on the selected project and module Id
        /// </summary>
        /// <param name="projectId">The selected project</param>
        /// <param name="moduleId">The selected module</param>
        /// <returns></returns>
        [System.Web.Mvc.AllowAnonymous]
        [HttpPost]
        public System.Web.Mvc.JsonResult GetTestCaseBuildByFilter(int projectId, int moduleId)
        {
            System.Web.Mvc.JsonResult res = new System.Web.Mvc.JsonResult();

            Result<DataTable> result = dataAccess.SelectByQuery("SELECT TestCaseBuildId,TestCaseBuildName FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId=" + projectId + " AND ModuleId = " + moduleId);
            if (result.isSuccessful)
            {
                Dictionary<string, string> forms = result.responseData.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());

                res.Data = forms;
            }
            res.JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet;
            return res;
        }
        #endregion

    }
}
