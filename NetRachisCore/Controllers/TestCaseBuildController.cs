﻿#region using

 
using System; 
using NetKathirBaseCore;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using NetRachisCore.Helpers;
using NetKathirBaseCore.Class;
using NetRachisCore.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http; 
using Microsoft.AspNetCore.Mvc;
using NetRachisCore.Models.BusinessObjects;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;

#endregion

namespace NetRachisCore.Controllers
{
    [System.Web.Mvc.Authorize]
    [SessionExpire]
    public class TestCaseBuildController : BaseController
    {

        #region Declaration
        NetkathirBaseClass dataAccess = new NetkathirBaseClass();
        List<string> lstColumnsAdd = new List<string>() { "ProjectId", "ModuleId", "TestCaseBuildName", "Description", "CreatedBy" };
        List<string> lstColumnEdit = new List<string>() { "TestCaseBuildId", "ProjectId", "ModuleId", "TestCaseBuildName", "Description", "ModifiedBy" };
        List<string> lstColumns = new List<string>();
        List<string> lstValues = new List<string>();
        List<string> parameterNameList = new List<string>();
        List<string> parameterValueList = new List<string>();
        string strTableName = "TestCaseBuild";
        #endregion

        #region public ActionResult Index()
        /// <summary>
        /// Action/View returs the details of TestCaseBuild
        /// </summary> 
        /// <returns>List the details</returns>
        public ActionResult Index()
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewList = new List<TestCaseBuild>();

            try
            {
                //To insert the application log
                var sessionHelper = new SessionHelper(new HttpContextAccessor());  
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuild", "Index", "" };
                InsertErrorAppLog(lstValues, 2);

                Result<DataTable> spResult = dataAccess.ExecuteSp("USP_SelectAllTestCaseBuild");
                if (spResult.isSuccessful)
                {
                    viewList = (from DataRow row in spResult.responseData.Rows
                                select new TestCaseBuild
                                {
                                    TestCaseBuildId = Convert.ToInt32(row["TestCaseBuildId"]),
                                    TestCaseBuildName = Convert.ToString(row["TestCaseBuildName"]),
                                    ProjectId = Convert.ToInt32(Convert.ToString(row["ProjectId"])),
                                    ProjectName = Convert.ToString(row["ProjectName"]),
                                    ModuleId = Convert.ToInt32(row["ModuleId"]),
                                    ModuleName = Convert.ToString(row["ModuleName"]),
                                    Description = Convert.ToString(row["Description"]),
                                }).ToList();

                }
                else
                {
                    ErrorMessage = spResult.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuild", "Index", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuild", "Index", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(Views.Index, viewList);
        }
        #endregion

        #region public ActionResult AddEdit(int testCaseBuildId)
        /// <summary>
        /// Save/Edit the TestCaseBuild
        /// </summary>
        /// <param name="testCaseBuildId">The TestCaseBuild  id</param>
        /// <returns></returns>
        public ActionResult AddEdit(int testCaseBuildId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewData = new TestCaseBuild();
            viewData.ProjectList = new SelectList("");
            viewData.ModuleList = new SelectList("");
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuild", "AddEdit-Get", "Id=" + testCaseBuildId };
                InsertErrorAppLog(lstValues, 2);

                if (testCaseBuildId > 0)
                {
                    Result<DataTable> result = dataAccess.SelectById(testCaseBuildId, lstColumnEdit, strTableName);

                    if (!result.isSuccessful)
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuild", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }
                    else
                    {
                        if (result.responseData.Rows.Count > 0)
                        {
                            viewData.TestCaseBuildId = Convert.ToInt32(result.responseData.Rows[0]["TestCaseBuildId"]);
                            viewData.ProjectId = Convert.ToInt32(result.responseData.Rows[0]["ProjectId"]);
                            viewData.ModuleId = Convert.ToInt32(result.responseData.Rows[0]["ModuleId"]);
                            viewData.TestCaseBuildName = Convert.ToString(result.responseData.Rows[0]["TestCaseBuildName"]);
                            viewData.Description = Convert.ToString(result.responseData.Rows[0]["Description"]);
                        }
                    }
                }
                viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId);
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuild", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(Views.AddEdit, viewData);
        }

        #endregion

        #region public ActionResult AddEdit(TestCaseBuild viewData)
        /// <summary>
        ///  Save 
        /// </summary>
        /// <param name="viewData">Sending the TestCaseBuild values</param>
        /// <returns></returns>
        [HttpPost]
        [System.Web.Mvc.ValidateInput(false)]
        public ActionResult AddEdit(TestCaseBuild viewData)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                string objString = JsonConvert.SerializeObject(viewData);
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuild", "AddEdit-Post", objString.Replace("\"", "").Replace("{", "").Replace("}", "") };
                InsertErrorAppLog(lstValues, 2);

                if (ModelState.IsValid)
                {
                    var result = new Result<Int64>
                    {
                        isSuccessful = true,
                        message = default(string),
                        responseData = default(Int64)
                    };

                    //Check TestCaseBuild already exist
                    string qry = "";
                    if (viewData.TestCaseBuildId > 0)
                    {
                        qry = "SELECT TestCaseBuildId FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId = " + viewData.ProjectId + " AND ModuleId = " + viewData.ModuleId + " AND TestCaseBuildId <> " + viewData.TestCaseBuildId + " AND TestCaseBuildName = '" + viewData.TestCaseBuildName + "'";
                    }
                    else
                    {
                        qry = "SELECT TestCaseBuildId FROM testcasebuild WHERE IsDeleted = 0 AND ProjectId = " + viewData.ProjectId + " AND ModuleId = " + viewData.ModuleId + " AND TestCaseBuildName = '" + viewData.TestCaseBuildName + "'";
                    }
                    int chkResult = dataAccess.SelectIntValueByQuery(qry);
                    if (chkResult < 0)
                    {
                        ErrorMessage = UserMessages.COMMON_ERROR_MESSAGE;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuild", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);

                        viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                        viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId);

                        return View(viewData);
                    }
                    else if (chkResult > 0 && chkResult != viewData.TestCaseBuildId)
                    {
                        ErrorMessage = "TestCaseBuild name already exists. Duplicate entries not allowed.";
                        viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                        viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId);

                        return View(viewData);
                    }

                    if (viewData.TestCaseBuildId > 0)
                    {
                        lstValues = new List<string>() { Convert.ToString(viewData.TestCaseBuildId), Convert.ToString(viewData.ProjectId), Convert.ToString(viewData.ModuleId), viewData.TestCaseBuildName, viewData.Description, Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Update(lstColumnEdit, lstValues, strTableName);
                    }
                    else
                    {
                        lstValues = new List<string>() { Convert.ToString(viewData.ProjectId), Convert.ToString(viewData.ModuleId), viewData.TestCaseBuildName, viewData.Description, Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Insert(lstColumnsAdd, lstValues, strTableName);
                    }

                    CanClearMessage = false;
                    if (result.isSuccessful)
                    {
                        if (viewData.TestCaseBuildId > 0)
                        {
                            SuccessMessage = UserMessages.UPDATE_SUCCESS;
                        }
                        else
                        {
                            SuccessMessage = UserMessages.SAVE_SUCCESS;
                        }
                    }
                    else
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuild", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }

                    return RedirectToAction(Actions.Index);
                }

                var modalStateError = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                ErrorMessage = modalStateError.ToString();
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuild", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }
            viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
            viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + viewData.ProjectId);
            return View(viewData);
        }
        #endregion

        #region public ActionResult Delete(int testCaseBuildId)
        /// <summary>
        /// Delete the TestCaseBuild
        /// </summary>
        /// <param name="testCaseBuildId">The TestCaseBuild Id</param>
        /// <returns></returns>
        public ActionResult Delete(int testCaseBuildId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuild", "Delete", "Id=" + testCaseBuildId };
                InsertErrorAppLog(lstValues, 2);

                //Soft Delete
                lstColumns = new List<string>() { "TestCaseBuildId", "IsDeleted", "ModifiedBy" };
                lstValues = new List<string>() { Convert.ToString(testCaseBuildId), "1", Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                Result<Int64> result = dataAccess.Delete(true, lstColumns, lstValues, strTableName);

                CanClearMessage = false;
                if (!result.isSuccessful)
                {
                    ErrorMessage = result.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuild", "Delete", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
                else
                {
                    SuccessMessage = UserMessages.DELETE_SUCCESS;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "TestCaseBuild", "Delete", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return RedirectToAction(Actions.Index);
        }

        #endregion

        #region public JsonResult GetModulesByProjectId(int projectId)
        /// <summary>
        /// To select the modules based on the selected project Id
        /// </summary>
        /// <param name="projectId">The selected project</param>
        /// <returns></returns>
        [System.Web.Mvc.AllowAnonymous]
        [HttpPost]
        public System.Web.Mvc.JsonResult GetModulesByProjectId(int projectId)
        {
            System.Web.Mvc.JsonResult res = new System.Web.Mvc.JsonResult();

            Result<DataTable> result = dataAccess.SelectByQuery("SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0 AND ProjectId=" + projectId);
            if (result.isSuccessful)
            {
                Dictionary<string, string> forms = result.responseData.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                       row => row[1].ToString());

                res.Data = forms;
            }
            res.JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet;
            return res;
        }
        #endregion

    }
}
