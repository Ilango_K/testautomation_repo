﻿#region using

 
using System; 
using NetKathirBaseCore;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using NetRachisCore.Helpers;
using NetKathirBaseCore.Class;
using NetRachisCore.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http; 
using Microsoft.AspNetCore.Mvc;
using NetRachisCore.Models.BusinessObjects;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;

#endregion

namespace NetRachisCore.Controllers
{
    [System.Web.Mvc.Authorize]
    [SessionExpire]
    public class ModuleController : BaseController
    {

        #region Declaration
        NetkathirBaseClass dataAccess = new NetkathirBaseClass();
        List<string> lstColumnsAdd = new List<string>() { "ProjectId", "ModuleName", "Description", "CreatedBy" };
        List<string> lstColumnEdit = new List<string>() { "ModuleId", "ProjectId", "ModuleName", "Description", "ModifiedBy" };
        List<string> lstColumns = new List<string>();
        List<string> lstValues = new List<string>();
        List<string> parameterNameList = new List<string>();
        List<string> parameterValueList = new List<string>();
        string strTableName = "Module";
        #endregion

        #region public ActionResult Index()
        /// <summary>
        /// Action/View returs the details of Module
        /// </summary> 
        /// <returns>List the details</returns>
        public ActionResult Index()
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewList = new List<Module>();

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Module", "Index", "" };
                InsertErrorAppLog(lstValues, 2);

                Result<DataTable> spResult = dataAccess.ExecuteSp("USP_SelectAllModule");
                if (spResult.isSuccessful)
                {
                    viewList = (from DataRow row in spResult.responseData.Rows
                                select new Module
                                {
                                    ModuleId = Convert.ToInt32(Convert.ToString(row["ModuleId"])),
                                    ModuleName = Convert.ToString(row["ModuleName"]),
                                    ProjectId = Convert.ToInt32(Convert.ToString(row["ProjectId"])),
                                    ProjectName = Convert.ToString(row["ProjectName"]),
                                    Description = Convert.ToString(row["Description"]),
                                }).ToList();

                }
                else
                {
                    ErrorMessage = spResult.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Module", "Index", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Module", "Index", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(Views.Index, viewList);
        }
        #endregion

        #region public ActionResult AddEdit(int moduleId)
        /// <summary>
        /// Save/Edit the Module
        /// </summary>
        /// <param name="moduleId">The Module  id</param>
        /// <returns></returns>
        public ActionResult AddEdit(int moduleId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewData = new Module();
            viewData.ProjectList = new SelectList("");
            try
            {
                //To insert the application log
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Module", "AddEdit-Get", "Id=" + moduleId };
                InsertErrorAppLog(lstValues, 2);

                if (moduleId > 0)
                {
                    Result<DataTable> result = dataAccess.SelectById(moduleId, lstColumnEdit, strTableName);

                    if (!result.isSuccessful)
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Module", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }
                    else
                    {
                        if (result.responseData.Rows.Count > 0)
                        {
                            viewData.ModuleId = Convert.ToInt32(result.responseData.Rows[0]["ModuleId"]);
                            viewData.ProjectId = Convert.ToInt32(result.responseData.Rows[0]["ProjectId"]);
                            viewData.ModuleName = Convert.ToString(result.responseData.Rows[0]["ModuleName"]);
                            viewData.Description = Convert.ToString(result.responseData.Rows[0]["Description"]);
                        }
                    }
                }
                viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Module", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(Views.AddEdit, viewData);
        }

        #endregion

        #region public ActionResult AddEdit(Module viewData)
        /// <summary>
        ///  Save 
        /// </summary>
        /// <param name="viewData">Sending the Module values</param>
        /// <returns></returns>
        [HttpPost]
        [System.Web.Mvc.ValidateInput(false)]
        public ActionResult AddEdit(Module viewData)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                string objString = JsonConvert.SerializeObject(viewData);
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Module", "AddEdit-Post", objString.Replace("\"", "").Replace("{", "").Replace("}", "") };
                InsertErrorAppLog(lstValues, 2);

                if (ModelState.IsValid)
                {
                    var result = new Result<Int64>
                    {
                        isSuccessful = true,
                        message = default(string),
                        responseData = default(Int64)
                    };

                    //Check Module already exist
                    string qry = "";
                    if (viewData.ModuleId > 0)
                    {
                        qry = "SELECT ModuleId FROM module WHERE IsDeleted = 0 AND ProjectId = " + viewData.ProjectId + " AND ModuleId <> " + viewData.ModuleId + " AND ModuleName = '" + viewData.ModuleName + "'";
                    }
                    else
                    {
                        qry = "SELECT ModuleId FROM module WHERE IsDeleted = 0 AND ProjectId = " + viewData.ProjectId + " AND ModuleName = '" + viewData.ModuleName + "'";
                    }
                    int chkResult = dataAccess.SelectIntValueByQuery(qry);
                    if (chkResult < 0)
                    {
                        ErrorMessage = UserMessages.COMMON_ERROR_MESSAGE;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Module", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);

                        viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                        return View(viewData);
                    }
                    else if (chkResult > 0 && chkResult != viewData.ModuleId)
                    {
                        ErrorMessage = "Module name already exists. Duplicate entries not allowed.";

                        viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                        return View(viewData);
                    }

                    if (viewData.ModuleId > 0)
                    {
                        lstValues = new List<string>() { Convert.ToString(viewData.ModuleId), Convert.ToString(viewData.ProjectId), viewData.ModuleName, viewData.Description, Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Update(lstColumnEdit, lstValues, strTableName);
                    }
                    else
                    {
                        lstValues = new List<string>() { Convert.ToString(viewData.ProjectId), viewData.ModuleName, viewData.Description, Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Insert(lstColumnsAdd, lstValues, strTableName);
                    }

                    CanClearMessage = false;
                    if (result.isSuccessful)
                    {
                        if (viewData.ModuleId > 0)
                        {
                            SuccessMessage = UserMessages.UPDATE_SUCCESS;
                        }
                        else
                        {
                            SuccessMessage = UserMessages.SAVE_SUCCESS;
                        }
                    }
                    else
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Module", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }

                    return RedirectToAction(Actions.Index);
                }

                var modalStateError = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                ErrorMessage = modalStateError.ToString();
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Module", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }
            viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
            return View(viewData);
        }
        #endregion

        #region public ActionResult Delete(int moduleId)
        /// <summary>
        /// Delete the Module
        /// </summary>
        /// <param name="moduleId">The Module Id</param>
        /// <returns></returns>
        public ActionResult Delete(int moduleId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Module", "Delete", "Id=" + moduleId };
                InsertErrorAppLog(lstValues, 2);

                //Soft Delete
                lstColumns = new List<string>() { "ModuleId", "IsDeleted", "ModifiedBy" };
                lstValues = new List<string>() { Convert.ToString(moduleId), "1", Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                Result<Int64> result = dataAccess.Delete(true, lstColumns, lstValues, strTableName);

                CanClearMessage = false;
                if (!result.isSuccessful)
                {
                    ErrorMessage = result.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Module", "Delete", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
                else
                {
                    SuccessMessage = UserMessages.DELETE_SUCCESS;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Module", "Delete", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return RedirectToAction(Actions.Index);
        }

        #endregion

    }
}
