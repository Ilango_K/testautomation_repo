﻿#region using

 
using System; 
using NetKathirBaseCore;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using NetRachisCore.Helpers;
using NetKathirBaseCore.Class;
using NetRachisCore.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http; 
using Microsoft.AspNetCore.Mvc;
using NetRachisCore.Models.BusinessObjects;
using Microsoft.AspNetCore.Authorization;

#endregion

namespace NetRachisCore.Controllers
{
    [System.Web.Mvc.Authorize]
    [SessionExpire]
    public class ConfigurationController : BaseController
    {

        #region Declaration
        NetkathirBaseClass dataAccess = new NetkathirBaseClass();
        List<string> lstColumnsAdd = new List<string>() { "ProjectId", "ModuleId", "TestCaseBuildId", "InputFileTypeId", "InputFilePath", "InputFileFormat", "InputFileSet", "OutputFileTypeId", "OutputFilePath", "OutputFileFormat", "OutputFileSet", "SchedulerInputStartDate", "SchedulerInputStartTime", "SchedulerInputEndDate", "SchedulerInputEndTime", "SchedulerInputFrequency", "SchedulerInputSet", "SchedulerOutputStartDate", "SchedulerOutputStartTime", "SchedulerOutputEndDate", "SchedulerOutputEndTime", "SchedulerOutputFrequency", "SchedulerOutputSet", "CreatedBy" };
        List<string> lstColumnEdit = new List<string>() { "ConfigurationId", "ProjectId", "ModuleId", "TestCaseBuildId", "InputFileTypeId", "InputFilePath", "InputFileFormat", "InputFileSet", "OutputFileTypeId", "OutputFilePath", "OutputFileFormat", "OutputFileSet", "SchedulerInputStartDate", "SchedulerInputStartTime", "SchedulerInputEndDate", "SchedulerInputEndTime", "SchedulerInputFrequency", "SchedulerInputSet", "SchedulerOutputStartDate", "SchedulerOutputStartTime", "SchedulerOutputEndDate", "SchedulerOutputEndTime", "SchedulerOutputFrequency", "SchedulerOutputSet", "ModifiedBy" };
        List<string> lstColumns = new List<string>();
        List<string> lstValues = new List<string>();
        List<string> parameterNameList = new List<string>();
        List<string> parameterValueList = new List<string>();
        string strTableName = "Configuration";
        string strFileRefTableName = "ConfigurationFile";
        string strTableRefTableName = "ConfigurationTable";
        #endregion

        #region public ActionResult Index()
        /// <summary>
        /// Action/View returs the details of Configuration
        /// </summary> 
        /// <returns>List the details</returns>
        public ActionResult Index()
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewList = new List<Configuration>();

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Configuration", "Index", "" };
                InsertErrorAppLog(lstValues, 2);

                Result<DataTable> spResult = dataAccess.ExecuteSp("USP_SelectAllConfiguration");
                if (spResult.isSuccessful)
                {
                    viewList = (from DataRow row in spResult.responseData.Rows
                                select new Configuration
                                {
                                    ConfigurationId = Convert.ToInt32(row["ConfigurationId"]),
                                    ProjectId = Convert.ToInt32(row["ProjectId"]),
                                    ProjectName = Convert.ToString(row["ProjectName"]),
                                    ModuleId = Convert.ToInt32(row["ModuleId"]),
                                    ModuleName = Convert.ToString(row["ModuleName"]),
                                    TestCaseBuildId = Convert.ToInt32(row["TestCaseBuildId"]),
                                    TestCaseBuildName = Convert.ToString(row["TestCaseBuildName"])
                                }).ToList();
                }
                else
                {
                    ErrorMessage = spResult.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Configuration", "Index", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Configuration", "Index", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(Views.Index, viewList);
        }
        #endregion

        #region public ActionResult AddEdit(int configurationId)
        /// <summary>
        /// Save/Edit the Configuration
        /// </summary>
        /// <param name="configurationId">The Configuration  id</param>
        /// <returns></returns>
        public ActionResult AddEdit(int configurationId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewData = new Configuration();
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Configuration", "AddEdit-Get", "Id=" + configurationId };
                InsertErrorAppLog(lstValues, 2);

                viewData.SchedulerInputStartDate = DateTime.Now;
                viewData.SchedulerInputEndDate = DateTime.Now;
                viewData.SchedulerOutputStartDate = DateTime.Now;
                viewData.SchedulerOutputEndDate = DateTime.Now;

                viewData.SchedulerInputFrequency = "10";
                viewData.SchedulerOutputFrequency = "10";

                if (configurationId > 0)
                {
                    Result<DataTable> result = dataAccess.SelectById(configurationId, lstColumnEdit, strTableName);

                    if (!result.isSuccessful)
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Configuration", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }
                    else
                    {
                        if (result.responseData.Rows.Count > 0)
                        {
                            viewData.ConfigurationId = Convert.ToInt32(result.responseData.Rows[0]["ConfigurationId"]);
                            viewData.ProjectId = Convert.ToInt32(result.responseData.Rows[0]["ProjectId"]);
                            viewData.ModuleId = Convert.ToInt32(result.responseData.Rows[0]["ModuleId"]);
                            viewData.TestCaseBuildId = Convert.ToInt32(result.responseData.Rows[0]["TestCaseBuildId"]);
                            viewData.InputFileTypeId = Convert.ToInt32(result.responseData.Rows[0]["InputFileTypeId"]);
                            viewData.InputFilePath = Convert.ToString(result.responseData.Rows[0]["InputFilePath"]);
                            viewData.InputFileFormat = Convert.ToString(result.responseData.Rows[0]["InputFileFormat"]);
                            viewData.InputFileSet = Convert.ToBoolean(result.responseData.Rows[0]["InputFileSet"]);
                            viewData.OutputFileTypeId = Convert.ToInt32(result.responseData.Rows[0]["OutputFileTypeId"]);
                            viewData.OutputFilePath = Convert.ToString(result.responseData.Rows[0]["OutputFilePath"]);
                            viewData.OutputFileFormat = Convert.ToString(result.responseData.Rows[0]["OutputFileFormat"]);
                            viewData.OutputFileSet = Convert.ToBoolean(result.responseData.Rows[0]["OutputFileSet"]);
                            viewData.SchedulerInputStartDate = Convert.ToDateTime(result.responseData.Rows[0]["SchedulerInputStartDate"]);
                            viewData.SchedulerInputStartTime = Convert.ToString(result.responseData.Rows[0]["SchedulerInputStartTime"]);
                            viewData.SchedulerInputEndDate = Convert.ToDateTime(result.responseData.Rows[0]["SchedulerInputEndDate"]);
                            viewData.SchedulerInputEndTime = Convert.ToString(result.responseData.Rows[0]["SchedulerInputEndTime"]);
                            viewData.SchedulerInputFrequency = Convert.ToString(result.responseData.Rows[0]["SchedulerInputFrequency"]);
                            viewData.SchedulerInputSet = Convert.ToBoolean(result.responseData.Rows[0]["SchedulerInputSet"]);
                            viewData.SchedulerOutputStartDate = Convert.ToDateTime(result.responseData.Rows[0]["SchedulerOutputStartDate"]);
                            viewData.SchedulerOutputStartTime = Convert.ToString(result.responseData.Rows[0]["SchedulerOutputStartTime"]);
                            viewData.SchedulerOutputEndDate = Convert.ToDateTime(result.responseData.Rows[0]["SchedulerOutputEndDate"]);
                            viewData.SchedulerOutputEndTime = Convert.ToString(result.responseData.Rows[0]["SchedulerOutputEndTime"]);
                            viewData.SchedulerOutputFrequency = Convert.ToString(result.responseData.Rows[0]["SchedulerOutputFrequency"]);
                            viewData.SchedulerOutputSet = Convert.ToBoolean(result.responseData.Rows[0]["SchedulerOutputSet"]);
                        }

                        Result<DataTable> staticFile = dataAccess.SelectByQuery("SELECT FileTypeId, FilePath, FileNameFormat, IsSet,IsInput FROM ConfigurationFile WHERE ConfigurationId = " + configurationId);
                        if (staticFile.isSuccessful)
                        {
                            if (staticFile.responseData.Rows.Count > 0)
                            {
                                viewData.InputFileList = new List<InputOutputFile>();
                                viewData.InputFileList = (from DataRow row in staticFile.responseData.Rows
                                                          where row.Field<bool>("IsInput") == true
                                                          select new InputOutputFile
                                                          {
                                                              FileTypeId = Convert.ToInt32(row["FileTypeId"]),
                                                              FilePath = Convert.ToString(row["FilePath"]),
                                                              FileNameFormat = Convert.ToString(row["FileNameFormat"]),
                                                              Set = Convert.ToBoolean(row["IsSet"]),
                                                              FileTypeList = GetDropdownRecordsByWhere(Convert.ToString(row["FileTypeId"]), false, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0"),
                                                          }).ToList();

                                viewData.OutputFileList = new List<InputOutputFile>();
                                viewData.OutputFileList = (from DataRow row in staticFile.responseData.Rows
                                                           where row.Field<bool>("IsInput") == false
                                                           select new InputOutputFile
                                                           {
                                                               FileTypeId = Convert.ToInt32(row["FileTypeId"]),
                                                               FilePath = Convert.ToString(row["FilePath"]),
                                                               FileNameFormat = Convert.ToString(row["FileNameFormat"]),
                                                               Set = Convert.ToBoolean(row["IsSet"]),
                                                               FileTypeList = GetDropdownRecordsByWhere(Convert.ToString(row["FileTypeId"]), false, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM filetypeconfiguration WHERE IsDeleted = 0"),
                                                           }).ToList();
                            }
                        }
                        else
                        {
                            ErrorMessage = staticFile.message;
                            //To insert the error log 
                            lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Configuration", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                            InsertErrorAppLog(lstValues, 1);

                            viewData.TableTypeList = GetDropdownRecordsByWhere("", true, "SELECT TableTypeId,TableTypeName FROM TableTypes WHERE IsDeleted = 0");
                            viewData.FileTypeList = GetDropdownRecordsByWhere("", true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
                            viewData.InputFileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.InputFileTypeId), true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
                            viewData.OutputFileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.OutputFileTypeId), true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
                            viewData.TestCaseBuildList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TestCaseBuildId), true, "SELECT TestCaseBuildId,TestCaseBuildName FROM TestCaseBuild WHERE IsDeleted = 0");
                            viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM Project WHERE IsDeleted = 0");
                            viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM Module WHERE IsDeleted = 0");

                            return View(Views.AddEdit, viewData);
                        }

                        Result<DataTable> staticTable = dataAccess.SelectByQuery("SELECT TableTypeId, TableName, IsInput FROM ConfigurationTable WHERE ConfigurationId = " + configurationId);
                        if (staticTable.isSuccessful)
                        {
                            if (staticTable.responseData.Rows.Count > 0)
                            {
                                viewData.InputTableList = new List<InputOutputTable>();
                                viewData.InputTableList = (from DataRow row in staticTable.responseData.Rows
                                                           where row.Field<bool>("IsInput") == true
                                                           select new InputOutputTable
                                                           {
                                                               TableTypeId = Convert.ToInt32(row["TableTypeId"]),
                                                               TableName = Convert.ToString(row["TableName"]),
                                                               IOTableTypeList = GetDropdownRecordsByWhere(Convert.ToString(row["TableTypeId"]), false, "SELECT TableTypeId,TableTypeName FROM TableTypes WHERE IsDeleted = 0"),
                                                           }).ToList();

                                viewData.OutputTableList = new List<InputOutputTable>();
                                viewData.OutputTableList = (from DataRow row in staticTable.responseData.Rows
                                                            where row.Field<bool>("IsInput") == false
                                                            select new InputOutputTable
                                                            {
                                                                TableTypeId = Convert.ToInt32(row["TableTypeId"]),
                                                                TableName = Convert.ToString(row["TableName"]),
                                                                IOTableTypeList = GetDropdownRecordsByWhere(Convert.ToString(row["TableTypeId"]), false, "SELECT TableTypeId,TableTypeName FROM TableTypes WHERE IsDeleted = 0"),
                                                            }).ToList();
                            }
                        }
                        else
                        {
                            ErrorMessage = staticFile.message;
                            //To insert the error log 
                            lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Configuration", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                            InsertErrorAppLog(lstValues, 1);

                            viewData.TableTypeList = GetDropdownRecordsByWhere("", true, "SELECT TableTypeId,TableTypeName FROM TableTypes WHERE IsDeleted = 0");
                            viewData.FileTypeList = GetDropdownRecordsByWhere("", true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
                            viewData.InputFileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.InputFileTypeId), true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
                            viewData.OutputFileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.OutputFileTypeId), true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
                            viewData.TestCaseBuildList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TestCaseBuildId), true, "SELECT TestCaseBuildId,TestCaseBuildName FROM TestCaseBuild WHERE IsDeleted = 0");
                            viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM Project WHERE IsDeleted = 0");
                            viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM Module WHERE IsDeleted = 0");

                            return View(Views.AddEdit, viewData);
                        }
                    }
                }

                viewData.TableTypeList = GetDropdownRecordsByWhere("", true, "SELECT TableTypeId,TableTypeName FROM TableTypes WHERE IsDeleted = 0");
                viewData.FileTypeList = GetDropdownRecordsByWhere("", true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
                viewData.InputFileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.InputFileTypeId), true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
                viewData.OutputFileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.OutputFileTypeId), true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
                viewData.TestCaseBuildList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TestCaseBuildId), true, "SELECT TestCaseBuildId,TestCaseBuildName FROM TestCaseBuild WHERE IsDeleted = 0");
                viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM Project WHERE IsDeleted = 0");
                viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM Module WHERE IsDeleted = 0");

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Configuration", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);

                viewData.TableTypeList = GetDropdownRecordsByWhere("", true, "SELECT TableTypeId,TableTypeName FROM TableTypes WHERE IsDeleted = 0");
                viewData.FileTypeList = GetDropdownRecordsByWhere("", true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
                viewData.InputFileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.InputFileTypeId), true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
                viewData.OutputFileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.OutputFileTypeId), true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
                viewData.TestCaseBuildList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TestCaseBuildId), true, "SELECT TestCaseBuildId,TestCaseBuildName FROM TestCaseBuild WHERE IsDeleted = 0");
                viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM Project WHERE IsDeleted = 0");
                viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM Module WHERE IsDeleted = 0");

            }

            return View(Views.AddEdit, viewData);
        }

        #endregion

        #region public ActionResult AddEdit(Configuration viewData)
        /// <summary>
        ///  Save 
        /// </summary>
        /// <param name="viewData">Sending the Configuration values</param>
        /// <returns></returns>
        [HttpPost]
        [System.Web.Mvc.ValidateInput(false)]
        public ActionResult AddEdit(Configuration viewData)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                string objString = JsonConvert.SerializeObject(viewData);
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Configuration", "AddEdit-Post", objString.Replace("\"", "").Replace("{", "").Replace("}", "") };
                InsertErrorAppLog(lstValues, 2);

                int configId = viewData.ConfigurationId;
                if (ModelState.IsValid)
                {
                    var result = new Result<Int64>
                    {
                        isSuccessful = true,
                        message = default(string),
                        responseData = default(Int64)
                    };

                    //Check Configuration already exist
                    string qry = "";
                    if (viewData.ConfigurationId > 0)
                    {
                        qry = "SELECT ConfigurationId FROM Configuration WHERE IsDeleted = 0 AND ConfigurationId <> " + viewData.ConfigurationId + " AND ProjectId = " + viewData.ProjectId + " AND ModuleId = " + viewData.ModuleId + " AND TestCaseBuildId = " + viewData.TestCaseBuildId;
                    }
                    else
                    {
                        qry = "SELECT ConfigurationId FROM Configuration WHERE IsDeleted = 0 AND ProjectId = " + viewData.ProjectId + " AND ModuleId = " + viewData.ModuleId + " AND TestCaseBuildId = " + viewData.TestCaseBuildId;
                    }
                    int chkResult = dataAccess.SelectIntValueByQuery(qry);
                    if (chkResult < 0)
                    {

                        viewData.TableTypeList = GetDropdownRecordsByWhere("", true, "SELECT TableTypeId,TableTypeName FROM TableTypes WHERE IsDeleted = 0");
                        viewData.FileTypeList = GetDropdownRecordsByWhere("", true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
                        viewData.InputFileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.InputFileTypeId), true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
                        viewData.OutputFileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.OutputFileTypeId), true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
                        viewData.TestCaseBuildList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TestCaseBuildId), true, "SELECT TestCaseBuildId,TestCaseBuildName FROM TestCaseBuild WHERE IsDeleted = 0");
                        viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM Project WHERE IsDeleted = 0");
                        viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM Module WHERE IsDeleted = 0");


                        ErrorMessage = UserMessages.COMMON_ERROR_MESSAGE;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Configuration", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);

                        return View(viewData);
                    }
                    else if (chkResult > 0 && chkResult != viewData.ConfigurationId)
                    {
                        ErrorMessage = "Configuration already exists for the selected entries. Duplicate entries not allowed.";

                        viewData.TableTypeList = GetDropdownRecordsByWhere("", true, "SELECT TableTypeId,TableTypeName FROM TableTypes WHERE IsDeleted = 0");
                        viewData.FileTypeList = GetDropdownRecordsByWhere("", true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
                        viewData.InputFileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.InputFileTypeId), true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
                        viewData.OutputFileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.OutputFileTypeId), true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
                        viewData.TestCaseBuildList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TestCaseBuildId), true, "SELECT TestCaseBuildId,TestCaseBuildName FROM TestCaseBuild WHERE IsDeleted = 0");
                        viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM Project WHERE IsDeleted = 0");
                        viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM Module WHERE IsDeleted = 0");

                        return View(viewData);
                    }

                    if (viewData.ConfigurationId > 0)
                    {
                        lstValues = new List<string>() { Convert.ToString(viewData.ConfigurationId), Convert.ToString(viewData.ProjectId), Convert.ToString(viewData.ModuleId), Convert.ToString(viewData.TestCaseBuildId), Convert.ToString(viewData.InputFileTypeId), viewData.InputFilePath.Replace(@"\", @"\\").Replace("'", @"\'"), viewData.InputFileFormat, (viewData.InputFileSet == true ? "1" : "0"), Convert.ToString(viewData.OutputFileTypeId), viewData.OutputFilePath.Replace(@"\", @"\\").Replace("'", @"\'"), viewData.OutputFileFormat, (viewData.OutputFileSet == true ? "1" : "0"), viewData.SchedulerInputStartDate.ToString("yyyy-MM-dd"), viewData.SchedulerInputStartTime, viewData.SchedulerInputEndDate.ToString("yyyy-MM-dd"), viewData.SchedulerInputEndTime, viewData.SchedulerInputFrequency, (viewData.SchedulerInputSet == true ? "1" : "0"), viewData.SchedulerOutputStartDate.ToString("yyyy-MM-dd"), viewData.SchedulerOutputStartTime, viewData.SchedulerOutputEndDate.ToString("yyyy-MM-dd"), viewData.SchedulerOutputEndTime, viewData.SchedulerOutputFrequency, (viewData.SchedulerOutputSet == true ? "1" : "0"), Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Update(lstColumnEdit, lstValues, strTableName);
                    }
                    else
                    {
                        lstValues = new List<string>() { Convert.ToString(viewData.ProjectId), Convert.ToString(viewData.ModuleId), Convert.ToString(viewData.TestCaseBuildId), Convert.ToString(viewData.InputFileTypeId), viewData.InputFilePath.Replace(@"\", @"\\").Replace("'", @"\'"), viewData.InputFileFormat, (viewData.InputFileSet == true ? "1" : "0"), Convert.ToString(viewData.OutputFileTypeId), viewData.OutputFilePath.Replace(@"\", @"\\").Replace("'", @"\'"), viewData.OutputFileFormat, (viewData.OutputFileSet == true ? "1" : "0"), viewData.SchedulerInputStartDate.ToString("yyyy-MM-dd"), viewData.SchedulerInputStartTime, viewData.SchedulerInputEndDate.ToString("yyyy-MM-dd"), viewData.SchedulerInputEndTime, viewData.SchedulerInputFrequency, (viewData.SchedulerInputSet == true ? "1" : "0"), viewData.SchedulerOutputStartDate.ToString("yyyy-MM-dd"), viewData.SchedulerOutputStartTime, viewData.SchedulerOutputEndDate.ToString("yyyy-MM-dd"), viewData.SchedulerOutputEndTime, viewData.SchedulerOutputFrequency, (viewData.SchedulerOutputSet == true ? "1" : "0"), Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Insert(lstColumnsAdd, lstValues, strTableName);

                        if (result.isSuccessful)
                            configId = Convert.ToInt32(result.responseData);
                    }

                    CanClearMessage = false;
                    if (result.isSuccessful)
                    {
                        //To save the child records
                        if (configId > 0)
                        {

                            lstColumns = new List<string>() { "ConfigurationId" };
                            lstValues = new List<string>() { Convert.ToString(configId) };
                            Result<Int64> fileRefDelete = dataAccess.Delete(false, lstColumns, lstValues, strFileRefTableName);
                            if (!fileRefDelete.isSuccessful)
                            {
                                ErrorMessage = fileRefDelete.message;
                                return RedirectToAction(Actions.Index);
                            }

                            viewData.InputFileList = new List<InputOutputFile>();
                            if (!string.IsNullOrEmpty(viewData.strInputFileList))
                            {
                                viewData.InputFileList = new List<InputOutputFile>(Newtonsoft.Json.JsonConvert.DeserializeObject<InputOutputFile[]>(viewData.strInputFileList));
                            }

                            viewData.OutputFileList = new List<InputOutputFile>();
                            if (!string.IsNullOrEmpty(viewData.strOutputFileList))
                            {
                                viewData.OutputFileList = new List<InputOutputFile>(Newtonsoft.Json.JsonConvert.DeserializeObject<InputOutputFile[]>(viewData.strOutputFileList));
                            }

                            var inputOutputFileList = new List<InputOutputFile>();
                            inputOutputFileList.AddRange(viewData.InputFileList);
                            inputOutputFileList.AddRange(viewData.OutputFileList);
                            foreach (var item in inputOutputFileList)
                            {
                                lstColumns = new List<string>() { "ConfigurationId", "FileTypeId", "FilePath", "FileNameFormat", "IsSet", "IsInput", "CreatedBy" };
                                lstValues = new List<string>() { Convert.ToString(configId), Convert.ToString(item.FileTypeId), item.FilePath, item.FileNameFormat, (item.Set == true ? "1" : "0"), (item.IsInput == true ? "1" : "0"), Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                                result = dataAccess.Insert(lstColumns, lstValues, strFileRefTableName);
                                if (!result.isSuccessful)
                                {
                                    ErrorMessage = result.message;
                                    return RedirectToAction(Actions.Index);
                                }
                            }


                            lstColumns = new List<string>() { "ConfigurationId" };
                            lstValues = new List<string>() { Convert.ToString(configId) };
                            Result<Int64> tableRefDelete = dataAccess.Delete(false, lstColumns, lstValues, strTableRefTableName);
                            if (!tableRefDelete.isSuccessful)
                            {
                                ErrorMessage = tableRefDelete.message;
                                return RedirectToAction(Actions.Index);
                            }

                            viewData.InputTableList = new List<InputOutputTable>();
                            if (!string.IsNullOrEmpty(viewData.strInputTableList))
                            {
                                viewData.InputTableList = new List<InputOutputTable>(Newtonsoft.Json.JsonConvert.DeserializeObject<InputOutputTable[]>(viewData.strInputTableList));
                            }

                            viewData.OutputTableList = new List<InputOutputTable>();
                            if (!string.IsNullOrEmpty(viewData.strOutputTableList))
                            {
                                viewData.OutputTableList = new List<InputOutputTable>(Newtonsoft.Json.JsonConvert.DeserializeObject<InputOutputTable[]>(viewData.strOutputTableList));
                            }

                            var inputOutputTableList = new List<InputOutputTable>();
                            inputOutputTableList.AddRange(viewData.InputTableList);
                            inputOutputTableList.AddRange(viewData.OutputTableList);

                            foreach (var item in inputOutputTableList)
                            {
                                lstColumns = new List<string>() { "ConfigurationId", "TableTypeId", "TableName", "IsInput", "CreatedBy" };
                                lstValues = new List<string>() { Convert.ToString(configId), Convert.ToString(item.TableTypeId), item.TableName, (item.IsInput == true ? "1" : "0"), Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                                result = dataAccess.Insert(lstColumns, lstValues, strTableRefTableName);
                                if (!result.isSuccessful)
                                {
                                    ErrorMessage = result.message;
                                    return RedirectToAction(Actions.Index);
                                }
                            }

                        }


                        if (viewData.ConfigurationId > 0)
                        {
                            SuccessMessage = UserMessages.UPDATE_SUCCESS;
                        }
                        else
                        {
                            SuccessMessage = UserMessages.SAVE_SUCCESS;
                        }
                    }
                    else
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Configuration", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }

                    return RedirectToAction(Actions.Index);
                }

                var modalStateError = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                ErrorMessage = modalStateError.ToString();

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "Configuration", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            } 
            viewData.TableTypeList = GetDropdownRecordsByWhere("", true, "SELECT TableTypeId,TableTypeName FROM TableTypes WHERE IsDeleted = 0");
            viewData.FileTypeList = GetDropdownRecordsByWhere("", true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
            viewData.InputFileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.InputFileTypeId), true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
            viewData.OutputFileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.OutputFileTypeId), true, "SELECT FileTypeConfigurationId, CONCAT(GeneratedFileTypeId,' - ',GeneratedFileTypeName) AS GeneratedFileType FROM FileTypeConfiguration WHERE IsDeleted = 0");
            viewData.TestCaseBuildList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TestCaseBuildId), true, "SELECT TestCaseBuildId,TestCaseBuildName FROM TestCaseBuild WHERE IsDeleted = 0");
            viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM Project WHERE IsDeleted = 0");
            viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM Module WHERE IsDeleted = 0");


            return View(viewData);
        }
        #endregion

        #region public ActionResult Delete(int configurationId)
        /// <summary>
        /// Delete the configurationId
        /// </summary>
        /// <param name="configurationId">The configurationId</param>
        /// <returns></returns>
        public ActionResult Delete(int configurationId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "configuration", "Delete", "Id=" + configurationId };
                InsertErrorAppLog(lstValues, 2);

                //Soft Delete
                lstColumns = new List<string>() { "ConfigurationId", "IsDeleted", "ModifiedBy" };
                lstValues = new List<string>() { Convert.ToString(configurationId), "1", Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                Result<Int64> result = dataAccess.Delete(true, lstColumns, lstValues, strTableName);

                CanClearMessage = false;
                if (!result.isSuccessful)
                {
                    ErrorMessage = result.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "configuration", "Delete", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
                else
                {
                    SuccessMessage = UserMessages.DELETE_SUCCESS;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "configuration", "Delete", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return RedirectToAction(Actions.Index);
        }

        #endregion

    }
}
