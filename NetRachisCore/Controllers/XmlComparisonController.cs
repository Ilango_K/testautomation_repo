﻿#region using


using System;
using NetKathirBaseCore;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using NetRachisCore.Helpers;
using NetKathirBaseCore.Class;
using NetRachisCore.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NetRachisCore.Models.BusinessObjects;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.XmlDiffPatch;
using System.Xml;

#endregion

namespace NetRachisCore.Controllers
{
    [System.Web.Mvc.Authorize]
    //[SessionExpire]
    public class XmlComparisonController : BaseController
    {

        #region Declaration
        NetkathirBaseClass dataAccess = new NetkathirBaseClass();
        List<string> lstColumns = new List<string>();
        List<string> lstValues = new List<string>();
        List<string> parameterNameList = new List<string>();
        List<string> parameterValueList = new List<string>();
        #endregion

        #region public ActionResult XmlCompare()
        /// <summary>
        /// Action/View returs the view
        /// </summary> 
        /// <returns></returns>
        public ActionResult XmlCompare()
        {
            //return View();
            return View("XmlCompare");
        }
        #endregion

        #region public ActionResult CompareXml(XmlComparison viewData, IFormFile file)
        /// <summary>
        /// Action to compare the trade
        /// </summary> 
        /// <returns></returns>
        [HttpPost]
        public ActionResult CompareXml(XmlComparison viewData, IFormFile file)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            try
            {
                CanClearMessage = false;
                if (ModelState.IsValid)
                {
                    var result = new Result<Int64>
                    {
                        isSuccessful = true,
                        message = default(string),
                        responseData = default(Int64)
                    };

                    //To insert the application log   
                    var sessionHelper = new SessionHelper(new HttpContextAccessor());
                    string objString = JsonConvert.SerializeObject(viewData);
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "XmlComparison", "CompareXml-Post", objString.Replace("\"", "").Replace("{", "").Replace("}", "") };
                    InsertErrorAppLog(lstValues, 2);

                    string sourcePath = "D:\\GPG\\Projects\\NetRachisCore\\Documents\\SourceTrade.xml";
                    string destinationPath = "D:\\GPG\\Projects\\NetRachisCore\\Documents\\DestinationTrade.xml";

                    string filePath = string.Empty;
                    //if (file != null && file.Length > 0)
                    //{
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Uploads\\Trades\\");
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        filePath = path + "diff.xml";
                        System.Xml.XmlWriter xml_writer = System.Xml.XmlWriter.Create(filePath);

                        XmlDiff diff =
                        new XmlDiff(XmlDiffOptions.IgnoreChildOrder |
                                    XmlDiffOptions.IgnoreComments |
                                    XmlDiffOptions.IgnoreWhitespace);
                        diff.Compare("D:\\GPG\\Projects\\NetRachisCore\\Documents\\SourceTrade.xml",
                                     "D:\\GPG\\Projects\\NetRachisCore\\Documents\\DestinationTrade.xml",
                                     true,
                                     xml_writer);

                        string pattern = @">.+\s?.*</";
                        string[] source = System.IO.File.ReadAllLines(sourcePath);
                        string[] dest = System.IO.File.ReadAllLines(destinationPath);
                        System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(pattern);

                        for (int i = 0; i < source.Length; i++)
                        {
                            if (regex.IsMatch(source[i]))
                            {
                                System.Text.RegularExpressions.Match match = regex.Match(source[i]);
                                if (!match.Value.Contains("No Change"))
                                {
                                    dest[i] = System.Text.RegularExpressions.Regex.Replace(dest[i], pattern, match.Value);
                                }
                            }
                        }
                        System.IO.File.WriteAllLines(filePath, dest);


                    //} 
                    
                    return RedirectToAction("Index");
                }

                var modalStateError = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                ErrorMessage = modalStateError.ToString();

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "XmlComparison", "CompareXml-Post", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(viewData);
        }
        #endregion

        #region public ActionResult CompareXmlTestOld()
        /// <summary>
        /// Action to compare the trade
        /// </summary> 
        /// <returns></returns>
        [HttpPost]
        public ActionResult CompareXmlTestOld()
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            try
            {
                CanClearMessage = false;
                if (ModelState.IsValid)
                {
                    var result = new Result<Int64>
                    {
                        isSuccessful = true,
                        message = default(string),
                        responseData = default(Int64)
                    }; 
                    string sourcePath = "D:\\GPG\\Projects\\NetRachisCore\\Documents\\SourceTrade.xml";
                    string destinationPath = "D:\\GPG\\Projects\\NetRachisCore\\Documents\\DestinationTrade.xml";

                    string filePath = string.Empty;
                    string opfilePath = string.Empty;
                    //if (file != null && file.Length > 0)
                    //{
                    string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Uploads\\Trades\\");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    filePath = path + "diff.xml";
                    System.Xml.XmlWriter xml_writer = System.Xml.XmlWriter.Create(filePath);

                    XmlDiff diff =
                    new XmlDiff(XmlDiffOptions.IgnoreChildOrder |
                                XmlDiffOptions.IgnoreNamespaces |
                                XmlDiffOptions.IgnoreWhitespace);
                    diff.Compare(sourcePath,
                                 destinationPath,
                                 true,
                                 xml_writer);
                    xml_writer.Close();


                    opfilePath = path + "opdiff.xml";
                    XmlDocument sourceDoc = new XmlDocument(new NameTable());
                    sourceDoc.Load(sourcePath);
                    XmlTextReader diffgramReader = new XmlTextReader(filePath);

                    XmlPatch xmlpatch = new XmlPatch();
                    xmlpatch.Patch(sourceDoc, diffgramReader);

                    XmlTextWriter output = new XmlTextWriter(opfilePath, System.Text.Encoding.Unicode);
                    sourceDoc.Save(output);
                    output.Close();

                    //} 

                    return RedirectToAction("Index");
                }

                var modalStateError = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                ErrorMessage = modalStateError.ToString();

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "XmlComparison", "CompareXml-Post", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View();
        }
        #endregion

        #region public ActionResult CompareXmlTest()
        /// <summary>
        /// Action to compare the trade
        /// </summary> 
        /// <returns></returns>
        [HttpPost]
        public ActionResult CompareXmlTest()
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            try
            {
                CanClearMessage = false;
                if (ModelState.IsValid)
                {
                    var result = new Result<Int64>
                    {
                        isSuccessful = true,
                        message = default(string),
                        responseData = default(Int64)
                    };
                    string sourcePath = "D:\\GPG\\Projects\\NetRachisCore\\Documents\\SourceTrade.xml";
                    string destinationPath = "D:\\GPG\\Projects\\NetRachisCore\\Documents\\DestinationTrade.xml";

                    string filePath = string.Empty;
                    string opfilePath = string.Empty;
                    //if (file != null && file.Length > 0)
                    //{
                    string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Uploads\\Trades\\");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    filePath = path + "diff.xml";
                    System.Xml.XmlWriter xml_writer = System.Xml.XmlWriter.Create(filePath);

                    XmlDiff diff =
                    new XmlDiff(XmlDiffOptions.IgnoreChildOrder |
                                XmlDiffOptions.IgnoreNamespaces |
                                XmlDiffOptions.IgnoreWhitespace);
                    diff.Compare(sourcePath,
                                 destinationPath,
                                 true,
                                 xml_writer);
                    xml_writer.Close();


                    opfilePath = path + "opdiff.xml";
                    //XmlDocument sourceDoc = new XmlDocument(new NameTable());
                    //sourceDoc.Load(sourcePath);
                    //XmlTextReader diffgramReader = new XmlTextReader(filePath);

                    //XmlPatch xmlpatch = new XmlPatch();
                    //xmlpatch.Patch(sourceDoc, diffgramReader);

                    //XmlTextWriter output = new XmlTextWriter(opfilePath, System.Text.Encoding.Unicode);
                    //sourceDoc.Save(output);
                    //output.Close();

                    ConvertXmlWithXSLT("", "", "");

                    //} 

                    return RedirectToAction("Index");
                }

                var modalStateError = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                ErrorMessage = modalStateError.ToString();

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "XmlComparison", "CompareXml-Post", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View();
        }
        #endregion

        public void ConvertXmlWithXSLT(string xsltFile, string inputFile, string outputFile)
        {

            //
            xsltFile = "D:\\GPG\\Projects\\NetRachisCore\\Documents\\TradeXSLTFile.xslt";
            //inputFile = "D:\\GPG\\Projects\\NetRachisCore\\Documents\\SourceTrade.xml";

            // Need to change it
            inputFile = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Uploads\\Trades\\diff.xml");
            /////// Need to change it - enD

            outputFile = "D:\\GPG\\Projects\\NetRachisCore\\Documents\\NewXSLTFile.xml";
            //

            // Load the XSLT schema into the tranform object
            System.Xml.Xsl.XslCompiledTransform xslt = new System.Xml.Xsl.XslCompiledTransform(false);
            using (StreamReader srXslt = new StreamReader(xsltFile))
            {
                XmlReader readerXslt = XmlReader.Create(srXslt);
                xslt.Load(readerXslt);
            }

            // Create and open the output file
            using (FileStream fsOutput = System.IO.File.Create(outputFile))
            {
                XmlWriterSettings xmlSettings = new XmlWriterSettings();
                xmlSettings.Indent = true;
                XmlWriter writerXML = XmlTextWriter.Create(fsOutput, xmlSettings);

                // Open the input file
                using (XmlReader readerInput = XmlReader.Create(inputFile))
                {
                    xslt.Transform(readerInput, null, writerXML);
                }
            }
        }

    }
}
