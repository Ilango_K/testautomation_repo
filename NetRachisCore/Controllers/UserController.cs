﻿#region using

 
using System; 
using NetKathirBaseCore;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using NetRachisCore.Helpers;
using NetKathirBaseCore.Class;
using NetRachisCore.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http; 
using Microsoft.AspNetCore.Mvc;
using NetRachisCore.Models.BusinessObjects;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;

#endregion

namespace NetRachisCore.Controllers
{
    [System.Web.Mvc.Authorize]
    [SessionExpire]
    public class UserController : BaseController
    {

        #region Declaration
        NetkathirBaseClass dataAccess = new NetkathirBaseClass();
        List<string> lstColumnsAdd = new List<string>() { "UserName", "UserPassword", "UserEmailId", "RoleId", "ActiveStatus", "CreatedBy" };
        List<string> lstColumnEdit = new List<string>() { "AppUserId", "UserName", "UserPassword", "UserEmailId", "RoleId", "ActiveStatus", "ModifiedBy" };
        List<string> lstColumns = new List<string>();
        List<string> lstValues = new List<string>();
        List<string> parameterNameList = new List<string>();
        List<string> parameterValueList = new List<string>();
        string strTableName = "AppUser";
        #endregion

        #region public ActionResult Index()
        /// <summary>
        /// Action/View returs the details of User
        /// </summary> 
        /// <returns>List the details</returns>
        public ActionResult Index()
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewList = new List<User>();

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "User", "Index", "" };
                InsertErrorAppLog(lstValues, 2);

                Result<DataTable> spResult = dataAccess.ExecuteSp("USP_SelectAllUser");
                if (spResult.isSuccessful)
                {
                    viewList = (from DataRow row in spResult.responseData.Rows
                                select new User
                                {
                                    UserId = Convert.ToInt32(row["AppUserId"]),
                                    UserName = Convert.ToString(row["UserName"]),
                                    EmailId = Convert.ToString(row["UserEmailId"]),
                                    RoleId = Convert.ToInt32(row["RoleId"]),
                                    RoleName = Convert.ToString(row["RoleName"]),
                                    ActiveStatus = Convert.ToInt16(row["ActiveStatus"]) == 1 ? true : false,
                                }).ToList();
                }
                else
                {
                    ErrorMessage = spResult.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "User", "Index", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "User", "Index", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(Views.Index, viewList);
        }
        #endregion

        #region public ActionResult AddEdit(int userId)
        /// <summary>
        /// Save/Edit the User
        /// </summary>
        /// <param name="userId">The User  id</param>
        /// <returns></returns>
        public ActionResult AddEdit(int userId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewData = new User();
            viewData.Roles = new SelectList("");
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "User", "AddEdit-Get", "Id=" + userId };
                InsertErrorAppLog(lstValues, 2);

                if (userId > 0)
                {
                    Result<DataTable> result = dataAccess.SelectById(userId, lstColumnEdit, strTableName);

                    if (!result.isSuccessful)
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "User", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }
                    else
                    {
                        if (result.responseData.Rows.Count > 0)
                        {
                            viewData.UserId = Convert.ToInt32(result.responseData.Rows[0]["AppUserId"]);
                            viewData.UserName = Convert.ToString(result.responseData.Rows[0]["UserName"]);
                            viewData.Password = CommonHelper.Decryption(Convert.ToString(result.responseData.Rows[0]["UserPassword"]));
                            viewData.ConfirmPassword = CommonHelper.Decryption(Convert.ToString(result.responseData.Rows[0]["UserPassword"]));
                            viewData.EmailId = Convert.ToString(result.responseData.Rows[0]["UserEmailId"]);
                            viewData.RoleId = Convert.ToInt32(result.responseData.Rows[0]["RoleId"]);
                            viewData.ActiveStatus = Convert.ToInt16(result.responseData.Rows[0]["ActiveStatus"]) == 1 ? true : false;
                        }
                    }
                }
                viewData.Roles = GetDropdownRecordsByWhere(Convert.ToString(viewData.RoleId), true, "SELECT RoleId,RoleName FROM role WHERE IsDeleted = 0");
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "User", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(Views.AddEdit, viewData);
        }

        #endregion

        #region public ActionResult AddEdit(User viewData)
        /// <summary>
        ///  Save 
        /// </summary>
        /// <param name="viewData">Sending the User values</param>
        /// <returns></returns>
        [HttpPost]
        [System.Web.Mvc.ValidateInput(false)]
        public ActionResult AddEdit(User viewData)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            if (viewData == null) throw new ArgumentNullException("User");

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                string objString = JsonConvert.SerializeObject(viewData);
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "User", "AddEdit-Post", objString.Replace("\"", "").Replace("{", "").Replace("}", "") };
                InsertErrorAppLog(lstValues, 2);

                if (ModelState.IsValid)
                {
                    var result = new Result<Int64>
                    {
                        isSuccessful = true,
                        message = default(string),
                        responseData = default(Int64)
                    };
                    CanClearMessage = false;

                    if (viewData.Password != viewData.ConfirmPassword)
                    {
                        ErrorMessage = UserMessages.USER_PWD_MISMATCH;
                        viewData.Roles = GetDropdownRecordsByWhere(Convert.ToString(viewData.RoleId), true, "SELECT RoleId,RoleName FROM role WHERE IsDeleted = 0");
                        return View(viewData);
                    }

                    Result<DataTable> existResult = dataAccess.SelectByQuery("SELECT AppUserId FROM appuser WHERE UserEmailId='" + viewData.EmailId + "' AND AppUserId!=" + viewData.UserId + " AND IsDeleted = 0");
                    if (existResult.isSuccessful)
                    {
                        if (existResult.responseData.Rows.Count > 0)
                        {
                            ErrorMessage = UserMessages.EMAIL_ALREADY_EXISTS;
                            viewData.Roles = GetDropdownRecordsByWhere(Convert.ToString(viewData.RoleId), true, "SELECT RoleId,RoleName FROM role WHERE IsDeleted = 0");
                            return View(viewData);
                        }
                    }
                    else
                    {
                        ErrorMessage = existResult.message;
                        //To insert the error log
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "User", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);

                        viewData.Roles = GetDropdownRecordsByWhere(Convert.ToString(viewData.RoleId), true, "SELECT RoleId,RoleName FROM role WHERE IsDeleted = 0");
                        return View(viewData);
                    }

                    if (viewData.UserId > 0)
                    {
                        lstValues = new List<string>() { Convert.ToString(viewData.UserId), viewData.UserName, CommonHelper.Encryption(viewData.Password), viewData.EmailId, Convert.ToString(viewData.RoleId), (viewData.ActiveStatus == true ? "1" : "0"), Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Update(lstColumnEdit, lstValues, strTableName);
                    }
                    else
                    {

                        lstValues = new List<string>() { viewData.UserName, CommonHelper.Encryption(viewData.Password), viewData.EmailId, Convert.ToString(viewData.RoleId), (viewData.ActiveStatus == true ? "1" : "0"), Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Insert(lstColumnsAdd, lstValues, strTableName);
                    }

                    if (result.isSuccessful)
                    {
                        if (viewData.RoleId > 0)
                        {
                            SuccessMessage = UserMessages.UPDATE_SUCCESS;
                        }
                        else
                        {
                            SuccessMessage = UserMessages.SAVE_SUCCESS;
                        }
                    }
                    else
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "User", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }

                    return RedirectToAction(Actions.Index);
                }

                var modalStateError = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                ErrorMessage = modalStateError.ToString();
                viewData.Roles = GetDropdownRecordsByWhere(Convert.ToString(viewData.RoleId), true, "SELECT RoleId,RoleName FROM role WHERE IsDeleted = 0");
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "User", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
                viewData.Roles = GetDropdownRecordsByWhere(Convert.ToString(viewData.RoleId), true, "SELECT RoleId,RoleName FROM role WHERE IsDeleted = 0");
            }

            return View(viewData);
        }
        #endregion

        #region public ActionResult Delete(int userId)
        /// <summary>
        /// Delete the User
        /// </summary>
        /// <param name="userId">The User Id</param>
        /// <returns></returns>
        public ActionResult Delete(int userId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "User", "Delete", "Id=" + userId };
                InsertErrorAppLog(lstValues, 2);

                //Soft Delete
                lstColumns = new List<string>() { "AppUserId", "IsDeleted", "ModifiedBy" };
                lstValues = new List<string>() { Convert.ToString(userId), "1", Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                Result<Int64> result = dataAccess.Delete(true, lstColumns, lstValues, strTableName);

                CanClearMessage = false;
                if (!result.isSuccessful)
                {
                    ErrorMessage = result.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "User", "Delete", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
                else
                {
                    SuccessMessage = UserMessages.DELETE_SUCCESS;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "User", "Delete", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return RedirectToAction(Actions.Index);
        }

        #endregion

    }
}
