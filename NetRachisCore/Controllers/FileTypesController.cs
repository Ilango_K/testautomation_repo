﻿#region using

 
using System; 
using NetKathirBaseCore;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using NetRachisCore.Helpers;
using NetKathirBaseCore.Class;
using NetRachisCore.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http; 
using Microsoft.AspNetCore.Mvc;
using NetRachisCore.Models.BusinessObjects;
using Microsoft.AspNetCore.Authorization;

#endregion

namespace NetRachisCore.Controllers
{
    [System.Web.Mvc.Authorize]
    [SessionExpire]
    public class FileTypesController : BaseController
    {

        #region Declaration
        NetkathirBaseClass dataAccess = new NetkathirBaseClass();
        List<string> lstColumnsAdd = new List<string>() { "FileTypeName", "Description", "CreatedBy" };
        List<string> lstColumnEdit = new List<string>() { "FileTypeId", "FileTypeName", "Description", "ModifiedBy" };
        List<string> lstColumns = new List<string>();
        List<string> lstValues = new List<string>();
        List<string> parameterNameList = new List<string>();
        List<string> parameterValueList = new List<string>();
        string strTableName = "FileTypes";
        #endregion

        #region public ActionResult Index()
        /// <summary>
        /// Action/View returs the details of FileType
        /// </summary> 
        /// <returns>List the details</returns>
        public ActionResult Index()
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewList = new List<FileTypes>();

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypes", "Index", "" };
                InsertErrorAppLog(lstValues, 2);

                Result<DataTable> spResult = dataAccess.SelectByQuery("SELECT FileTypeId, FileTypeName, Description FROM filetypes WHERE IsDeleted = 0");
                if (spResult.isSuccessful)
                {
                    viewList = (from DataRow row in spResult.responseData.Rows
                                select new FileTypes
                                {
                                    FileTypeId = Convert.ToInt32(row["FileTypeId"]),
                                    FileTypeName = Convert.ToString(row["FileTypeName"]),
                                    Description = Convert.ToString(row["Description"])
                                }).ToList();
                }
                else
                {
                    ErrorMessage = spResult.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypes", "Index", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypes", "Index", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(Views.Index, viewList);
        }
        #endregion

        #region public ActionResult AddEdit(int fileTypeId)
        /// <summary>
        /// Save/Edit the FileType
        /// </summary>
        /// <param name="FileTypeId">The FileTypeId</param>
        /// <returns></returns>
        public ActionResult AddEdit(int fileTypeId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewData = new FileTypes();
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypes", "AddEdit-Get", "Id=" + fileTypeId };
                InsertErrorAppLog(lstValues, 2);

                if (fileTypeId > 0)
                {
                    Result<DataTable> result = dataAccess.SelectById(fileTypeId, lstColumnEdit, strTableName);

                    if (!result.isSuccessful)
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypes", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }
                    else
                    {
                        if (result.responseData.Rows.Count > 0)
                        {
                            viewData.FileTypeId = Convert.ToInt32(result.responseData.Rows[0]["FileTypeId"]);
                            viewData.FileTypeName = Convert.ToString(result.responseData.Rows[0]["FileTypeName"]);
                            viewData.Description = Convert.ToString(result.responseData.Rows[0]["Description"]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypes", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(Views.AddEdit, viewData);
        }

        #endregion

        #region public ActionResult AddEdit(FileTypes viewData)
        /// <summary>
        ///  Save 
        /// </summary>
        /// <param name="viewData">Sending the FileTypes values</param>
        /// <returns></returns>
        [HttpPost]
        [System.Web.Mvc.ValidateInput(false)]
        public ActionResult AddEdit(FileTypes viewData)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                string objString = JsonConvert.SerializeObject(viewData);
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypes", "AddEdit-Post", objString.Replace("\"", "").Replace("{", "").Replace("}", "") };
                InsertErrorAppLog(lstValues, 2);

                if (ModelState.IsValid)
                {
                    var result = new Result<Int64>
                    {
                        isSuccessful = true,
                        message = default(string),
                        responseData = default(Int64)
                    };

                    //Check FileType already exist
                    string qry = "";
                    if (viewData.FileTypeId > 0)
                    {
                        qry = "SELECT FileTypeId FROM filetypes WHERE IsDeleted = 0 AND FileTypeId <> " + viewData.FileTypeId + " AND FileTypeName = '" + viewData.FileTypeName + "'";
                    }
                    else
                    {
                        qry = "SELECT FileTypeId FROM filetypes WHERE IsDeleted = 0 AND FileTypeName = '" + viewData.FileTypeName + "'";
                    }
                    int chkResult = dataAccess.SelectIntValueByQuery(qry);
                    if (chkResult < 0)
                    {
                        ErrorMessage = UserMessages.COMMON_ERROR_MESSAGE;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypes", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);

                        return View(viewData);
                    }
                    else if (chkResult > 0 && chkResult != viewData.FileTypeId)
                    {
                        ErrorMessage = "File Type already exists. Duplicate entries not allowed.";
                        return View(viewData);
                    }

                    if (viewData.FileTypeId > 0)
                    {
                        lstValues = new List<string>() { Convert.ToString(viewData.FileTypeId), viewData.FileTypeName, viewData.Description, Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Update(lstColumnEdit, lstValues, strTableName);
                    }
                    else
                    {
                        lstValues = new List<string>() { viewData.FileTypeName, viewData.Description, Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Insert(lstColumnsAdd, lstValues, strTableName);
                    }

                    CanClearMessage = false;
                    if (result.isSuccessful)
                    {
                        if (viewData.FileTypeId > 0)
                        {
                            SuccessMessage = UserMessages.UPDATE_SUCCESS;
                        }
                        else
                        {
                            SuccessMessage = UserMessages.SAVE_SUCCESS;
                        }
                    }
                    else
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypes", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }

                    return RedirectToAction(Actions.Index);
                }

                var modalStateError = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                ErrorMessage = modalStateError.ToString();

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypes", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(viewData);
        }
        #endregion

        #region public ActionResult Delete(int fileTypeId)
        /// <summary>
        /// Delete the FileType
        /// </summary>
        /// <param name="FileTypeId">The FileTypeId</param>
        /// <returns></returns>
        public ActionResult Delete(int fileTypeId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;
            try
            {
                //To insert the application log
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypes", "Delete", "Id=" + fileTypeId };
                InsertErrorAppLog(lstValues, 2);

                //Soft Delete
                lstColumns = new List<string>() { "FileTypeId", "IsDeleted", "ModifiedBy" };
                lstValues = new List<string>() { Convert.ToString(fileTypeId), "1", Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                Result<Int64> result = dataAccess.Delete(true, lstColumns, lstValues, strTableName);

                CanClearMessage = false;
                if (!result.isSuccessful)
                {
                    ErrorMessage = result.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypes", "Delete", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
                else
                {
                    SuccessMessage = UserMessages.DELETE_SUCCESS;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypes", "Delete", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return RedirectToAction(Actions.Index);
        }

        #endregion

    }
}
