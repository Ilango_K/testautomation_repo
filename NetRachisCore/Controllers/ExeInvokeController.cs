﻿#region using

 
using System; 
using NetKathirBaseCore;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using NetRachisCore.Helpers;
using NetKathirBaseCore.Class;
using NetRachisCore.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http; 
using Microsoft.AspNetCore.Mvc;
using NetRachisCore.Models.BusinessObjects;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;

#endregion

namespace NetRachisCore.Controllers
{
    [System.Web.Mvc.Authorize]
    //[SessionExpire]
    public class ExeInvokeController : BaseController
    {

        #region Declaration
        NetkathirBaseClass dataAccess = new NetkathirBaseClass();
       List<string> lstColumns = new List<string>();
        List<string> lstValues = new List<string>();
        List<string> parameterNameList = new List<string>();
        List<string> parameterValueList = new List<string>();
        #endregion

        #region public ActionResult Index()
        /// <summary>
        /// Action/View returs the details of Module
        /// </summary> 
        /// <returns>List the details</returns>
        public ActionResult Index()
        {

            //var proc = System.Diagnostics.Process.Start("PATH to exe", "Command Line Arguments"); 
            try
            {
                //System.Diagnostics.Process.Start("D:\\Softwares\\AnyDesk.exe");
                System.Diagnostics.Process.Start("D:\\GPG\\Projects\\NetRachisCore\\Documents\\Exe\\testIMDate.file");
            }
            catch (Exception ex)
            {
                // Log error.
            }

            //// Use ProcessStartInfo class
            //ProcessStartInfo startInfo = new ProcessStartInfo();
            //startInfo.CreateNoWindow = false;
            //startInfo.UseShellExecute = false;
            //startInfo.FileName = "D:\\Softwares\\AnyDesk.exe";
            ////startInfo.FileName = "D:\\GPG\\Projects\\NetRachisCore\\Documents\\Exe\\testIMDate.file";
            //startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            //startInfo.Arguments = "";

            //try
            //{
            //    // Start the process with the info we specified.
            //    // Call WaitForExit and then the using statement will close.
            //    using (Process exeProcess = Process.Start(startInfo))
            //    {
            //        exeProcess.WaitForExit();
            //    }
            //}
            //catch (Exception)
            //{
            //    // Log error.
            //}

            return View();
        }
        #endregion 
    }
}
