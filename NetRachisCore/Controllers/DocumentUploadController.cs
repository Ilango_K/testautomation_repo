﻿#region using

 
using System; 
using NetKathirBaseCore;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using NetRachisCore.Helpers;
using NetKathirBaseCore.Class;
using NetRachisCore.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http; 
using Microsoft.AspNetCore.Mvc;
using NetRachisCore.Models.BusinessObjects;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

#endregion

namespace NetRachisCore.Controllers
{
    [System.Web.Mvc.Authorize]
    [SessionExpire]
    public class DocumentUploadController : BaseController
    {

        //private readonly IHostingEnvironment hostingEnvironment;
        //public DocumentUploadController(IHostingEnvironment environment)
        //{
        //    hostingEnvironment = environment;
        //}

        #region Declaration
        NetkathirBaseClass dataAccess = new NetkathirBaseClass();
        List<string> lstColumns = new List<string>();
        List<string> lstValues = new List<string>();

        List<string> lstColumnsTradeUploadAdd = new List<string>() { "TradeBatchNumber", "FilePath", "CreatedBy" };
        List<string> lstColumnTradeUploadEdit = new List<string>() { "TradeUploadId", "TradeBatchNumber", "FilePath", "ModifiedBy" };
        string strTradeUploadTable = "TradeUpload";
        List<string> lstColumnsTradeAdd = new List<string>() { "TickNumber", "TradeDate", "SettleDate", "BuySell", "ExternSymbol", "Ticker", "SecType", "ClientCode", "Trader", "ExchangeGroup", "Broker", "AvgPrice", "PriceCurrency", "SettleCountryCode", "Shares", "NetCost", "SecFees", "Tax", "OtherCharges", "Commission", "AccruedInterest", "ElectronicallyMatched", "MatureDate", "AvgYield", "Strategy", "CouponRate", "TradeUploadID" };
        string strTradeTable = "Trade";

        List<string> lstColumnsTransactionUploadAdd = new List<string>() { "TransactionBatchNumber", "FilePath", "CreatedBy" };
        List<string> lstColumnTransactionUploadEdit = new List<string>() { "TransactionUploadId", "TransactionBatchNumber", "FilePath", "ModifiedBy" };
        //string strTransactionUploadTable = "TransactionUpload";
        #endregion

        #region public ActionResult UploadTradeIndex()
        /// <summary>
        /// Action/View returs the details of Uploaded Trade
        /// </summary> 
        /// <returns>List the details</returns>
        public ActionResult UploadTradeIndex()
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewList = new List<DocumentUpload>();

            try
            {
                //To insert the application log
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "DocumentUpload", "UploadTradeIndex", "" };
                InsertErrorAppLog(lstValues, 2);

                Result<DataTable> qryResult = dataAccess.SelectByQuery("SELECT * FROM tradeupload WHERE IsDeleted = 0");
                if (qryResult.isSuccessful)
                {
                    viewList = (from DataRow row in qryResult.responseData.Rows
                                select new DocumentUpload
                                {
                                    UploadId = Convert.ToInt32(Convert.ToString(row["TradeUploadId"])),
                                    BatchNumber = Convert.ToString(row["TradeBatchNumber"]),
                                    FilePath = Convert.ToString(row["FilePath"]),
                                }).ToList();

                }
                else
                {
                    ErrorMessage = qryResult.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "DocumentUpload", "UploadTradeIndex", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "DocumentUpload", "UploadTradeIndex", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(viewList);
        }
        #endregion

        #region public ActionResult UploadTrade(int tradeUploadId)
        /// <summary>
        /// Action to upload the trade
        /// </summary> 
        /// <param name="tradeUploadId">The trade upload id</param>
        /// <returns></returns>
        public ActionResult UploadTrade(int tradeUploadId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewData = new DocumentUpload();
            viewData.TradeList = new List<Trade>();

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "DocumentUpload", "UploadTrade-Get", "Id=" + tradeUploadId };
                InsertErrorAppLog(lstValues, 2);

                if (tradeUploadId > 0)
                {
                    Result<DataTable> qryResult = dataAccess.SelectByQuery("SELECT * FROM trade WHERE IsDeleted = 0 AND TradeUploadID=" + tradeUploadId);
                    if (qryResult.isSuccessful)
                    {
                        viewData.TradeList = (from DataRow row in qryResult.responseData.Rows
                                              select new Trade
                                              {
                                                  TradeId = Convert.ToInt32(row["TradeId"]),
                                                  TickNumber = Convert.ToString(row["TickNumber"]),
                                                  TradeDate = Convert.ToString(row["TradeDate"]),
                                                  SettleDate = Convert.ToString(row["SettleDate"]),
                                                  BuySell = Convert.ToString(row["BuySell"]),
                                                  ExternSymbol = Convert.ToString(row["ExternSymbol"]),
                                                  Ticker = Convert.ToString(row["Ticker"]),
                                                  SecType = Convert.ToString(row["SecType"]),
                                                  ClientCode = Convert.ToString(row["ClientCode"]),
                                                  Trader = Convert.ToString(row["Trader"]),
                                                  ExchangeGroup = Convert.ToString(row["ExchangeGroup"]),
                                                  Broker = Convert.ToString(row["Broker"]),
                                                  AvgPrice = Convert.ToDecimal(row["AvgPrice"]),
                                                  PriceCurrency = Convert.ToString(row["PriceCurrency"]),
                                                  SettleCountryCode = Convert.ToString(row["SettleCountryCode"]),
                                                  Shares = Convert.ToDecimal(row["Shares"]),
                                                  NetCost = Convert.ToDecimal(row["NetCost"]),
                                                  SecFees = Convert.ToDecimal(row["Tax"]),
                                                  Tax = Convert.ToDecimal(row["TradeDate"]),
                                                  OtherCharges = Convert.ToDecimal(row["OtherCharges"]),
                                                  Commission = Convert.ToDecimal(row["Commission"]),
                                                  AccruedInterest = Convert.ToDecimal(row["AccruedInterest"]),
                                                  ElectronicallyMatched = Convert.ToString(row["ElectronicallyMatched"]),
                                                  MatureDate = Convert.ToString(row["MatureDate"]),
                                                  AvgYield = Convert.ToDecimal(row["AvgYield"]),
                                                  Strategy = Convert.ToString(row["Strategy"]),
                                                  CouponRate = Convert.ToDecimal(row["CouponRate"]),
                                                  TradeUploadID = Convert.ToInt32(row["TradeUploadID"]),
                                              }).ToList();
                    }
                    else
                    {
                        ErrorMessage = qryResult.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "DocumentUpload", "UploadTradeIndex", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "DocumentUpload", "UploadTrade-Get", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(viewData);
        }
        #endregion

        #region public ActionResult UploadTrade(DocumentUpload viewData, IFormFile file)
        /// <summary>
        /// Action to upload the trade
        /// </summary> 
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadTrade(DocumentUpload viewData, IFormFile file)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            try
            {
                CanClearMessage = false;
                if (ModelState.IsValid)
                {
                    var result = new Result<Int64>
                    {
                        isSuccessful = true,
                        message = default(string),
                        responseData = default(Int64)
                    };

                    //To insert the application log   
                    var sessionHelper = new SessionHelper(new HttpContextAccessor());
                    string objString = JsonConvert.SerializeObject(viewData);
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "DocumentUpload", "UploadTrade-Post", objString.Replace("\"", "").Replace("{", "").Replace("}", "") };
                    InsertErrorAppLog(lstValues, 2);

                    string filePath = string.Empty;
                    string extension = string.Empty;
                    if (file != null && file.Length > 0)
                    {
                        extension = Path.GetExtension(file.FileName);
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Uploads\\Trades\\");
                        //string path = Path.Combine(hostingEnvironment.WebRootPath, "Uploads\\Trades\\");
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        filePath = path + Path.GetFileName(file.FileName);
                        //file.SaveAs(filePath);
                        using (var strem = System.IO.File.Create(filePath))
                        {
                            file.CopyTo(strem);
                        } 
                        viewData.FilePath = Path.Combine((Url.Content("~/Uploads/Trades/")), Path.GetFileName(file.FileName));
                    }

                    lstValues = new List<string>() { Convert.ToString(viewData.BatchNumber), viewData.FilePath, Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                    result = dataAccess.Insert(lstColumnsTradeUploadAdd, lstValues, strTradeUploadTable);

                    if (result.isSuccessful)
                    {
                        //To convert the uploaded csv values into datatable
                        DataTable dt = new DataTable();

                        //ExcelDataReader method
                        dt = ExcelUtility.ConvertCSVtoDataTable(filePath);

                        DataColumn tradeUploadIdCol = dt.Columns.Add("TradeUploadID", typeof(Int32));
                        foreach (DataRow row in dt.Rows)
                        {
                            row["TradeUploadID"] = result.responseData;
                        }

                        string query = CommonLogic.BulkInsert(ref dt, strTradeTable, lstColumnsTradeAdd);
                        var insertRes = dataAccess.ExecuteQuery(query);
                        if (!insertRes.isSuccessful)
                        {
                            ErrorMessage = insertRes.message;
                            //To insert the error log 
                            lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "DocumentUpload", "UploadTrade-Post", ErrorMessage.Replace("'", "\"") };
                            InsertErrorAppLog(lstValues, 1);
                        }
                        else
                        {
                            SuccessMessage = UserMessages.FILE_UPLOAD_SUCCESS;
                        }
                    }
                    else
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "DocumentUpload", "UploadTrade-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }
                    return RedirectToAction("UploadTradeIndex");
                }

                var modalStateError = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                ErrorMessage = modalStateError.ToString();

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "DocumentUpload", "UploadTrade-Post", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(viewData);
        }
        #endregion

        #region public ActionResult TradeDelete(int tradeUploadId)
        /// <summary>
        /// Delete the Role
        /// </summary>
        /// <param name="tradeUploadId">The TradeUploadId Id</param>
        /// <returns></returns>
        public ActionResult TradeDelete(int tradeUploadId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "DocumentUpload", "TradeDelete", "Id=" + tradeUploadId };
                InsertErrorAppLog(lstValues, 2);

                //Soft Delete
                lstColumns = new List<string>() { "TradeUploadId", "IsDeleted", "ModifiedBy" };
                lstValues = new List<string>() { Convert.ToString(tradeUploadId), "1", Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                Result<Int64> result = dataAccess.Delete(true, lstColumns, lstValues, strTradeUploadTable);

                CanClearMessage = false;
                if (!result.isSuccessful)
                {
                    ErrorMessage = result.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "DocumentUpload", "TradeDelete", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
                else
                {
                    SuccessMessage = UserMessages.DELETE_SUCCESS;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "DocumentUpload", "TradeDelete", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return RedirectToAction("UploadTradeIndex");
        }

        #endregion

        #region public ActionResult UploadTransaction()
        /// <summary>
        /// Action to upload the transaction
        /// </summary> 
        /// <returns></returns>
        public ActionResult UploadTransaction()
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewData = new DocumentUpload();
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "DocumentUpload", "UploadTransaction-Get", "" };
                InsertErrorAppLog(lstValues, 2);

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "DocumentUpload", "UploadTransaction-Get", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(viewData);
        }
        #endregion

    }
}
