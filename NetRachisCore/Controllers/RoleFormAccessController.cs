﻿#region using

 
using System; 
using NetKathirBaseCore;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using NetRachisCore.Helpers;
using NetKathirBaseCore.Class;
using NetRachisCore.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http; 
using Microsoft.AspNetCore.Mvc;
using NetRachisCore.Models.BusinessObjects;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;

#endregion

namespace NetRachisCore.Controllers
{
    [System.Web.Mvc.Authorize]
    [SessionExpire]
    public class RoleFormAccessController : BaseController
    {

        #region Declaration
        NetkathirBaseClass dataAccess = new NetkathirBaseClass();
        List<string> lstColumnsAdd = new List<string>() { "RoleId", "MenuIds", "FormIds", "IsWriteAccess", "CreatedBy" };
        List<string> lstColumnEdit = new List<string>() { "RoleFormAccessId", "RoleId", "MenuIds", "FormIds", "IsWriteAccess", "ModifiedBy" };
        List<string> lstColumns = new List<string>();
        List<string> lstValues = new List<string>();
        List<string> parameterNameList = new List<string>();
        List<string> parameterValueList = new List<string>();
        string strTableName = "RoleFormAccess";
        #endregion

        #region public ActionResult Index()
        /// <summary>
        /// Action/View returs the details of form access for role
        /// </summary> 
        /// <returns>List the all form access for role</returns>
        public ActionResult Index()
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewList = new List<RoleFormAccess>();

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "RoleFormAccess", "Index", "" };
                InsertErrorAppLog(lstValues, 2);

                Result<DataTable> spResult = dataAccess.ExecuteSp("USP_SelectAllRoleFormAccess");
                if (spResult.isSuccessful)
                {
                    viewList = (from DataRow row in spResult.responseData.Rows
                                select new RoleFormAccess
                                {
                                    RoleFormAccessId = Convert.ToInt32(row["RoleFormAccessId"]),
                                    RoleName = Convert.ToString(row["RoleName"]),
                                    MenuNames = Convert.ToString(row["MenuNames"]),
                                    FormNames = Convert.ToString(row["FormNames"]),
                                    IsWriteAccess = Convert.ToString(row["IsWriteAccess"]),
                                }).ToList();
                }
                else
                {
                    ErrorMessage = spResult.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "RoleFormAccess", "Index", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "RoleFormAccess", "Index", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(Views.Index, viewList);
        }
        #endregion

        #region public ActionResult AddEdit(int roleFormAccessId)
        /// <summary>
        /// Save/Edit the RoleFormAccess
        /// </summary>
        /// <param name="roleFormAccessId">The RoleFormAccess  id</param>
        /// <returns></returns>
        public ActionResult AddEdit(int roleFormAccessId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewData = new RoleFormAccess();
            viewData.Roles = new SelectList("");
            viewData.Menus = new SelectList("");
            viewData.Forms = new SelectList("");
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "RoleFormAccess", "AddEdit-Get", "Id=" + roleFormAccessId };
                InsertErrorAppLog(lstValues, 2);

                viewData.Roles = GetDropdownRecordsByWhere(Convert.ToString(viewData.RoleId), false, "SELECT RoleId, RoleName FROM role WHERE IsDeleted = 0");
                viewData.Menus = GetDropdownRecordsByWhere(Convert.ToString(0), false, "SELECT MenuId, MenuName FROM menudetail WHERE IsDeleted = 0");
                viewData.Forms = GetDropdownRecordsByWhere(Convert.ToString(0), false, "SELECT FormId, FormName FROM formdetail WHERE IsDeleted = 0");

                if (roleFormAccessId > 0)
                {
                    Result<DataTable> result = dataAccess.SelectById(roleFormAccessId, lstColumnEdit, strTableName);

                    if (!result.isSuccessful)
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "RoleFormAccess", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }
                    else
                    {
                        if (result.responseData.Rows.Count > 0)
                        {
                            viewData.RoleFormAccessId = Convert.ToInt32(result.responseData.Rows[0]["RoleFormAccessId"]);
                            viewData.RoleId = Convert.ToInt32(result.responseData.Rows[0]["RoleId"]);
                            viewData.SelectedMenus = Convert.ToString(result.responseData.Rows[0]["MenuIds"]).Split(new string[] { "," }, StringSplitOptions.None);
                            viewData.SelectedForms = Convert.ToString(result.responseData.Rows[0]["FormIds"]).Split(new string[] { "," }, StringSplitOptions.None);
                            if (Convert.ToString(result.responseData.Rows[0]["IsWriteAccess"]) == "Y")
                            {
                                viewData.ChkWriteAccess = true;
                            }
                            else
                            {
                                viewData.ChkWriteAccess = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "RoleFormAccess", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(Views.AddEdit, viewData);
        }

        #endregion

        #region public ActionResult AddEdit(RoleFormAccess viewData)
        /// <summary>
        ///  Save 
        /// </summary>
        /// <param name="viewData">Sending the RoleFormAccess values</param>
        /// <returns></returns>
        [HttpPost]
        [System.Web.Mvc.ValidateInput(false)]
        public ActionResult AddEdit(RoleFormAccess viewData)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                string objString = JsonConvert.SerializeObject(viewData);
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "RoleFormAccess", "AddEdit-Post", objString.Replace("\"", "").Replace("{", "").Replace("}", "") };
                InsertErrorAppLog(lstValues, 2);

                if (ModelState.IsValid)
                {
                    var result = new Result<Int64>
                    {
                        isSuccessful = true,
                        message = default(string),
                        responseData = default(Int64)
                    };

                    string strWriteAccess = "";
                    if (viewData.ChkWriteAccess)
                    {
                        strWriteAccess = "Y";
                    }
                    else
                    {
                        strWriteAccess = "N";
                    }

                    if (viewData.RoleFormAccessId > 0)
                    {
                        lstValues = new List<string>() { Convert.ToString(viewData.RoleFormAccessId), Convert.ToString(viewData.RoleId), string.Join(",", viewData.SelectedMenus), string.Join(",", viewData.SelectedForms), strWriteAccess, Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Update(lstColumnEdit, lstValues, strTableName);
                    }
                    else
                    {
                        Result<DataTable> isExist = dataAccess.SelectByQuery("SELECT * FROM roleformaccess WHERE RoleId = " + viewData.RoleId.ToString() + " AND IsWriteAccess = '" + strWriteAccess + "'");


                        if (isExist.isSuccessful)
                        {
                            if (isExist.responseData.Rows.Count > 0)
                            {
                                string menuIds = Convert.ToString(isExist.responseData.Rows[0]["MenuIds"]) + ',' + string.Join(",", viewData.SelectedMenus);
                                string formIds = Convert.ToString(isExist.responseData.Rows[0]["FormIds"]) + ',' + string.Join(",", viewData.SelectedForms);
                                lstValues = new List<string>() { Convert.ToString(isExist.responseData.Rows[0]["RoleFormAccessId"]), Convert.ToString(viewData.RoleId), menuIds, formIds, strWriteAccess, Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                                result = dataAccess.Update(lstColumnEdit, lstValues, strTableName);
                            }
                            else
                            {
                                lstValues = new List<string>() { Convert.ToString(viewData.RoleId), string.Join(",", viewData.SelectedMenus), string.Join(",", viewData.SelectedForms), strWriteAccess, Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                                result = dataAccess.Insert(lstColumnsAdd, lstValues, strTableName);
                            }
                        }
                        else
                            throw new Exception(isExist.message);
                    }

                    CanClearMessage = false;
                    if (result.isSuccessful)
                    {
                        if (viewData.RoleFormAccessId > 0)
                        {
                            SuccessMessage = UserMessages.UPDATE_SUCCESS;
                        }
                        else
                        {
                            SuccessMessage = UserMessages.SAVE_SUCCESS;
                        }
                    }
                    else
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "RoleFormAccess", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }

                    return RedirectToAction(Actions.Index);
                }

                var modalStateError = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                ErrorMessage = modalStateError.ToString();

                viewData.Roles = GetDropdownRecordsByWhere(Convert.ToString(viewData.RoleId), false, "SELECT RoleId, RoleName FROM role WHERE IsDeleted = 0");
                viewData.Menus = GetDropdownRecordsByWhere(Convert.ToString(0), false, "SELECT MenuId, MenuName FROM menudetail WHERE IsDeleted = 0");
                viewData.Forms = GetDropdownRecordsByWhere(Convert.ToString(0), false, "SELECT FormId, FormName FROM formdetail WHERE IsDeleted = 0");

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "RoleFormAccess", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);

                viewData.Roles = GetDropdownRecordsByWhere(Convert.ToString(viewData.RoleId), false, "SELECT RoleId, RoleName FROM role WHERE IsDeleted = 0");
                viewData.Menus = GetDropdownRecordsByWhere(Convert.ToString(0), false, "SELECT MenuId, MenuName FROM menudetail WHERE IsDeleted = 0");
                viewData.Forms = GetDropdownRecordsByWhere(Convert.ToString(0), false, "SELECT FormId, FormName FROM formdetail WHERE IsDeleted = 0");
            }

            return View(viewData);
        }
        #endregion

        #region public ActionResult Delete(int roleFormAccessId)
        /// <summary>
        /// Delete the Role Form Access
        /// </summary>
        /// <param name="roleFormAccessId">The Role Form Access Id</param>
        /// <returns></returns>
        public ActionResult Delete(int roleFormAccessId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "RoleFormAccess", "Delete", "Id=" + roleFormAccessId };
                InsertErrorAppLog(lstValues, 2);

                //Soft Delete
                lstColumns = new List<string>() { "RoleFormAccessId" };
                lstValues = new List<string>() { Convert.ToString(roleFormAccessId) };
                Result<Int64> result = dataAccess.Delete(false, lstColumns, lstValues, strTableName);

                CanClearMessage = false;
                if (!result.isSuccessful)
                {
                    ErrorMessage = result.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "RoleFormAccess", "Delete", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
                else
                {
                    SuccessMessage = UserMessages.DELETE_SUCCESS;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "RoleFormAccess", "Delete", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return RedirectToAction(Actions.Index);
        }

        #endregion

        #region public JsonResult GetFormIds(string strMenuIds)
        /// <summary>
        /// To select the form details based on the selected menus
        /// </summary>
        /// <param name="strPOIds">The selected POs</param>
        /// <returns></returns>
        [System.Web.Mvc.AllowAnonymous]
        [HttpPost]
        public System.Web.Mvc.JsonResult GetFormIds(string strMenuIds)
        {
            System.Web.Mvc.JsonResult res = new System.Web.Mvc.JsonResult();
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "RoleFormAccess", "GetFormIds", "Id=" + strMenuIds };
                InsertErrorAppLog(lstValues, 2);

                Result<DataTable> result = dataAccess.SelectByQuery("SELECT FormId, FormName from formdetail where MenuId in (" + strMenuIds + ")");
                if (result.isSuccessful)
                {
                    Dictionary<string, string> forms = result.responseData.AsEnumerable().ToDictionary<DataRow, string, string>(row => row[0].ToString(),
                                           row => row[1].ToString());

                    res.Data = forms;
                }
                else
                {
                    ErrorMessage = result.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "RoleFormAccess", "GetFormIds", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
                res.JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet;
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "RoleFormAccess", "GetFormIds", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }
            return res;
        }
        #endregion

    }
}
