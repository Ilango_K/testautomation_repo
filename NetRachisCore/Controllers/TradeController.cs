﻿#region using


using System;
using NetKathirBaseCore;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using NetRachisCore.Helpers;
using NetKathirBaseCore.Class;
using NetRachisCore.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NetRachisCore.Models.BusinessObjects;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.XmlDiffPatch;
using System.Xml;
using Renci.SshNet;
using Microsoft.AspNetCore.Mvc.Rendering;

#endregion

namespace NetRachisCore.Controllers
{
    [System.Web.Mvc.Authorize]
    //[SessionExpire]
    public class TradeController : BaseController
    {

        #region Declaration
        NetkathirBaseClass dataAccess = new NetkathirBaseClass();
        List<string> lstColumns = new List<string>();
        List<string> lstValues = new List<string>();
        List<string> parameterNameList = new List<string>();
        List<string> parameterValueList = new List<string>();
        #endregion

        #region public ActionResult Index()
        /// <summary>
        /// Action/View returs the view
        /// </summary> 
        /// <returns></returns>
        public ActionResult Index()
        {
            var viewData = new Trade();
            //viewData.TradeTypeList = new SelectList("SWAP","MM","SWAPTION");

            viewData.TradeTypeList = new SelectList(new[]
                                          {
                                              new {ID="1",Name="SWAP"},
                                              new{ID="2",Name="MM"},
                                              new{ID="3",Name="SWAPTION"},
                                          },
                            "ID", "Name", 1);

            return View(viewData); 
        }
        #endregion  
     
    }
}
