﻿#region using

 
using System; 
using NetKathirBaseCore;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using NetRachisCore.Helpers;
using NetKathirBaseCore.Class;
using NetRachisCore.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http; 
using Microsoft.AspNetCore.Mvc;
using NetRachisCore.Models.BusinessObjects;
using Microsoft.AspNetCore.Authorization;

#endregion

namespace NetRachisCore.Controllers
{
    [System.Web.Mvc.Authorize]
    [SessionExpire]
    public class FileTypeConfigurationController : BaseController
    {

        #region Declaration
        NetkathirBaseClass dataAccess = new NetkathirBaseClass();
        List<string> lstColumnsAdd = new List<string>() { "ProjectId", "ModuleId", "GeneratedFileTypeId", "GeneratedFileTypeName", "FileExtnTypeId", "FileTypeId", "TableTypeId", "TableName", "IsSet", "DataRowStart", "CreatedBy" };
        List<string> lstColumnEdit = new List<string>() { "FileTypeConfigurationId", "ProjectId", "ModuleId", "GeneratedFileTypeId", "GeneratedFileTypeName", "FileExtnTypeId", "FileTypeId", "TableTypeId", "TableName", "IsSet", "DataRowStart", "ModifiedBy" };
        List<string> lstColumns = new List<string>();
        List<string> lstValues = new List<string>();
        List<string> parameterNameList = new List<string>();
        List<string> parameterValueList = new List<string>();
        string strTableName = "filetypeconfiguration";
        string strFileColRefTableName = "filecolumn";
        string strTagRefTableName = "tagcolumn";
        #endregion

        #region public ActionResult Index()
        /// <summary>
        /// Action/View returs the details of file Configuration
        /// </summary> 
        /// <returns>List the details</returns>
        public ActionResult Index()
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            var viewList = new List<FileTypeConfiguration>();

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypeConfiguration", "Index", "" };
                InsertErrorAppLog(lstValues, 2);

                Result<DataTable> spResult = dataAccess.ExecuteSp("USP_SelectAllFileTypeConfiguration");
                if (spResult.isSuccessful)
                {
                    viewList = (from DataRow row in spResult.responseData.Rows
                                select new FileTypeConfiguration
                                {
                                    FileTypeConfId = Convert.ToInt32(row["FileTypeConfigurationId"]),
                                    ProjectId = Convert.ToInt32(row["ProjectId"]),
                                    ProjectName = Convert.ToString(row["ProjectName"]),
                                    ModuleId = Convert.ToInt32(row["ModuleId"]),
                                    ModuleName = Convert.ToString(row["ModuleName"]),
                                    GeneratedFileTypeId = Convert.ToString(row["GeneratedFileTypeId"])
                                }).ToList();
                }
                else
                {
                    ErrorMessage = spResult.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypeConfiguration", "Index", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypeConfiguration", "Index", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return View(Views.Index, viewList);
        }
        #endregion

        #region public ActionResult AddEdit(int fileConfigurationId)
        /// <summary>
        /// Save/Edit the Configuration
        /// </summary>
        /// <param name="fileConfigurationId">The File Configuration  id</param>
        /// <returns></returns>
        public ActionResult AddEdit(int fileConfigurationId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;
            var viewData = new FileTypeConfiguration();
            try
            {
                //To insert the application log
                var sessionHelper = new SessionHelper(new HttpContextAccessor());     
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypeConfiguration", "AddEdit-Get", "Id=" + fileConfigurationId };
                InsertErrorAppLog(lstValues, 2);

                if (fileConfigurationId > 0)
                {
                    Result<DataTable> result = dataAccess.SelectById(fileConfigurationId, lstColumnEdit, strTableName);

                    if (!result.isSuccessful)
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypeConfiguration", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }
                    else
                    {
                        if (result.responseData.Rows.Count > 0)
                        {
                            viewData.FileTypeConfId = Convert.ToInt32(result.responseData.Rows[0]["FileTypeConfigurationId"]);
                            viewData.ProjectId = Convert.ToInt32(result.responseData.Rows[0]["ProjectId"]);
                            viewData.ModuleId = Convert.ToInt32(result.responseData.Rows[0]["ModuleId"]);
                            viewData.GeneratedFileTypeId = Convert.ToString(result.responseData.Rows[0]["GeneratedFileTypeId"]);
                            viewData.GeneratedFileTypeName = Convert.ToString(result.responseData.Rows[0]["GeneratedFileTypeName"]);
                            viewData.FileExtnTypeId = Convert.ToInt32(result.responseData.Rows[0]["FileExtnTypeId"]);
                            viewData.FileTypeId = Convert.ToInt32(result.responseData.Rows[0]["FileTypeId"]);
                            viewData.TableTypeId = Convert.ToInt32(result.responseData.Rows[0]["TableTypeId"]);
                            viewData.TableName = Convert.ToString(result.responseData.Rows[0]["TableName"]);
                            viewData.IsSet = Convert.ToBoolean(result.responseData.Rows[0]["IsSet"]);
                            viewData.DataRowStart = Convert.ToString(result.responseData.Rows[0]["DataRowStart"]);
                        }

                        Result<DataTable> fileColumnResult = dataAccess.SelectByQuery("SELECT fc.FileColumnId, fc.ColumnName, fc.DataTypeId, dt.DataType, fc.ColumnNo FROM filecolumn fc INNER JOIN datatypes dt ON dt.DataTypeId = fc.DataTypeId WHERE fc.IsDeleted = 0 AND FileTypeConfigurationId = " + fileConfigurationId);
                        if (fileColumnResult.isSuccessful)
                        {
                            viewData.FileColumnList = new List<FileColumn>();
                            viewData.FileColumnList = (from DataRow row in fileColumnResult.responseData.Rows
                                                       select new FileColumn
                                                       {
                                                           ColumnName = Convert.ToString(row["ColumnName"]),
                                                           DataTypeId = Convert.ToInt32(row["DataTypeId"]),
                                                           DataType = Convert.ToString(row["DataType"]),
                                                           ColumnNo = Convert.ToString(row["ColumnNo"]),
                                                           DataTypeList = GetDropdownRecordsByWhere(Convert.ToString(row["DataTypeId"]), false, "SELECT DataTypeId,DataType FROM datatypes WHERE IsDeleted = 0"),
                                                       }).ToList();
                        }
                        else
                        {
                            ErrorMessage = fileColumnResult.message;
                            //To insert the error log 
                            lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypeConfiguration", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                            InsertErrorAppLog(lstValues, 1);

                            viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                            viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0");
                            viewData.FileExtnTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.FileExtnTypeId), true, "SELECT FileExtnTypeId,FileExtnType FROM fileextntype WHERE IsDeleted = 0");
                            viewData.FileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.FileTypeId), true, "SELECT FileTypeId,FileTypeName FROM filetypes WHERE IsDeleted = 0");
                            viewData.TableTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TableTypeId), true, "SELECT TableTypeId,TableTypeName FROM tabletypes WHERE IsDeleted = 0");
                            viewData.DataTypeList = GetDropdownRecordsByWhere("0", true, "SELECT DataTypeId,DataType FROM datatypes WHERE IsDeleted = 0");

                            return View(Views.AddEdit, viewData);
                        }

                        Result<DataTable> tagColumnResult = dataAccess.SelectByQuery("SELECT tc.TagColumnId, tc.Tag, tc.DataTypeId, dt.DataType FROM tagcolumn tc INNER JOIN datatypes dt ON dt.DataTypeId = tc.DataTypeId WHERE tc.IsDeleted = 0 AND FileTypeConfigurationId = " + fileConfigurationId);
                        if (tagColumnResult.isSuccessful)
                        {
                            viewData.TagColumnList = new List<TagColumn>();
                            viewData.TagColumnList = (from DataRow row in tagColumnResult.responseData.Rows
                                                      select new TagColumn
                                                      {
                                                          Tag = Convert.ToString(row["Tag"]),
                                                          DataTypeId = Convert.ToInt32(row["DataTypeId"]),
                                                          DataType = Convert.ToString(row["DataType"]),
                                                          DataTypeList = GetDropdownRecordsByWhere(Convert.ToString(row["DataTypeId"]), false, "SELECT DataTypeId,DataType FROM datatypes WHERE IsDeleted = 0"),
                                                      }).ToList();
                        }
                        else
                        {
                            ErrorMessage = tagColumnResult.message;
                            //To insert the error log 
                            lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypeConfiguration", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                            InsertErrorAppLog(lstValues, 1);

                            viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                            viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0");
                            viewData.FileExtnTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.FileExtnTypeId), true, "SELECT FileExtnTypeId,FileExtnType FROM fileextntype WHERE IsDeleted = 0");
                            viewData.FileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.FileTypeId), true, "SELECT FileTypeId,FileTypeName FROM filetypes WHERE IsDeleted = 0");
                            viewData.TableTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TableTypeId), true, "SELECT TableTypeId,TableTypeName FROM tabletypes WHERE IsDeleted = 0");
                            viewData.DataTypeList = GetDropdownRecordsByWhere("0", true, "SELECT DataTypeId,DataType FROM datatypes WHERE IsDeleted = 0");

                            return View(Views.AddEdit, viewData);
                        }
                    }
                }
                else
                {
                    //To generate File Type Id 
                    Result<DataTable> fileTypeIdResult = dataAccess.ExecuteSp("USP_GenerateFileTypeId"); 
                    if (!fileTypeIdResult.isSuccessful)
                    {
                        ErrorMessage = fileTypeIdResult.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypeConfiguration", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }
                    else
                    {
                        if (fileTypeIdResult.responseData.Rows.Count > 0)
                        {
                            viewData.GeneratedFileTypeId = Convert.ToString(fileTypeIdResult.responseData.Rows[0]["NewFileTypeId"]);
                        }
                        else
                        {
                            viewData.GeneratedFileTypeId = "FileType/001";
                        }
                    }

                }

                viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0");
                viewData.FileExtnTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.FileExtnTypeId), true, "SELECT FileExtnTypeId,FileExtnType FROM fileextntype WHERE IsDeleted = 0");
                viewData.FileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.FileTypeId), true, "SELECT FileTypeId,FileTypeName FROM filetypes WHERE IsDeleted = 0");
                viewData.TableTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TableTypeId), true, "SELECT TableTypeId,TableTypeName FROM tabletypes WHERE IsDeleted = 0");
                viewData.DataTypeList = GetDropdownRecordsByWhere("0", true, "SELECT DataTypeId,DataType FROM datatypes WHERE IsDeleted = 0");

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypeConfiguration", "AddEdit-Get", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);

                viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0");
                viewData.FileExtnTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.FileExtnTypeId), true, "SELECT FileExtnTypeId,FileExtnType FROM fileextntype WHERE IsDeleted = 0");
                viewData.FileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.FileTypeId), true, "SELECT FileTypeId,FileTypeName FROM filetypes WHERE IsDeleted = 0");
                viewData.TableTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TableTypeId), true, "SELECT TableTypeId,TableTypeName FROM tabletypes WHERE IsDeleted = 0");
                viewData.DataTypeList = GetDropdownRecordsByWhere("0", true, "SELECT DataTypeId,DataType FROM datatypes WHERE IsDeleted = 0");

            }
            return View(Views.AddEdit, viewData);
        }

        #endregion

        #region public ActionResult AddEdit(FileTypeConfiguration viewData)
        /// <summary>
        ///  Save 
        /// </summary>
        /// <param name="viewData">Sending the FileTypeConfiguration values</param>
        /// <returns></returns>
        [HttpPost]
        [System.Web.Mvc.ValidateInput(false)]
        public ActionResult AddEdit(FileTypeConfiguration viewData)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;

            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                string objString = JsonConvert.SerializeObject(viewData);
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypeConfiguration", "AddEdit-Post", objString.Replace("\"", "").Replace("{", "").Replace("}", "") };
                InsertErrorAppLog(lstValues, 2);

                int fileTypeConfigId = viewData.FileTypeConfId;
                if (ModelState.IsValid)
                {
                    var result = new Result<Int64>
                    {
                        isSuccessful = true,
                        message = default(string),
                        responseData = default(Int64)
                    };

                    //Check FileTypeConfiguration already exist
                    string qry = "";
                    if (viewData.FileTypeConfId > 0)
                    {
                        qry = "SELECT FileTypeConfigurationId FROM filetypeconfiguration WHERE IsDeleted = 0 AND FileTypeConfigurationId <> " + viewData.FileTypeConfId + " AND ProjectId = " + viewData.ProjectId + " AND ModuleId = " + viewData.ModuleId + " AND GeneratedFileTypeId = '" + viewData.GeneratedFileTypeId + "'";
                    }
                    else
                    {
                        qry = "SELECT FileTypeConfigurationId FROM filetypeconfiguration WHERE IsDeleted = 0 AND ProjectId = " + viewData.ProjectId + " AND ModuleId = " + viewData.ModuleId + " AND GeneratedFileTypeId = '" + viewData.GeneratedFileTypeId + "'";
                    }
                    int chkResult = dataAccess.SelectIntValueByQuery(qry);
                    if (chkResult < 0)
                    {

                        viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                        viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0");
                        viewData.FileExtnTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.FileExtnTypeId), true, "SELECT FileExtnTypeId,FileExtnType FROM fileextntype WHERE IsDeleted = 0");
                        viewData.FileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.FileTypeId), true, "SELECT FileTypeId,FileTypeName FROM filetypes WHERE IsDeleted = 0");
                        viewData.TableTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TableTypeId), true, "SELECT TableTypeId,TableTypeName FROM tabletypes WHERE IsDeleted = 0");
                        viewData.DataTypeList = GetDropdownRecordsByWhere("0", true, "SELECT DataTypeId,DataType FROM datatypes WHERE IsDeleted = 0");

                        ErrorMessage = UserMessages.COMMON_ERROR_MESSAGE;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypeConfiguration", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);

                        return View(viewData);
                    }
                    else if (chkResult > 0 && chkResult != viewData.FileTypeConfId)
                    {
                        ErrorMessage = "File Type Configuration already exists for the selected entries. Duplicate entries not allowed.";

                        viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
                        viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0");
                        viewData.FileExtnTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.FileExtnTypeId), true, "SELECT FileExtnTypeId,FileExtnType FROM fileextntype WHERE IsDeleted = 0");
                        viewData.FileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.FileTypeId), true, "SELECT FileTypeId,FileTypeName FROM filetypes WHERE IsDeleted = 0");
                        viewData.TableTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TableTypeId), true, "SELECT TableTypeId,TableTypeName FROM tabletypes WHERE IsDeleted = 0");
                        viewData.DataTypeList = GetDropdownRecordsByWhere("0", true, "SELECT DataTypeId,DataType FROM datatypes WHERE IsDeleted = 0");

                        return View(viewData);
                    }

                    if (viewData.FileTypeConfId > 0)
                    {
                        lstValues = new List<string>() { Convert.ToString(viewData.FileTypeConfId), Convert.ToString(viewData.ProjectId), Convert.ToString(viewData.ModuleId), viewData.GeneratedFileTypeId, viewData.GeneratedFileTypeName, Convert.ToString(viewData.FileExtnTypeId), Convert.ToString(viewData.FileTypeId), Convert.ToString(viewData.TableTypeId), viewData.TableName, (viewData.IsSet == true ? "1" : "0"), viewData.DataRowStart, Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Update(lstColumnEdit, lstValues, strTableName);
                    }
                    else
                    {
                        lstValues = new List<string>() { Convert.ToString(viewData.ProjectId), Convert.ToString(viewData.ModuleId), viewData.GeneratedFileTypeId, viewData.GeneratedFileTypeName, Convert.ToString(viewData.FileExtnTypeId), Convert.ToString(viewData.FileTypeId), Convert.ToString(viewData.TableTypeId), viewData.TableName, (viewData.IsSet == true ? "1" : "0"), viewData.DataRowStart, Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                        result = dataAccess.Insert(lstColumnsAdd, lstValues, strTableName);

                        if (result.isSuccessful)
                            fileTypeConfigId = Convert.ToInt32(result.responseData);

                    }

                    CanClearMessage = false;
                    if (result.isSuccessful)
                    {
                        //To save the child records
                        if (fileTypeConfigId > 0)
                        {
                            if (!string.IsNullOrEmpty(viewData.strFileColumnList))
                            {
                                lstColumns = new List<string>() { "FileTypeConfigurationId" };
                                lstValues = new List<string>() { Convert.ToString(fileTypeConfigId) };
                                Result<Int64> fileColRefDelete = dataAccess.Delete(false, lstColumns, lstValues, strFileColRefTableName);
                                if (!fileColRefDelete.isSuccessful)
                                {
                                    ErrorMessage = fileColRefDelete.message;
                                    return RedirectToAction(Actions.Index);
                                }

                                string tableQry = "";
                                tableQry = tableQry + "DROP TABLE IF EXISTS " + viewData.TableName + ";";
                                tableQry = tableQry + "CREATE TABLE " + viewData.TableName + "(";
                                viewData.FileColumnList = new List<FileColumn>(Newtonsoft.Json.JsonConvert.DeserializeObject<FileColumn[]>(viewData.strFileColumnList));
                                foreach (var item in viewData.FileColumnList)
                                {
                                    lstColumns = new List<string>() { "FileTypeConfigurationId", "ColumnName", "DataTypeId", "ColumnNo" };
                                    lstValues = new List<string>() { Convert.ToString(fileTypeConfigId), item.ColumnName, Convert.ToString(item.DataTypeId), item.ColumnNo };
                                    result = dataAccess.Insert(lstColumns, lstValues, strFileColRefTableName);
                                    if (!result.isSuccessful)
                                    {
                                        ErrorMessage = result.message;
                                        return RedirectToAction(Actions.Index);
                                    }

                                    tableQry = tableQry + item.ColumnName + " " + item.DataType + ",";
                                }
                                tableQry = tableQry.TrimEnd(',') + ")";
                                Result<Int64> tableCreation = dataAccess.ExecuteQuery(tableQry);
                                if (!tableCreation.isSuccessful)
                                {
                                    ErrorMessage = result.message;
                                    return RedirectToAction(Actions.Index);
                                }
                            }

                            if (!string.IsNullOrEmpty(viewData.strTagColumnList))
                            {
                                lstColumns = new List<string>() { "FileTypeConfigurationId" };
                                lstValues = new List<string>() { Convert.ToString(fileTypeConfigId) };
                                Result<Int64> tagRefDelete = dataAccess.Delete(false, lstColumns, lstValues, strTagRefTableName);
                                if (!tagRefDelete.isSuccessful)
                                {
                                    ErrorMessage = tagRefDelete.message;
                                    return RedirectToAction(Actions.Index);
                                }

                                viewData.TagColumnList = new List<TagColumn>(Newtonsoft.Json.JsonConvert.DeserializeObject<TagColumn[]>(viewData.strTagColumnList));
                                foreach (var item in viewData.TagColumnList)
                                {
                                    lstColumns = new List<string>() { "FileTypeConfigurationId", "Tag", "DataTypeId" };
                                    lstValues = new List<string>() { Convert.ToString(fileTypeConfigId), item.Tag, Convert.ToString(item.DataTypeId) };
                                    result = dataAccess.Insert(lstColumns, lstValues, strTagRefTableName);
                                    if (!result.isSuccessful)
                                    {
                                        ErrorMessage = result.message;
                                        return RedirectToAction(Actions.Index);
                                    }
                                }
                            }
                        }

                        if (viewData.FileTypeConfId > 0)
                        {
                            SuccessMessage = UserMessages.UPDATE_SUCCESS;
                        }
                        else
                        {
                            SuccessMessage = UserMessages.SAVE_SUCCESS;
                        }
                    }
                    else
                    {
                        ErrorMessage = result.message;
                        //To insert the error log 
                        lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypeConfiguration", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                        InsertErrorAppLog(lstValues, 1);
                    }

                    return RedirectToAction(Actions.Index);
                }

                var modalStateError = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                ErrorMessage = modalStateError.ToString();

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypeConfiguration", "AddEdit-Post", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }
            viewData.ProjectList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ProjectId), true, "SELECT ProjectId,ProjectName FROM project WHERE IsDeleted = 0");
            viewData.ModuleList = GetDropdownRecordsByWhere(Convert.ToString(viewData.ModuleId), true, "SELECT ModuleId,ModuleName FROM module WHERE IsDeleted = 0");
            viewData.FileExtnTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.FileExtnTypeId), true, "SELECT FileExtnTypeId,FileExtnType FROM fileextntype WHERE IsDeleted = 0");
            viewData.FileTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.FileTypeId), true, "SELECT FileTypeId,FileTypeName FROM filetypes WHERE IsDeleted = 0");
            viewData.TableTypeList = GetDropdownRecordsByWhere(Convert.ToString(viewData.TableTypeId), true, "SELECT TableTypeId,TableTypeName FROM tabletypes WHERE IsDeleted = 0");
            viewData.DataTypeList = GetDropdownRecordsByWhere("0", true, "SELECT DataTypeId,DataType FROM datatypes WHERE IsDeleted = 0");

            return View(viewData);
        }
        #endregion

        #region public ActionResult Delete(int fileConfigurationId)
        /// <summary>
        /// Delete the file type configurationId
        /// </summary>
        /// <param name="fileConfigurationId">The file type configurationId</param>
        /// <returns></returns>
        public ActionResult Delete(int fileConfigurationId)
        {
            if (CanClearMessage)
                ClearMessage();
            else
                CanClearMessage = true;
            try
            {
                //To insert the application log   
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypeConfiguration", "Delete", "Id=" + fileConfigurationId };
                InsertErrorAppLog(lstValues, 2);

                //Soft Delete
                lstColumns = new List<string>() { "FileTypeConfigurationId", "IsDeleted", "ModifiedBy" };
                lstValues = new List<string>() { Convert.ToString(fileConfigurationId), "1", Convert.ToString(sessionHelper.SessionLogOnInfo().UserId) };
                Result<Int64> result = dataAccess.Delete(true, lstColumns, lstValues, strTableName);

                CanClearMessage = false;
                if (!result.isSuccessful)
                {
                    ErrorMessage = result.message;
                    //To insert the error log 
                    lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypeConfiguration", "Delete", ErrorMessage.Replace("'", "\"") };
                    InsertErrorAppLog(lstValues, 1);
                }
                else
                {
                    SuccessMessage = UserMessages.DELETE_SUCCESS;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message.ToString();
                //To insert the error log 
                var sessionHelper = new SessionHelper(new HttpContextAccessor());
                lstValues = new List<string>() { Convert.ToString(sessionHelper.SessionLogOnInfo().UserId), "FileTypeConfiguration", "Delete", ErrorMessage.Replace("'", "\"") };
                InsertErrorAppLog(lstValues, 1);
            }

            return RedirectToAction(Actions.Index);
        }

        #endregion

    }
}
