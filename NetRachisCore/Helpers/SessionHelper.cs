﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using NetRachisCore.Models.BusinessObjects;
using Newtonsoft.Json;

namespace NetRachisCore.Helpers
{
    /// <summary>
    /// Summary description for SessionHelper
    /// </summary>
    public class SessionHelper
    {
        IHttpContextAccessor accessor;
        public SessionHelper(IHttpContextAccessor accessor)
        {
            this.accessor = accessor;
        }

        #region Private Variables 
        private const string SessionUserinfo = "Session_UserInfo";
        #endregion

        #region public void SetObjectAsJson(string key, object value)
        /// <summary>
        /// To set the object value in session
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetObjectAsJson(string key, object value)
        {
            var httpContext = accessor.HttpContext;
            httpContext.Session.SetString(key, JsonConvert.SerializeObject(value));
        }
        #endregion

        #region public T GetObjectFromJson<T>(string key)
        /// <summary>
        /// To get the session value
        /// </summary>
        /// <typeparam name="T"></typeparam> 
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetObjectFromJson<T>(string key)
        {
            var httpContext = accessor.HttpContext;
            var value = httpContext.Session.GetString(key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
        #endregion

        #region public static void NewSession()

        public void NewSession()
        {
            var httpContext = accessor.HttpContext;
            if (httpContext.Session.GetString(SessionUserinfo) != null)
                httpContext.Session.SetString(SessionUserinfo, null);

            httpContext.Session.SetString(SessionUserinfo, JsonConvert.SerializeObject(new User()));
        }

        #endregion

        #region IsSessionValid 
        public bool IsSessionValid()
        {
            var httpContext = accessor.HttpContext;
            if (httpContext.Session.GetString(SessionUserinfo) == null)
                return false;

            return true;
        }

        #endregion

        #region public static User SessionLogOnInfo 
        /// <summary>
        /// Gets the session log on info.
        /// </summary>
        /// <value>
        /// The session log on info.
        /// </value> 
        public User SessionLogOnInfo()
        {
            var httpContext = accessor.HttpContext;
            string value = httpContext.Session.GetString(SessionUserinfo);
            return value == null ? default(User) : JsonConvert.DeserializeObject<User>(value);
        }

        #endregion

        #region public User SessionLogOnUserId 
        /// <summary>
        /// Gets the session log on info.
        /// </summary>
        /// <value>
        /// The session log on info.
        /// </value>
        public int SessionLogOnUserId()
        {
            var httpContext = accessor.HttpContext;
            var value = httpContext.Session.GetString(SessionUserinfo);
            return value == null ? default(int) : JsonConvert.DeserializeObject<User>(value).UserId;
        }

        #endregion

        #region public void ClearSessionStack()

        /// <summary>
        /// Clears the session stack.
        /// </summary>
        public void ClearSessionStack()
        {
            var httpContext = accessor.HttpContext;
            httpContext.Session.Remove(SessionUserinfo);
        }

        #endregion
    }
}
