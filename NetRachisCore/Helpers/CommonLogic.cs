﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetRachisCore.Helpers
{
    public class CommonLogic
    {
        /// <summary>
        /// Encrypt using DES
        /// </summary>
        /// <param name="source">The data.</param>
        /// <param name="key">Encryption Key</param>
        /// <returns>Encrypted string.</returns>
        public static string EncryptionDesCrypto(string source, string key)
        {
            return Encrypter.EncryptStringByDesCrypto(source, key);
        }

        /// <summary>
        /// Decrypt using DES
        /// </summary>
        /// <param name="source">The data.</param>
        /// <param name="key">Encryption Key.</param>
        /// <returns>Decrypted string.</returns>
        public static string DecryptionDesCrypto(string source, string key)
        {
            return Encrypter.DecryptStringByDesCrypto(source, key);
        }

        /// <summary>
        /// To insert datatable values to table
        /// </summary>
        /// <param name="table"></param>
        /// <param name="table_name"></param>
        /// <param name="lstColumns"></param>
        /// <returns></returns>
        public static String BulkInsert(ref DataTable table, String table_name, List<string> lstColumns)
        {
            try
            {
                StringBuilder queryBuilder = new StringBuilder();
                DateTime dt;

                queryBuilder.AppendFormat("INSERT INTO {0} (", table_name);

                // more than 1 column required and 1 or more rows
                if (table.Columns.Count > 1 && table.Rows.Count > 0)
                {
                    // build all columns
                    //queryBuilder.AppendFormat("`{0}`", table.Columns[0].ColumnName);

                    //if (table.Columns.Count > 1)
                    //{
                    //    for (int i = 1; i < table.Columns.Count; i++)
                    //    {
                    //        queryBuilder.AppendFormat(", `{0}` ", table.Columns[i].ColumnName);
                    //    }
                    //}

                    queryBuilder.AppendFormat("{0}", lstColumns[0].ToString());
                    for (int i = 1; i < lstColumns.Count; i++)
                    {
                        queryBuilder.AppendFormat(", {0} ", lstColumns[i].ToString());
                    }

                    queryBuilder.AppendFormat(") VALUES (", table_name);

                    // build all values for the first row
                    // escape String & Datetime values!
                    if (table.Columns[0].DataType == typeof(String))
                    {
                        queryBuilder.AppendFormat("'{0}'", table.Rows[0][table.Columns[0].ColumnName].ToString().Replace(@"\", @"\\").Replace("'", @"\'"));
                    }
                    else if (table.Columns[0].DataType == typeof(DateTime))
                    {
                        dt = (DateTime)table.Rows[0][table.Columns[0].ColumnName];
                        queryBuilder.AppendFormat("'{0}'", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                    }
                    else if (table.Columns[0].DataType == typeof(Int32))
                    {
                        queryBuilder.AppendFormat("{0}", table.Rows[0].Field<Int32?>(table.Columns[0].ColumnName) ?? 0);
                    }
                    else if (table.Columns[0].DataType == typeof(Double))
                    {
                        queryBuilder.AppendFormat("'{0}'", table.Rows[0].Field<Double?>(table.Columns[0].ColumnName) ?? 0);
                    }
                    else
                    {
                        queryBuilder.AppendFormat("'{0}'", table.Rows[0][table.Columns[0].ColumnName].ToString());
                    }

                    for (int i = 1; i < table.Columns.Count; i++)
                    {
                        // escape String & Datetime values!
                        if (table.Columns[i].DataType == typeof(String))
                        {
                            queryBuilder.AppendFormat(", '{0}'", table.Rows[0][table.Columns[i].ColumnName].ToString().Replace(@"\", @"\\").Replace("'", @"\'"));
                        }
                        else if (table.Columns[i].DataType == typeof(DateTime))
                        {
                            dt = (DateTime)table.Rows[0][table.Columns[i].ColumnName];
                            queryBuilder.AppendFormat(", '{0}'", dt.ToString("yyyy-MM-dd HH:mm:ss"));

                        }
                        else if (table.Columns[i].DataType == typeof(Int32))
                        {
                            queryBuilder.AppendFormat(", {0}", table.Rows[0].Field<Int32?>(table.Columns[i].ColumnName) ?? 0);
                        }
                        else if (table.Columns[i].DataType == typeof(Double))
                        {
                            queryBuilder.AppendFormat(", '{0}'", table.Rows[0].Field<Double?>(table.Columns[i].ColumnName) ?? 0);
                        }
                        else
                        {
                            queryBuilder.AppendFormat(", '{0}'", table.Rows[0][table.Columns[i].ColumnName].ToString());
                        }
                    }

                    queryBuilder.Append(")");
                    queryBuilder.AppendLine();

                    // build all values all remaining rows
                    if (table.Rows.Count > 1)
                    {
                        // iterate over the rows
                        for (int row = 1; row < table.Rows.Count; row++)
                        {
                            // open value block
                            queryBuilder.Append(", (");

                            // escape String & Datetime values!
                            if (table.Columns[0].DataType == typeof(String))
                            {
                                queryBuilder.AppendFormat("'{0}'", table.Rows[row][table.Columns[0].ColumnName].ToString().Replace(@"\", @"\\").Replace("'", @"\'"));
                            }
                            else if (table.Columns[0].DataType == typeof(DateTime))
                            {
                                dt = (DateTime)table.Rows[row][table.Columns[0].ColumnName];
                                queryBuilder.AppendFormat("'{0}'", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                            }
                            else if (table.Columns[0].DataType == typeof(Int32))
                            {
                                queryBuilder.AppendFormat("{0}", table.Rows[row].Field<Int32?>(table.Columns[0].ColumnName) ?? 0);
                            }
                            else if (table.Columns[0].DataType == typeof(Double))
                            {
                                queryBuilder.AppendFormat("'{0}'", table.Rows[row].Field<Double?>(table.Columns[0].ColumnName) ?? 0);
                            }
                            else
                            {
                                queryBuilder.AppendFormat("'{0}'", table.Rows[row][table.Columns[0].ColumnName].ToString());
                            }

                            for (int col = 1; col < table.Columns.Count; col++)
                            {
                                // escape String & Datetime values!
                                if (table.Columns[col].DataType == typeof(String))
                                {
                                    queryBuilder.AppendFormat(", '{0}'", table.Rows[row][table.Columns[col].ColumnName].ToString().Replace(@"\", @"\\").Replace("'", @"\'"));
                                }
                                else if (table.Columns[col].DataType == typeof(DateTime))
                                {
                                    dt = (DateTime)table.Rows[row][table.Columns[col].ColumnName];
                                    queryBuilder.AppendFormat(", '{0}'", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                                }
                                else if (table.Columns[col].DataType == typeof(Int32))
                                {
                                    queryBuilder.AppendFormat(", {0}", table.Rows[row].Field<Int32?>(table.Columns[col].ColumnName) ?? 0);
                                }
                                else if (table.Columns[col].DataType == typeof(Int32))
                                {
                                    queryBuilder.AppendFormat(", '{0}'", table.Rows[row].Field<Int32?>(table.Columns[col].ColumnName) ?? 0);
                                }
                                else
                                {
                                    queryBuilder.AppendFormat(", '{0}'", table.Rows[row][table.Columns[col].ColumnName].ToString());
                                }
                            } // end for (int i = 1; i < table.Columns.Count; i++)

                            // close value block
                            queryBuilder.Append(")");
                            queryBuilder.AppendLine();

                        } // end for (int r = 1; r < table.Rows.Count; r++)

                        // sql delimiter =)
                        queryBuilder.Append(";");

                    } // end if (table.Rows.Count > 1)

                    return queryBuilder.ToString();
                }
                else
                {
                    return "";
                } // end if(table.Columns.Count > 1 && table.Rows.Count > 0)
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "";
            }
        }
    }
}
