﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace NetRachisCore.Helpers
{
    /// <summary>
    /// Common Helper for Web Application
    /// </summary>
    public class CommonHelper
    {
        #region Declaration

        /// <summary>
        /// Key to pull the success message
        /// </summary>
        public const string SuccessMessageKey = "_successMessage";

        /// <summary>
        /// Key to pull the error message
        /// </summary>
        public const string ErrorMessageKey = "_errorMessage";

        /// <summary>
        /// Key to flag if the message can be cleared
        /// </summary>
        public const string CanClearMessageKey = "_canClearMessage";

        #endregion

        #region Public Methods

        #region public static string GetSuccessMessage(object data)

        /// <summary>
        /// Returns the string representation of the success message object
        /// </summary>
        /// <param name="data">Success Message object</param>
        /// <returns>string</returns>
        public static string GetSuccessMessage(object data)
        {
            return data == null ? string.Empty : data.ToString();
        }

        #endregion

        #region public static string GetErrorMessage(object data)

        /// <summary>
        /// Returns the string representation of the error message object
        /// </summary>
        /// <param name="data">Error Message object</param>
        /// <returns>string</returns>
        public static string GetErrorMessage(object data)
        {
            return data == null ? string.Empty : data.ToString();
        }

        #endregion

        #region public static void SetCookieInfo(bool loggedIn, string email, string password)

        /// <summary>
        /// Set Cookie Information
        /// </summary>
        /// <param name="loggedIn">Logged In or not.</param>
        /// <param name="email">Email Id.</param>
        /// <param name="password">Password.</param>
        public static void SetCookieInfo(bool loggedIn, string email, string password)
        {
            var obj = new CookieHelper(new HttpContextAccessor());

            if (loggedIn)
            {
                obj.SetCookie("FFEmail", email);
                obj.SetCookie("FFPassword", password);
            }
            else
            {
                obj.SetCookie("FFEmail", string.Empty);
                obj.SetCookie("FFPassword", string.Empty);
            }
            obj.SetCookie("FFLoggedIn", loggedIn ? "Y" : "N");
        }

        #endregion

        #region public static void GetCookieInfo(ref bool loggedIn, ref string email, ref string password)

        /// <summary>
        /// Get LogOn Cookie information.
        /// </summary>
        /// <param name="loggedIn">Is Keep Me.</param>
        /// <param name="email">Email.</param>
        /// <param name="password">Password</param>
        public static void GetCookieInfo(ref bool loggedIn, ref string email, ref string password)
        {
            var obj = new CookieHelper(new HttpContextAccessor());
            loggedIn = false;

            if (!string.IsNullOrEmpty(obj.GetCookie("FFLoggedIn")) &&
                !string.IsNullOrEmpty(obj.GetCookie("FFEmail")) &&
                !string.IsNullOrEmpty(obj.GetCookie("FFPassword")) &&
                string.Compare(obj.GetCookie("FFLoggedIn"), "Y", true, CultureInfo.CurrentCulture) == 0)
            {
                loggedIn = true;
                email = obj.GetCookie("FFEmail");
                password = obj.GetCookie("FFPassword");
            }
        }

        #endregion   

        #region public static string Encryption(string value)

        /// <summary>
        /// Encrypts the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Encrypted string</returns>
        public static string Encryption(string value)
        {
            //return CommonLogic.EncryptionDesCrypto(value, MvcApplication.EncryptionKey);
            return CommonLogic.EncryptionDesCrypto(value, "$NetRachis!"); // Need to change from configuration file
        }

        #endregion

        #region public static string Decryption(string value)

        /// <summary>
        /// Decrypt the string value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Decrypted string</returns>
        public static string Decryption(string value)
        {
            // return CommonLogic.DecryptionDesCrypto(value, MvcApplication.EncryptionKey);
            return CommonLogic.DecryptionDesCrypto(value, "$NetRachis!"); // Need to change from configuration file
        }

        #endregion

        #endregion
    }
}
