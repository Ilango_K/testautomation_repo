﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace NetRachisCore.Helpers
{
    /// <summary>
    /// Helps to encrypt or decrypt the string or file.
    /// </summary>
    public static class Encrypter
    {
        #region Public Methods

        #region public static void EncryptFileByDesCrypto(string fileToEncrypt, string encryptedFile, string cryptoKey)

        /// <summary>
        /// Encrypts the file. It uses DESCryptoServiceProvider to encrypt.
        /// </summary>
        /// <param name="fileToEncrypt">The input filename.</param>
        /// <param name="encryptedFile">The output filename.</param>
        /// <param name="cryptoKey">The crypto key.</param>
        public static void EncryptFileByDesCrypto(string fileToEncrypt, string encryptedFile, string cryptoKey)
        {
            ICryptoTransform desEncrypt;
            using (var des = new DESCryptoServiceProvider
            {
                Key = Encoding.ASCII.GetBytes(cryptoKey),
                IV = Encoding.ASCII.GetBytes(cryptoKey)
            })
            {
                desEncrypt = des.CreateEncryptor();
            }
            using (var fsRead = new FileStream(fileToEncrypt, FileMode.Open, FileAccess.Read))
            {
                using (var fsEncrypt = new FileStream(encryptedFile, FileMode.Create, FileAccess.Write))
                {
                    using (var cryptoStream = new CryptoStream(fsEncrypt, desEncrypt, CryptoStreamMode.Write))
                    {
                        while (true)
                        {
                            var inputBytes = new byte[1024];
                            //int bytesRead = zipStream.Read(inputBytes, 0, inputBytes.Length);
                            long bytesRead = fsRead.Read(inputBytes, 0, inputBytes.Length);
                            cryptoStream.Write(inputBytes, 0, inputBytes.Length);
                            //decompress.Write(buffer, 0, bytesRead);
                            if (bytesRead != inputBytes.Length)
                            {
                                break;
                            }
                        }


                        //var inputBytes = new byte[fsRead.Length];
                        //fsRead.Read(inputBytes, 0, inputBytes.Length);
                        //cryptoStream.Write(inputBytes, 0, inputBytes.Length);
                        cryptoStream.Flush();
                        cryptoStream.Close();
                    }
                    fsEncrypt.Flush();
                    fsEncrypt.Close();
                }
                fsRead.Flush();
                fsRead.Close();
            }
        }

        #endregion

        #region public static void DecryptFileByDesCrypto(string fileToDecrypt, string decryptedFile, string cryptoKey)

        /// <summary>
        /// Decrypts the file. It uses DESCryptoServiceProvider to decrypt.
        /// </summary>
        /// <param name="fileToDecrypt">The input filename.</param>
        /// <param name="decryptedFile">The output filename.</param>
        /// <param name="cryptoKey">The crypto key.</param>
        public static void DecryptFileByDesCrypto(string fileToDecrypt, string decryptedFile, string cryptoKey)
        {
            ICryptoTransform desDecrypt;
            using (var des = new DESCryptoServiceProvider
            {
                Key = Encoding.ASCII.GetBytes(cryptoKey),
                IV = Encoding.ASCII.GetBytes(cryptoKey)
            })
            {
                desDecrypt = des.CreateDecryptor();
            }
            //Create crypto stream set to read and do a 
            //DES decryption transform on incoming bytes.
            using (var fsread = new FileStream(fileToDecrypt, FileMode.Open, FileAccess.Read))
            {
                using (var cryptoStream = new CryptoStream(fsread, desDecrypt, CryptoStreamMode.Read))
                {
                    using (var fsDecrypt = new StreamWriter(decryptedFile))
                    {
                        fsDecrypt.Write(new StreamReader(cryptoStream).ReadToEnd());
                        fsDecrypt.Flush();
                        fsDecrypt.Close();
                    }
                }
                fsread.Flush();
                fsread.Close();
            }
        }

        #endregion

        #region public static string GetDesCryptoKey()

        /// <summary>
        /// Gets the DES crypto key.
        /// </summary>
        /// <returns>A crypto key</returns>
        public static string GetDesCryptoKey()
        {
            // Create an instance of Symmetric Algorithm. Key and IV is generated automatically.
            var desCrypto = (DESCryptoServiceProvider)DES.Create();

            // Use the Automatically generated key for Encryption. 
            return Encoding.ASCII.GetString(desCrypto.Key);
        }

        #endregion

        #region public static string EncryptStringByUnicode(string source)

        /// <summary>
        /// Encrypts the specified data. It uses Encoding.Unicode method to encrypt.
        /// </summary>
        /// <param name="source">The data to encrypt.</param>
        /// <returns>A encrypted string.</returns>
        public static string EncryptStringByUnicode(string source)
        {
            return Convert.ToBase64String(
                Encoding.Unicode.GetBytes(source)
                );
        }

        #endregion

        #region public static string DecryptStringByUnicode(string source)

        /// <summary>
        /// Decrypts the specified data. It uses Encoding.Unicode method to decrypt.
        /// </summary>
        /// <param name="source">The data to decrypt.</param>
        /// <returns>A decrypted string.</returns>
        public static string DecryptStringByUnicode(string source)
        {
            return Encoding.Unicode.GetString(
                Convert.FromBase64String(source)
                );
        }

        #endregion

        #region public static string EncryptStringByDesCrypto(string source, string cryptoKey)

        /// <summary>
        /// Encrypts a string using dual encryption method. It uses TripleDESCryptoServiceProvider to encrypt.
        /// </summary>
        /// ///
        /// <param name="source">The data to encrypt.</param>
        /// <param name="cryptoKey">The crypto key.</param>
        /// <returns>A encrypted string.</returns>
        public static string EncryptStringByDesCrypto(string source, string cryptoKey)
        {
            try
            {
                if (string.IsNullOrEmpty(source))
                {
                    return string.Empty;
                }

                byte[] toEncryptArray = Encoding.UTF8.GetBytes(source);

                byte[] keyArray;
                using (var hashmd5 = new MD5CryptoServiceProvider())
                {
                    keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(cryptoKey));
                    hashmd5.Clear();
                }


                byte[] resultArray;
                using (var tdes = new TripleDESCryptoServiceProvider
                {
                    Key = keyArray,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7
                })
                {
                    using (ICryptoTransform cTransform = tdes.CreateEncryptor())
                    {
                        resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                    }
                    tdes.Clear();
                }
                return Convert.ToBase64String(
                    resultArray, 0, resultArray.Length
                    );
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        #endregion

        #region public static string DecryptStringByDesCrypto(string source, string cryptoKey)

        /// <summary>
        /// Decrypt a string using dual encryption method. It uses TripleDESCryptoServiceProvider to decrypt.
        /// </summary>
        /// <param name="source">The data to decrypt.</param>
        /// <param name="cryptoKey">The crypto key.</param>
        /// <returns>A decrypted string.</returns>
        public static string DecryptStringByDesCrypto(string source, string cryptoKey)
        {
            try
            {
                byte[] toEncryptArray = Convert.FromBase64String(source);
                //byte[] toEncryptArray = Convert.FromBase64String(source);
                byte[] keyArray;
                using (var hashmd5 = new MD5CryptoServiceProvider())
                {
                    keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(cryptoKey));
                    hashmd5.Clear();
                }

                byte[] resultArray;
                using (var tdes = new TripleDESCryptoServiceProvider
                {
                    Key = keyArray,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7
                })
                {
                    using (ICryptoTransform cTransform = tdes.CreateDecryptor())
                    {
                        resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                    }

                    tdes.Clear();
                }
                return Encoding.UTF8.GetString(resultArray);
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        #endregion

        #region public static string EncryptStringByAscii(string source)

        /// <summary>
        /// Encrypts a string. It uses Encoding.ASCII method to encrypt.
        /// </summary>
        /// <param name="source">The data to encrypt.</param>
        /// <returns>A encrypted string.</returns>
        public static string EncryptStringByAscii(string source)
        {
            return Convert.ToBase64String(
                Encoding.ASCII.GetBytes(source)
                );
        }

        #endregion

        #region public static string DecryptStringByAscii(string source)

        /// <summary>
        /// Decrypts a string. It uses Encoding.ASCII method to decrypt.
        /// </summary>
        /// <param name="source">The data to decrypt.</param>
        /// <returns>A decrypted string.</returns>
        public static string DecryptStringByAscii(string source)
        {
            Byte[] b = Convert.FromBase64String(source);
            return Encoding.ASCII.GetString(b);
        }

        #endregion

        #endregion
    }
}
