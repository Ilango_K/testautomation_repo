﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;

namespace NetRachisCore.Helpers
{
    /// <summary>
    /// Cookie Helper for Web Application
    /// </summary>
    public class CookieHelper
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public CookieHelper(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        #region Public Methods

        #region public void SetCookie(string key, string value)

        /// <summary>
        /// Sets the cookie.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public void SetCookie(string key, string value)
        {
            CookieOptions cookie = new CookieOptions();
            DateTime dtExpiry = DateTime.UtcNow.AddDays(7);
            cookie.Expires = dtExpiry;
            _httpContextAccessor.HttpContext.Response.Cookies.Append(key, value, cookie);
        }

        #endregion

        #region public string GetCookie(string key)

        /// <summary>
        /// Gets the cookie.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public string GetCookie(string key)
        {
            var cookie = _httpContextAccessor.HttpContext.Request.Cookies[key];
            if (cookie == null)
                return string.Empty;
            try
            {
                return cookie;
            }
            catch (FormatException)
            {
                return string.Empty;
            }
        }

        #endregion

        #endregion


    }
}
