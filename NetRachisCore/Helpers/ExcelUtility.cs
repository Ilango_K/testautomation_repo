﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Data;
using System.Data.OleDb;

namespace NetRachisCore.Helpers
{
    /// <summary>
    /// Helps to encrypt or decrypt the string or file.
    /// </summary>
    public static class ExcelUtility
    {
        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            DataTable dt = new DataTable();
            try
            {
                using (StreamReader sr = new StreamReader(strFilePath))
                {
                    string[] headers = sr.ReadLine().Split(',');
                    foreach (string header in headers)
                    {
                        dt.Columns.Add(header);
                    }

                    while (!sr.EndOfStream)
                    {
                        string[] rows = sr.ReadLine().Split(',');
                        if (rows.Length > 1)
                        {
                            DataRow dr = dt.NewRow();
                            for (int i = 0; i < headers.Length; i++)
                            {
                                dr[i] = rows[i].Trim();
                            }
                            dt.Rows.Add(dr);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public static DataTable ConvertXSLXtoDataTable(string strFilePath, string connString)
        {
            OleDbConnection oledbConn = new OleDbConnection(connString);
            DataTable dt = new DataTable();
            try
            {
                oledbConn.Open();
                using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Upload$]", oledbConn))
                {
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    oleda.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    oleda.Fill(ds);

                    dt = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oledbConn.Close();
            }
            return dt;
        }

        //public static System.Data.DataTable ConvertToDataTableInteropExcel(string path)
        //{

        //    System.Data.DataTable dt = null;

        //    try
        //    {

        //        object rowIndex = 1;

        //        dt = new System.Data.DataTable();

        //        DataRow row;

        //        Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();

        //        Microsoft.Office.Interop.Excel.Workbook workBook = app.Workbooks.Open(path, 0, true, 5, "", "", true,

        //            Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

        //        Microsoft.Office.Interop.Excel.Worksheet workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet;

        //        int temp = 1;

        //        while (((Microsoft.Office.Interop.Excel.Range)workSheet.Cells[rowIndex, temp]).Value2 != null)
        //        {

        //            dt.Columns.Add(Convert.ToString(((Microsoft.Office.Interop.Excel.Range)workSheet.Cells[rowIndex, temp]).Value2));

        //            temp++;

        //        }

        //        rowIndex = Convert.ToInt32(rowIndex) + 1;

        //        int columnCount = temp;

        //        // Get range of the worksheet
        //        Microsoft.Office.Interop.Excel.Range range = (Microsoft.Office.Interop.Excel.Range)workSheet.UsedRange;

        //        for (int x = 0; x < range.Rows.Count; x++)
        //        {

        //            row = dt.NewRow();

        //            for (int i = 1; i < columnCount; i++)
        //            {

        //                row[i - 1] = Convert.ToString(((Microsoft.Office.Interop.Excel.Range)workSheet.Cells[rowIndex, i]).Value2);

        //            }

        //            dt.Rows.Add(row);

        //            rowIndex = Convert.ToInt32(rowIndex) + 1;

        //        }

        //        app.Workbooks.Close();

        //    }

        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return dt;

        //}

        //public static DataTable ConvertToDataTableExcelDataReader(string filepath)
        //{
        //    FileStream stream = File.Open(filepath, FileMode.Open, FileAccess.Read);
        //    IExcelDataReader excelReader;

        //    if (Path.GetExtension(filepath).ToUpper() == ".XLS")
        //    {
        //        //1.1 Reading from a binary Excel file ('97-2003 format; *.xls)
        //        excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
        //    }
        //    else
        //    {
        //        //1.2 Reading from a OpenXml Excel file (2007 format; *.xlsx)
        //        excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
        //    }
        //    //excelReader.IsFirstRowAsColumnNames = true;
        //    var conf = new ExcelDataSetConfiguration
        //    {
        //        ConfigureDataTable = _ => new ExcelDataTableConfiguration
        //        {
        //            UseHeaderRow = true
        //        }
        //    };

        //    DataSet result = excelReader.AsDataSet(conf);
        //    DataTable dt = new DataTable();
        //    dt = result.Tables[0];
        //    return dt;
        //}
    }
}
