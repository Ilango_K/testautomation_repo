﻿using NetRachisCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetRachisCore.Helpers
{
    public interface IMailService
    {
        Task SendEmailAsync(MailRequest mailRequest);
    }
}
