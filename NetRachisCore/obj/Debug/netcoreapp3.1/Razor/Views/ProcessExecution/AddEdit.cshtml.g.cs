#pragma checksum "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7b0a622ca87d67d4b0bc1b33166529dd7d3782a0"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_ProcessExecution_AddEdit), @"mvc.1.0.view", @"/Views/ProcessExecution/AddEdit.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\_ViewImports.cshtml"
using NetRachisCore;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\_ViewImports.cshtml"
using NetRachisCore.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
using NetRachisCore.Helpers;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7b0a622ca87d67d4b0bc1b33166529dd7d3782a0", @"/Views/ProcessExecution/AddEdit.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"36b0984280c8a791d966e938dbb2d4807911ebe8", @"/Views/_ViewImports.cshtml")]
    public class Views_ProcessExecution_AddEdit : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<NetRachisCore.Models.BusinessObjects.ProcessExecution>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/jquery-1.10.2.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 3 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
  
    Layout = "~/Views/Shared/_MasterLayout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("    <title>");
#nullable restore
#line 5 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
      Write(ViewBag.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("</title>\r\n");
#nullable restore
#line 6 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
    var recCount = 1;

#line default
#line hidden
#nullable disable
            WriteLiteral(@"

<div class=""single-product-tab-area pd-tb-15"">
    <!-- Single pro tab review Start-->
    <div class=""single-pro-review-area"">
        <div class=""container-fluid"">
            <div class=""row"">
                <div class=""col-lg-12 col-md-12 col-sm-12 col-xs-12"">
                    <div class=""review-tab-pro-inner"">
                        <ul id=""myTab3"" class=""tab-review-design"">
");
#nullable restore
#line 18 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                             if (Model.ProcessExecutionId > 0)
                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                <li class=\"active\"><a href=\"#\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i> Process Execution Edit</a></li>\r\n");
#nullable restore
#line 21 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                            }
                            else
                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                <li class=\"active\"><a href=\"#\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i> Process Execution Create</a></li>\r\n");
#nullable restore
#line 25 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                            } 

#line default
#line hidden
#nullable disable
            WriteLiteral("                        </ul>\r\n                        <a href=\"../ProcessExecution/TestResultIndex\" data-toggle=\"tooltip\" title=\"Click to view existing results\" style=\"float:right\">View Existing Result</a>\r\n");
#nullable restore
#line 28 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                         using (Html.BeginForm(Actions.AddEdit, Controllers.ProcessExecution, FormMethod.Post, new { id = "frmProcessExecution" }))
                        {
                            

#line default
#line hidden
#nullable disable
#nullable restore
#line 30 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                       Write(Html.HiddenFor(m => m.ProcessExecutionId));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                            <div id=""myTabContent"" class=""tab-content custom-product-edit"">
                                <div class=""product-tab-list tab-pane fade active in"" id=""description"">
                                    <div class=""row"">
                                        <div class=""col-lg-6 col-md-6 col-sm-6 col-xs-12"">
                                            <div class=""review-content-section"">
                                                <div class=""input-group mg-b-pro-edt"">
                                                    <span class=""input-group-addon""><i class=""fa fa-level-down"" aria-hidden=""true""></i></span>
                                                    ");
#nullable restore
#line 39 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                                               Write(Html.DropDownListFor(m => m.ProjectId, Model.ProjectList, "Select Project", new { @tabindex = 1, @class = "select2_demo_2 form-control", @style = "width:100%;position: absolute;" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                                    ");
#nullable restore
#line 40 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                                               Write(Html.HiddenFor(m => m.ProjectId));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                                                </div>
                                            </div>
                                        </div>
                                        <div class=""col-lg-6 col-md-6 col-sm-6 col-xs-12"">
                                            <div class=""review-content-section"">
                                                <div class=""input-group mg-b-pro-edt"">
                                                    <span class=""input-group-addon""><i class=""fa fa-level-down"" aria-hidden=""true""></i></span>
                                                    ");
#nullable restore
#line 48 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                                               Write(Html.DropDownListFor(m => m.ModuleId, Model.ModuleList, "Select Module", new { @tabindex = 2, @class = "select2_demo_2 form-control", @style = "width:100%;position: absolute;" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                                    ");
#nullable restore
#line 49 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                                               Write(Html.HiddenFor(m => m.ModuleId));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class=""wizard"">
                                        <div class=""content"">
                                            <div id=""myTabContent"" class=""tab-content custom-product-edit m-20 p-20"">
                                                <div class=""table-responsive"" id=""divProcessTable"">
                                                    <table class=""table table-hover table-mailbox processTable"" style=""min-width:675px;"">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th class=""text-center"">Process Id</th>
                              ");
            WriteLiteral(@"                                  <th class=""text-center"">Testcase Builder</th>
                                                                <th class=""text-center"">Command</th>
                                                                <th class=""text-center"">Status</th> 
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
");
#nullable restore
#line 70 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                                                             if (Model.ProcessExecutionChildRefList != null)
                                                            {
                                                                if (Model.ProcessExecutionChildRefList.Count > 0)
                                                                {
                                                                    foreach (var item in Model.ProcessExecutionChildRefList)
                                                                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                                        <tr>\r\n");
#nullable restore
#line 77 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                                                                             if (recCount == 1)
                                                                            {

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                                                                                <td class=""text-right childtd tdwidth20""><i class=""fa fa-minus-circle text-danger removeProcessRow"" aria-hidden=""true"" style=""font-size: x-large;cursor: pointer;visibility:hidden""></i></td>
");
#nullable restore
#line 80 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                                                                            }
                                                                            else
                                                                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                                                <td class=\"text-right childtd tdwidth20\"><i class=\"fa fa-minus-circle text-danger removeProcessRow\" aria-hidden=\"true\" style=\"font-size: x-large;cursor: pointer;\"></i></td>\r\n");
#nullable restore
#line 84 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                                                                            }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                                                                            <td class=""childtd tdwidth110"">
                                                                                <div class=""review-content-section"">
                                                                                    <div class=""mg-b-pro-edt"" style=""text-align:center;"">
                                                                                        ");
#nullable restore
#line 88 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                                                                                   Write(recCount);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td class=""childtd tdwidth110"">
                                                                                <div class=""review-content-section"">
                                                                                    <div class=""mg-b-pro-edt"">
                                                                                        ");
#nullable restore
#line 95 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                                                                                   Write(Html.DropDownList("TestCaseBuildId", item.TestCaseBuildList, "Select Testcase Build", new { @tabindex = 3, @class = "form-control pro-edt-select form-control-primary drpTestcaseBuiild inputreq", @style = "cursor:pointer;" }));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td class=""childtd tdwidth80"">
                                                                                <div class=""review-content-section"">
                                                                                    <div class=""mg-b-pro-edt"">
                                                                                        ");
#nullable restore
#line 102 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                                                                                   Write(Html.DropDownList("Command", item.CommandList, "Select", new { @tabindex = 4, @class = "form-control pro-edt-select form-control-primary drpCommand inputreq", @style = "cursor:pointer;" }));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td class=""childtd tdwidth80"">
                                                                                <div class=""review-content-section"">
                                                                                    <div class=""mg-b-pro-edt"">
                                                                                        <input type='text' id='ProcessStatus' class=""form-control inputreq""");
            BeginWriteAttribute("value", " value=\'", 9692, "\'", 9719, 1);
#nullable restore
#line 109 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
WriteAttributeValue("", 9700, item.ProcessStatus, 9700, 19, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" placeholder=""Process Status"" tabindex=""5"" /> 
                                                                                    </div>
                                                                                </div>
                                                                            </td> 
                                                                            <td class=""childtd tdwidth20""><i class=""fa fa-plus-circle text-success addProcessRow"" aria-hidden=""true"" style=""font-size: x-large; cursor: pointer""></i></td>
                                                                        </tr>
");
#nullable restore
#line 115 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"

                                                                            recCount = recCount + 1;
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                                                                <tr>
                                                                    <td class=""text-right childtd tdwidth20""><i class=""fa fa-minus-circle text-danger removeProcessRow"" aria-hidden=""true"" style=""font-size: x-large;cursor: pointer;visibility:hidden""></i></td>
                                                                    <td class=""childtd tdwidth110"">
                                                                        <div class=""review-content-section"">
                                                                            <div class=""mg-b-pro-edt"" style=""text-align:center;"">
                                                                                1
                                                                            </div>
                                                                        </div>
                                                                    </td>
                               ");
            WriteLiteral(@"                                     <td class=""childtd tdwidth110"">
                                                                        <div class=""review-content-section"">
                                                                            <div class=""mg-b-pro-edt"">
                                                                                ");
#nullable restore
#line 134 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                                                                           Write(Html.DropDownList("TestCaseBuildId", Model.TestCaseBuildList, "Select Testcase Build", new { @tabindex = 3, @class = "form-control pro-edt-select form-control-primary drpTestcaseBuiild inputreq", @style = "cursor:pointer;" }));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td class=""childtd tdwidth80"">
                                                                        <div class=""review-content-section"">
                                                                            <div class=""mg-b-pro-edt"" style=""text-align:center;"">
");
            WriteLiteral(@"                                                                                <a href=""#"" title=""Start"" class=""pd-setting-ed startBtn"">Start</a>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td class=""childtd tdwidth80"">
                                                                        <div class=""review-content-section"">
                                                                            <div class=""mg-b-pro-edt"">
                                                                                <input type='text' id='ProcessStatus' class=""form-control inputreq""");
            BeginWriteAttribute("value", " value=\'", 14103, "\'", 14111, 0);
            EndWriteAttribute();
            WriteLiteral(@" placeholder=""Process Status"" tabindex=""5"" /> 
                                                                            </div>
                                                                        </div>
                                                                    </td> 
                                                                    <td class=""childtd tdwidth20""><i class=""fa fa-plus-circle text-success addProcessRow"" aria-hidden=""true"" style=""font-size: x-large; cursor: pointer""></i></td>
                                                                </tr>
");
#nullable restore
#line 155 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                                                            }

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                        </tbody>\r\n                                                    </table>\r\n                                                    ");
#nullable restore
#line 158 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                                               Write(Html.HiddenFor(m => m.strProcessExecutionChildRefList));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id=""divTCBuildSelect"" style=""display:none"">
                                        ");
#nullable restore
#line 165 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                                   Write(Html.DropDownList("HdnTestCaseType", Model.TestCaseBuildList, "Select Type", new { @class = "form-control pro-edt-select form-control-primary drpTestcaseBuiild inputreq", @style = "cursor:pointer;" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                    </div>\r\n                                    <div id=\"divCommandSelect\" style=\"display:none\">\r\n                                        ");
#nullable restore
#line 168 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                                   Write(Html.DropDownList("HdnCommand", Model.CommandList, "Select", new { @class = "form-control pro-edt-select form-control-primary drpCommand inputreq", @style = "cursor:pointer;" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                    </div>\r\n\r\n");
            WriteLiteral("                                </div>\r\n                            </div>\r\n");
#nullable restore
#line 183 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\ProcessExecution\AddEdit.cshtml"
                        }

#line default
#line hidden
#nullable disable
            WriteLiteral("                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7b0a622ca87d67d4b0bc1b33166529dd7d3782a026308", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
<script type=""text/javascript"">
    $(document).ready(function () { 
        $("".menuitem li a.active"").removeClass(""active"");
        $(""#testexecution-menu a"").attr(""aria-expanded"", ""true"");
        $(""#testexecution-menu ul"").addClass('in');
        $("".menuitem a#aServiceControl"").addClass('active');

        //To bind the module dropdown based on the project selection
        $(document).on(""change"", ""#ProjectId"", function () {
            var control = $(this);
            if ($(control).val() > 0) {
                $.ajax({
                    type: 'post',
                    url: ""../ProcessExecution/GetModulesByProjectId"",
                    data: {
                        projectId: $(control).val()
                    },
                    async: false,
                    success: function (data, item) {
                        $('input[name=' + $(control).attr(""id"") + '').val($(control).val());
                        $(""#ModuleId"").empty();
                        $(""#M");
            WriteLiteral(@"oduleId"").append('<option value="""">Select Module</option>');
                        $(""#select2-ModuleId-container"").text(""Select Module"");
                        $.each(data.data, function (key, value) {
                            $(""#ModuleId"").append('<option value=""' + key + '"">' + value + '</option>');
                        });
                        $("".drpTestcaseBuiild"").empty();
                        $("".drpTestcaseBuiild"").append('<option value="""">Select TestCase Build</option>');
                        $("".drpTestcaseBuiild"").val('');
                    },
                    cache: false,
                    error: function (e) {
                        if (e.status == 401) {
                            window.location.href = ""../Login/UserLogin"";
                        } else {
                            alert(e.responseText);
                        }
                    }
                });
            }
            else {
                $(""#ModuleId"").empty();");
            WriteLiteral(@"
                $(""#ModuleId"").append('<option value="""">Select Module</option>');
                $(""#ModuleId"").val('');
                $(""#select2-ModuleId-container"").text(""Select Module"");
                $("".drpTestcaseBuiild"").empty();
                $("".drpTestcaseBuiild"").append('<option value="""">Select TestCase Build</option>');
                $("".drpTestcaseBuiild"").val('');
            }
        });

        //To bind the test case build dropdown based on the module selection
        $(document).on(""change"", ""#ModuleId"", function () {
            var control = $(this);
            var projId = $(""#ProjectId"").val();
            if ($(control).val() > 0) {
                $.ajax({
                    type: 'post',
                    url: ""../ProcessExecution/GetTestCaseBuildByFilter"",
                    data: {
                        projectId: projId,
                        moduleId: $(control).val(),
                    },
                    async: false,
          ");
            WriteLiteral(@"          success: function (data, item) {
                    $('input[name=' + $(control).attr(""id"") + '').val($(control).val());
                    $("".drpTestcaseBuiild"").empty();
                    $("".drpTestcaseBuiild"").append('<option value="""">Select TestCase Build</option>');
                        $.each(data.data, function (key, value) {
                        $("".drpTestcaseBuiild"").append('<option value=""' + key + '"">' + value + '</option>');
                    });
                },
                cache: false,
                    error: function (e) {
                        if (e.status == 401) {
                            window.location.href = ""../Login/UserLogin"";
                        } else {
                            alert(e.responseText);
                        }
                    }
            });
            }
            else {
                $("".drpTestcaseBuiild"").empty();
                $("".drpTestcaseBuiild"").append('<option value="""">Select Tes");
            WriteLiteral(@"tCase Build</option>');
                $("".drpTestcaseBuiild"").val('');
            }
        });

        //To remove the required class on change event
        $(document).on(""change"", "".inputreq"", function () {
            if ($(this).val()) {
                if ($(this).hasClass(""inputreqerror"")) {
                    $(this).removeClass(""inputreqerror"");
                }
            }
        });

        // Add process row on add button click
        $(document).on(""keypress click"", "".addProcessRow"", function (evt) {
            var control = $(this);
            var empty = false;

            //if ($(""#ProjectId"").val() == '' || $(""#ProjectId"").val() == 0) {
            //    $(""#ProjectId"").addClass(""inputreqerror"");
            //    empty = true;
            //}
            //else {
            //    $(""#ProjectId"").removeClass(""inputreqerror"");
            //}

            //if ($(""#ModuleId"").val() == '' || $(""#ModuleId"").val() == 0) {
            //    $(""#ModuleId""");
            WriteLiteral(@").addClass(""inputreqerror"");
            //    empty = true;
            //}
            //else {
            //    $(""#ModuleId"").removeClass(""inputreqerror"");
            //}

            var input = $(control).parents(""tr"").find('input[type=""text""]');
            input.each(function () {
                if ((!$(this).val() || $(this).val() == 0) && $(this).hasClass(""inputreq"")) {
                    $(this).addClass(""inputreqerror"");
                    empty = true;
                } else {
                    $(this).removeClass(""inputreqerror"");
                }
            });

            var select = $(control).parents(""tr"").find('select');
            select.each(function () {
                if (!$(this).val()) {
                    $(this).addClass(""inputreqerror"");
                    empty = true;
                } else {
                    $(this).removeClass(""inputreqerror"");
                }
            });
            if (!empty) {
                var tcbSelect ");
            WriteLiteral(@"= $(""#divTCBuildSelect"").html();
                var cmdSelect = $(""#divCommandSelect"").html(); 

                var rowIndex = $(""#divProcessTable table tbody tr:last-child"").index();

                var row = '<tr>' +
                    '<td class=""text-right childtd tdwidth20""><i class=""fa fa-minus-circle text-danger removeProcessRow"" aria-hidden=""true"" style=""font-size: x-large;cursor: pointer;""></i></td>' +
                    '<td class=""childtd tdwidth110""><div class=""review-content-section""><div class=""mg-b-pro-edt"" style=""text-align:center;"">' + parseInt(rowIndex + 2) + '</div></div></td>' +
                    '<td class=""childtd tdwidth110""><div class=""review-content-section""><div class=""mg-b-pro-edt"">' + tcbSelect + '</div></div></td>' +
                    '<td class=""childtd tdwidth80""><div class=""review-content-section""><div class=""mg-b-pro-edt"" style=""text-align:center;""><a href=""#"" title=""Start"" class=""pd-setting-ed startBtn"">Start</a></div></div></td>' +
                    '<td");
            WriteLiteral(@" class=""childtd tdwidth80""><div class=""review-content-section""><div class=""mg-b-pro-edt""><input type=""text"" id=""ProcessStatus"" class=""form-control inputreq"" value="""" placeholder=""Process Status"" tabindex=""5"" /></div></div></td>' +
                    '<td class=""childtd tdwidth20""><i class=""fa fa-plus-circle text-success addProcessRow"" aria-hidden=""true"" style=""font-size: x-large; cursor: pointer""></i></td>' +
                    '</tr>';

                $(control).parents(""table.processTable>tbody"").append(row);
            }
        });

        // Delete process row on delete button click
        $(document).on(""keypress click"", "".removeProcessRow"", function (evt) {
            var control = $(this);
            $(control).parents(""table.processTable>tbody>tr"").remove();
        });

        //Button submit action - To save the child table records into the hidden string
        $(document).on(""click"", ""#btnSubmit"", function () {
            var processList = new Array();
            $(""#d");
            WriteLiteral(@"ivProcessTable table tbody tr"").each(function (i) {
                var tds = $(this).find(""td:visible"");
                if ($(tds[2]).find("".drpTestcaseBuiild"").val() != '' && $(tds[3]).find("".drpCommand"").val() != '' && $(tds[4]).find(""#ProcessStatus"").val() != '') {
                    var process = { TestCaseBuildId: $(tds[2]).find("".drpTestcaseBuiild"").val(), Command: $(tds[3]).find("".drpCommand"").val(), ProcessStatus: $(tds[4]).find(""#ProcessStatus"").val() };
                    processList.push(process);
                }
            });
            $('#strProcessExecutionChildRefList').val(JSON.stringify(processList));

            if (processList.length == 0) {
                alert(""Please select atleast one entry for process."");
                return false;
            }
        });

        $(document).on(""keypress click"", "".startBtn"", function (e) { 
            var control = $(this);
            var testCaseId = $(control).parents(""tr"").find(""#TestCaseBuildId"").val();  
     ");
            WriteLiteral(@"       if (testCaseId == """") {
                alert(""Please select the testcase to start the process."");
                return false;
            }
            else {
                $(control).parents(""tr"").find(""#ProcessStatus"").val('Started');
                $.ajax({
                    type: 'post',
                    url: ""../ProcessExecution/RunTestCaseBuilder"",
                    data: {
                        testCaseBuilderId: testCaseId, 
                    },
                    async: false,
                    success: function (data) {
                       // alert(data);
                        $(control).parents(""tr"").find(""#ProcessStatus"").val('Completed - ' + data);
                    },
                    cache: false,
                    error: function (e) {
                        if (e.status == 401) {
                            window.location.href = ""../Login/UserLogin"";
                        } else {
                            alert(e.responseText");
            WriteLiteral(");\r\n                        }\r\n                    }\r\n                });\r\n            } \r\n        });\r\n\r\n    });\r\n\r\n</script>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<NetRachisCore.Models.BusinessObjects.ProcessExecution> Html { get; private set; }
    }
}
#pragma warning restore 1591
