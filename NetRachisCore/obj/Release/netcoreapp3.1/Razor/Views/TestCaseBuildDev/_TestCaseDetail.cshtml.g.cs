#pragma checksum "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\TestCaseBuildDev\_TestCaseDetail.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a83ab18a9b2de9ed3166bfcdd8d6e399d43d5b9b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_TestCaseBuildDev__TestCaseDetail), @"mvc.1.0.view", @"/Views/TestCaseBuildDev/_TestCaseDetail.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\_ViewImports.cshtml"
using NetRachisCore;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\_ViewImports.cshtml"
using NetRachisCore.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a83ab18a9b2de9ed3166bfcdd8d6e399d43d5b9b", @"/Views/TestCaseBuildDev/_TestCaseDetail.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"36b0984280c8a791d966e938dbb2d4807911ebe8", @"/Views/_ViewImports.cshtml")]
    public class Views_TestCaseBuildDev__TestCaseDetail : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<NetRachisCore.Models.BusinessObjects.TestCaseBuildDevChild>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\TestCaseBuildDev\_TestCaseDetail.cshtml"
  
    Layout = null;
    var recCount = 1;

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 7 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\TestCaseBuildDev\_TestCaseDetail.cshtml"
 if (Model != null)
{
    if (Model.Count > 0)
    {
        foreach (var item in Model)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <tr>\r\n");
#nullable restore
#line 14 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\TestCaseBuildDev\_TestCaseDetail.cshtml"
                 if (recCount == 1)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <td class=\"text-right childtd tdwidth20\"><i class=\"fa fa-minus-circle text-danger removeTestCaseRow\" aria-hidden=\"true\" style=\"font-size: x-large;cursor: pointer;visibility:hidden\"></i></td>\r\n");
#nullable restore
#line 17 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\TestCaseBuildDev\_TestCaseDetail.cshtml"
                }
                else
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <td class=\"text-right childtd tdwidth20\"><i class=\"fa fa-minus-circle text-danger removeTestCaseRow\" aria-hidden=\"true\" style=\"font-size: x-large;cursor: pointer;\"></i></td>\r\n");
#nullable restore
#line 21 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\TestCaseBuildDev\_TestCaseDetail.cshtml"
                }

#line default
#line hidden
#nullable disable
            WriteLiteral("                <td class=\"childtd tdwidth110\">\r\n                    <div class=\"review-content-section\">\r\n                        <div class=\"mg-b-pro-edt\" style=\"text-align:center;\">\r\n                            ");
#nullable restore
#line 25 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\TestCaseBuildDev\_TestCaseDetail.cshtml"
                       Write(recCount);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                        </div>
                    </div>
                </td>
                <td class=""childtd tdwidth110"">
                    <div class=""review-content-section"">
                        <div class=""mg-b-pro-edt"">
                            ");
#nullable restore
#line 32 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\TestCaseBuildDev\_TestCaseDetail.cshtml"
                       Write(Html.DropDownList("TestCaseType", item.TestCaseTypeList, "Select Type", new { @tabindex = 4, @class = "form-control pro-edt-select form-control-primary drpType inputreq", @style = "cursor:pointer;" }));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                        </div>
                    </div>
                </td>
                <td class=""childtd tdwidth80"">
                    <div class=""review-content-section"">
                        <div class=""mg-b-pro-edt"">
                            ");
#nullable restore
#line 39 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\TestCaseBuildDev\_TestCaseDetail.cshtml"
                       Write(Html.DropDownList("TableId", item.TableList, "Select", new { @tabindex = 4, @class = "form-control pro-edt-select form-control-primary drpTable inputreq", @style = "cursor:pointer;" }));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                        </div>
                    </div>
                </td>
                <td class=""childtd tdwidth80"">
                    <div class=""review-content-section"">
                        <div class=""mg-b-pro-edt"">
                            ");
#nullable restore
#line 46 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\TestCaseBuildDev\_TestCaseDetail.cshtml"
                       Write(Html.DropDownList("ColumnId", item.ColumnList, "Select", new { @tabindex = 4, @class = "form-control pro-edt-select form-control-primary drpColumn inputreq", @style = "cursor:pointer;" }));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                        </div>
                    </div>
                </td>
                <td class=""childtd tdwidth80"">
                    <div class=""review-content-section"">
                        <div class=""mg-b-pro-edt"">
                            ");
#nullable restore
#line 53 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\TestCaseBuildDev\_TestCaseDetail.cshtml"
                       Write(Html.DropDownList("TargetTableId", item.TargetTableList, "Select", new { @tabindex = 4, @class = "form-control pro-edt-select form-control-primary drpTable drpTargetTable inputreq", @style = "cursor:pointer;" }));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                        </div>
                    </div>
                </td>
                <td class=""childtd tdwidth80"">
                    <div class=""review-content-section"">
                        <div class=""mg-b-pro-edt"">
                            ");
#nullable restore
#line 60 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\TestCaseBuildDev\_TestCaseDetail.cshtml"
                       Write(Html.DropDownList("TargetColumnId", item.TargetColumnList, "Select", new { @tabindex = 4, @class = "form-control pro-edt-select form-control-primary drpColumn drpTargetColumn inputreq", @style = "cursor:pointer;" }));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                        </div>
                    </div>
                </td>
                <td class=""childtd tdwidth80"" style=""vertical-align: baseline;"">
                    <div class=""review-content-section"">
                        <div class=""mg-b-pro-edt"">
                            <div class=""col-lg-10 col-md-10 col-sm-10 col-xs-10"" style=""padding:0;"">
                                <input type=""text"" id=""txtValue"" class=""form-control txtValue inputreq"" placeholder=""Value/ Match"" title=""Value/ Match""");
            BeginWriteAttribute("value", " value=\"", 3901, "\"", 3929, 1);
#nullable restore
#line 68 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\TestCaseBuildDev\_TestCaseDetail.cshtml"
WriteAttributeValue("", 3909, item.MatchCondition, 3909, 20, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                            </div>\r\n                            <div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-2\" style=\"padding:0;text-align:right;\">\r\n                                <a href=\"#\" data-id=\"");
#nullable restore
#line 71 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\TestCaseBuildDev\_TestCaseDetail.cshtml"
                                                Write(recCount);

#line default
#line hidden
#nullable disable
            WriteLiteral(@""" data-toggle=""modal"" title=""Formula Builder"" class=""pd-setting-ed mg-t exprpop"" data-target=""#exprModal""><i aria-hidden=""true"">...</i></a>
                            </div>
                        </div>
                    </div>
                </td>
                <td class=""childtd tdwidth20""><i class=""fa fa-plus-circle text-success addTestCaseRow"" aria-hidden=""true"" style=""font-size: x-large; cursor: pointer""></i><input type=""hidden"" id=""hdnBOQSubTypeId"" value=""0"" /></td>
            </tr>
");
#nullable restore
#line 78 "D:\GPG\Projects\GIT\TestAutomation\NetRachisCore\Views\TestCaseBuildDev\_TestCaseDetail.cshtml"

                recCount = recCount + 1;
        }
    }
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<NetRachisCore.Models.BusinessObjects.TestCaseBuildDevChild>> Html { get; private set; }
    }
}
#pragma warning restore 1591
