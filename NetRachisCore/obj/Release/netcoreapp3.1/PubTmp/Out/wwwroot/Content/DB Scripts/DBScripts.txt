﻿-- Database Scripts

DROP TABLE IF EXISTS ApplicationLog;
GO
CREATE TABLE ApplicationLog (
  ApplicationLogId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  UserId int NOT NULL,
  Controller varchar(100) NOT NULL,
  Action varchar(50) DEFAULT NULL,
  Parameter varchar(2500) DEFAULT NULL,
  DateTime datetime DEFAULT CURRENT_TIMESTAMP, 
)
GO
DROP TABLE IF EXISTS AppUser; 
GO
CREATE TABLE AppUser (
  AppUserId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  UserName varchar(200) NOT NULL,
  UserPassword varchar(300) NOT NULL,
  UserEmailId varchar(200) NOT NULL,
  RoleId int DEFAULT NULL,
  ActiveStatus int DEFAULT '0',
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  CreatedOn datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  ModifiedOn datetime DEFAULT CURRENT_TIMESTAMP 
)  
GO
INSERT INTO AppUser VALUES ('Ilango','AzQZHhgMH8N/fa1/9O3Ojw==','ilango.k@netkathir.com',1,1,0,NULL,'2019-12-06 10:42:00',1,'2019-12-06 10:42:00'),('Maraimani','AzQZHhgMH8N/fa1/9O3Ojw==','maraimani@netkathir.com',1,1,0,1,'2020-03-02 08:41:52',2,'2020-03-02 08:41:52');
GO
DROP TABLE IF EXISTS Commands; 
GO
CREATE TABLE Commands (
  CommandId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  Command varchar(100) NOT NULL,
  Description varchar(100) DEFAULT NULL,
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  RecCreationDate datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  RecLastModifiedDate datetime DEFAULT CURRENT_TIMESTAMP  
) 
GO
INSERT INTO Commands VALUES ('Start',NULL,0,NULL,'2020-05-18 11:22:42',NULL,'2020-05-18 11:22:42'),('Stop',NULL,0,NULL,'2020-05-18 11:22:42',NULL,'2020-05-18 11:22:42');
GO
DROP TABLE IF EXISTS CommandTypes; 
GO
CREATE TABLE CommandTypes (
  CommandTypeId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  CommandType varchar(100) NOT NULL,
  Description varchar(100) DEFAULT NULL,
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  RecCreationDate datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  RecLastModifiedDate datetime DEFAULT CURRENT_TIMESTAMP  
) 
GO
INSERT INTO CommandTypes VALUES ('CMD',NULL,0,NULL,'2020-05-18 11:21:25',NULL,'2020-05-18 11:21:25'),('Linux',NULL,0,NULL,'2020-05-18 11:21:25',NULL,'2020-05-18 11:21:25'),('XLA',NULL,0,NULL,'2020-05-18 11:21:25',NULL,'2020-05-18 11:21:25');
GO
DROP TABLE IF EXISTS Configuration;
GO
CREATE TABLE Configuration (
  ConfigurationId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  ProjectId int DEFAULT NULL,
  ModuleId int DEFAULT NULL,
  TestCaseBuildId int DEFAULT NULL,
  InputFileTypeId int DEFAULT NULL,
  InputFilePath varchar(150) DEFAULT NULL,
  InputFileFormat varchar(100) DEFAULT NULL,
  InputFileSet int DEFAULT '0',
  OutputFileTypeId int DEFAULT NULL,
  OutputFilePath varchar(150) DEFAULT NULL,
  OutputFileFormat varchar(100) DEFAULT NULL,
  OutputFileSet int DEFAULT '0',
  SchedulerInputStartDate datetime DEFAULT NULL,
  SchedulerInputStartTime varchar(100) DEFAULT NULL,
  SchedulerInputEndDate datetime DEFAULT NULL,
  SchedulerInputEndTime varchar(100) DEFAULT NULL,
  SchedulerInputFrequency varchar(150) DEFAULT NULL,
  SchedulerInputSet int DEFAULT '0',
  SchedulerOutputStartDate datetime DEFAULT NULL,
  SchedulerOutputStartTime varchar(100) DEFAULT NULL,
  SchedulerOutputEndDate datetime DEFAULT NULL,
  SchedulerOutputEndTime varchar(100) DEFAULT NULL,
  SchedulerOutputFrequency varchar(150) DEFAULT NULL,
  SchedulerOutputSet int DEFAULT '0',
  IsDeleted int DEFAULT '0',
  SchedulerInputLoaded int DEFAULT '0',
  SchedulerOutputLoaded int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  CreatedOn datetime DEFAULT NULL,
  ModifiedBy int DEFAULT NULL,
  ModifiedOn datetime DEFAULT NULL 
) 
GO
DROP TABLE IF EXISTS ConfigurationFile;
GO
CREATE TABLE ConfigurationFile (
  ConfigurationFileId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  ConfigurationId int DEFAULT NULL,
  FileTypeId int DEFAULT NULL,
  FilePath varchar(150) DEFAULT NULL,
  FileNameFormat varchar(100) DEFAULT NULL,
  IsSet int DEFAULT '0',
  IsInput int DEFAULT '0',
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  CreatedOn datetime DEFAULT NULL,
  ModifiedBy int DEFAULT NULL,
  ModifiedOn datetime DEFAULT NULL 
)
GO
DROP TABLE IF EXISTS ConfigurationTable;
GO
CREATE TABLE ConfigurationTable (
  ConfigurationTableId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  ConfigurationId int DEFAULT NULL,
  TableTypeId int DEFAULT NULL,
  TableName varchar(150) DEFAULT NULL,
  IsInput int DEFAULT '0',
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  CreatedOn datetime DEFAULT NULL,
  ModifiedBy int DEFAULT NULL,
  ModifiedOn datetime DEFAULT NULL 
)
GO
DROP TABLE IF EXISTS DataTypes;
GO
CREATE TABLE DataTypes (
  DataTypeId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  DataType varchar(100) NOT NULL,
  Description varchar(100) DEFAULT NULL,
  IsDeleted int DEFAULT '0' 
)
GO
INSERT INTO DataTypes VALUES ('int',NULL,0),('double',NULL,0),('varchar(100)',NULL,0),('datetime',NULL,0);
GO
DROP TABLE IF EXISTS FileColumn;
GO
CREATE TABLE FileColumn (
  FileColumnId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  FileTypeConfigurationId int DEFAULT NULL,
  ColumnName varchar(150) DEFAULT NULL,
  DataTypeId int DEFAULT NULL,
  ColumnNo varchar(10) DEFAULT NULL,
  IsDeleted int DEFAULT '0' 
)
GO
DROP TABLE IF EXISTS FileExtnType;
GO
CREATE TABLE FileExtnType (
  FileExtnTypeId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  FileExtnType varchar(100) NOT NULL,
  Description varchar(100) DEFAULT NULL,
  IsDeleted int DEFAULT '0' 
)
GO
INSERT INTO FileExtnType VALUES ('CSV',NULL,0),('XML',NULL,0);
GO
DROP TABLE IF EXISTS FileTypeConfiguration;
GO
CREATE TABLE FileTypeConfiguration (
  FileTypeConfigurationId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  ProjectId int DEFAULT NULL,
  ModuleId int DEFAULT NULL,
  GeneratedFileTypeId varchar(100) DEFAULT NULL,
  GeneratedFileTypeName varchar(200) DEFAULT NULL,
  FileExtnTypeId int DEFAULT NULL,
  FileTypeId int DEFAULT NULL,
  TableTypeId int DEFAULT NULL,
  TableName varchar(100) DEFAULT NULL,
  IsSet int DEFAULT '0',
  DataRowStart varchar(100) DEFAULT NULL,
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  CreatedOn datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  ModifiedOn datetime DEFAULT CURRENT_TIMESTAMP 
)
GO
DROP TABLE IF EXISTS FileTypes;
GO
CREATE TABLE FileTypes (
  FileTypeId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  FileTypeName varchar(250) DEFAULT NULL,
  Description varchar(500) DEFAULT NULL,
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  CreatedOn datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  ModifiedOn datetime DEFAULT CURRENT_TIMESTAMP 
) 
GO
INSERT INTO FileTypes VALUES ('CSV','csv file type',0,1,'2020-04-12 14:40:56',1,'2020-04-12 14:40:56');
GO
DROP TABLE IF EXISTS FormDetail;
GO
CREATE TABLE FormDetail (
  FormId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  FormName varchar(100) NOT NULL,
  MenuId int DEFAULT NULL,
  Description varchar(200) DEFAULT NULL,
  IsDeleted int DEFAULT '0' 
) 
GO
INSERT INTO FormDetail VALUES ('Dashboard',1,NULL,0),('Role',2,NULL,0),('RoleFormAccess',2,NULL,0),('User',2,NULL,0),('UploadTrade',3,NULL,0),('UploadTransaction',3,NULL,0),('Project',4,NULL,0),('Module',4,NULL,0),('TestCaseBuild',4,NULL,0),('TestCaseBuildDev',4,NULL,0),('FileTypeConfiguration',4,NULL,0),('Configuration',4,NULL,0),('FileTypes',5,NULL,0),('TableTypes',5,NULL,0),('TradeBookingConfig',4,NULL,0),('ServiceControl',6,NULL,0),('MonitoringControl',6,NULL,0),('ErrorLog',6,NULL,0),('Reports',7,NULL,0);
GO
DROP TABLE IF EXISTS MenuDetail;
GO
CREATE TABLE MenuDetail (
  MenuId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  MenuName varchar(100) NOT NULL,
  Description varchar(200) DEFAULT NULL,
  IsDeleted int DEFAULT '0' 
)
GO
INSERT INTO MenuDetail VALUES ('Dashboard',NULL,0),('Admin',NULL,0),('DocumentUpload',NULL,0),('Master',NULL,0),('Static Data',NULL,0),('TestExecution',NULL,0),('Reports',NULL,0);
GO
DROP TABLE IF EXISTS Module;
GO
CREATE TABLE Module (
  ModuleId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  ProjectId int NOT NULL,
  ModuleName varchar(100) DEFAULT NULL,
  Description varchar(250) DEFAULT NULL,
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  RecCreationDate datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  RecLastModifiedDate datetime DEFAULT CURRENT_TIMESTAMP 
)
GO
DROP TABLE IF EXISTS ProcessExecution;
GO
CREATE TABLE ProcessExecution (
  ProcessExecutionId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  ProjectId int NOT NULL,
  ModuleId int NOT NULL,
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  RecCreationDate datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  RecLastModifiedDate datetime DEFAULT CURRENT_TIMESTAMP  
)
GO
DROP TABLE IF EXISTS ProcessExecutionChild;
GO
CREATE TABLE ProcessExecutionChild (
  ProcessExecutionChildId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  ProcessExecutionId int NOT NULL,
  TestCaseBuildId int NOT NULL,
  Command varchar(50) DEFAULT NULL,
  ProcessStatus varchar(100) DEFAULT NULL 
) 
GO
DROP TABLE IF EXISTS Project; 
GO
CREATE TABLE Project (
  ProjectId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  ProjectName varchar(100) DEFAULT NULL,
  Description varchar(250) DEFAULT NULL,
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  RecCreationDate datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  RecLastModifiedDate datetime DEFAULT CURRENT_TIMESTAMP 
)
GO
INSERT INTO Project VALUES ('Testing Automation','Test',0,1,'2020-04-12 14:42:41',NULL,'2020-04-12 14:42:41'),('AIMCO TESTING','',0,2,'2020-05-06 04:33:01',NULL,'2020-05-06 04:33:01');
GO
DROP TABLE IF EXISTS Role;
GO
CREATE TABLE Role (
  RoleId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  RoleName varchar(100) NOT NULL,
  Description varchar(100) DEFAULT NULL,
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  RecCreationDate datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  RecLastModifiedDate datetime DEFAULT CURRENT_TIMESTAMP 
) 
GO
INSERT INTO Role VALUES ('Admin','Administrator Role',0,NULL,'2020-01-09 15:21:13',1,'2020-01-09 15:59:02'),('Manager','',1,1,'2020-01-09 15:59:21',1,'2020-01-09 15:59:31');
GO
DROP TABLE IF EXISTS RoleFormAccess;
GO
CREATE TABLE RoleFormAccess (
  RoleFormAccessId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  RoleId int DEFAULT NULL,
  MenuIds varchar(100) NOT NULL,
  FormIds varchar(300) DEFAULT NULL,
  IsWriteAccess varchar(10) DEFAULT NULL,
  CreatedBy int DEFAULT NULL,
  RecCreationDate datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  RecLastModifiedDate datetime DEFAULT CURRENT_TIMESTAMP 
)
GO
INSERT INTO RoleFormAccess VALUES (1,'1,2,3,4,5,6,7','1,2,3,4,5,6,7,8,9,10,15,16,17,18,19,20,21,22,23','N',0,'2020-01-09 16:49:41',1,'2020-05-06 19:44:30');
GO
DROP TABLE IF EXISTS TableTypes;
GO
CREATE TABLE TableTypes (
  TableTypeId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  TableTypeName varchar(250) DEFAULT NULL,
  Description varchar(500) DEFAULT NULL,
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  CreatedOn datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  ModifiedOn datetime DEFAULT CURRENT_TIMESTAMP 
) 
GO
INSERT INTO TableTypes VALUES ('New','Test',0,1,'2020-04-12 14:41:50',NULL,'2020-04-12 14:41:50');
GO
DROP TABLE IF EXISTS TagColumn;
GO
CREATE TABLE TagColumn (
  TagColumnId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  FileTypeConfigurationId int DEFAULT NULL,
  Tag varchar(100) DEFAULT NULL,
  DataTypeId int DEFAULT NULL,
  IsDeleted int DEFAULT '0' 
)
GO
DROP TABLE IF EXISTS TargetSystems;
GO
CREATE TABLE TargetSystems (
  TargetSystemId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  TargetSystem varchar(100) NOT NULL,
  Description varchar(100) DEFAULT NULL,
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  RecCreationDate datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  RecLastModifiedDate datetime DEFAULT CURRENT_TIMESTAMP  
)
GO
DROP TABLE IF EXISTS TestCaseBuild;
GO
CREATE TABLE TestCaseBuild (
  TestCaseBuildId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  ProjectId int NOT NULL,
  ModuleId int NOT NULL,
  TestCaseBuildName varchar(100) DEFAULT NULL,
  Description varchar(250) DEFAULT NULL,
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  RecCreationDate datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  RecLastModifiedDate datetime DEFAULT CURRENT_TIMESTAMP 
) 
GO
DROP TABLE IF EXISTS TestCaseBuildDev;
GO
CREATE TABLE TestCaseBuildDev (
  TestCaseBuildDevId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  ProjectId int NOT NULL,
  ModuleId int NOT NULL,
  TestCaseBuildId int NOT NULL,
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  RecCreationDate datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  RecLastModifiedDate datetime DEFAULT CURRENT_TIMESTAMP 
)
GO
DROP TABLE IF EXISTS TestCaseBuildDevChild;
GO
CREATE TABLE TestCaseBuildDevChild (
  TestCaseBuildDevChildId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  TestCaseBuildDevId int NOT NULL,
  TestCaseTypeId int NOT NULL,
  TableName varchar(100) DEFAULT NULL,
  ColumnName varchar(100) DEFAULT NULL,
  TargetTableName varchar(100) DEFAULT NULL,
  TargetColumnName varchar(100) DEFAULT NULL,
  MatchCondition varchar(1000) DEFAULT NULL 
)
GO
DROP TABLE IF EXISTS TestCaseResult;
GO
CREATE TABLE TestCaseResult (
  TestCaseResultId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  TestCaseResultNo varchar(50) DEFAULT NULL,
  TestcaseBuildDevChildId int NOT NULL,
  MatchCondition varchar(300) NOT NULL,
  SourceValue varchar(200) DEFAULT NULL,
  TargetValue varchar(200) DEFAULT NULL,
  MatchResult varchar(200) DEFAULT NULL 
)
GO
DROP TABLE IF EXISTS TestCaseType;
GO
CREATE TABLE TestCaseType (
  TestCaseTypeId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  TestCaseType varchar(100) NOT NULL,
  Description varchar(100) DEFAULT NULL,
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  RecCreationDate datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  RecLastModifiedDate datetime DEFAULT CURRENT_TIMESTAMP 
)
GO
INSERT INTO TestCaseType VALUES ('Trade Match',NULL,0,NULL,'2020-03-17 14:24:21',NULL,'2020-03-17 14:24:21'),('Security Match',NULL,0,NULL,'2020-03-17 14:24:21',NULL,'2020-03-17 14:24:21'),('Static',NULL,0,NULL,'2020-03-17 14:24:21',NULL,'2020-03-17 14:24:21');
GO
DROP TABLE IF EXISTS Trade;
GO
CREATE TABLE Trade (
  TradeId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  TickNumber varchar(50) DEFAULT NULL,
  TradeDate varchar(20) DEFAULT NULL,
  SettleDate varchar(20) DEFAULT NULL,
  BuySell varchar(10) DEFAULT NULL,
  ExternSymbol varchar(20) DEFAULT NULL,
  Ticker varchar(30) DEFAULT NULL,
  SecType varchar(10) DEFAULT NULL,
  ClientCode varchar(30) DEFAULT NULL,
  Trader varchar(50) DEFAULT NULL,
  ExchangeGroup varchar(30) DEFAULT NULL,
  Broker varchar(50) DEFAULT NULL,
  AvgPrice decimal(10,2) DEFAULT NULL,
  PriceCurrency varchar(10) DEFAULT NULL,
  SettleCountryCode varchar(10) DEFAULT NULL,
  Shares decimal(15,2) DEFAULT NULL,
  NetCost decimal(15,2) DEFAULT NULL,
  SecFees decimal(10,2) DEFAULT NULL,
  Tax decimal(10,2) DEFAULT NULL,
  OtherCharges decimal(10,2) DEFAULT NULL,
  Commission decimal(10,2) DEFAULT NULL,
  AccruedInterest decimal(10,2) DEFAULT NULL,
  ElectronicallyMatched varchar(10) DEFAULT NULL,
  MatureDate varchar(20) DEFAULT NULL,
  AvgYield decimal(10,6) DEFAULT NULL,
  Strategy varchar(50) DEFAULT NULL,
  CouponRate decimal(5,3) DEFAULT NULL,
  IsDeleted int DEFAULT '0',
  TradeUploadID int DEFAULT NULL,
  CreatedBy int DEFAULT NULL,
  RecCreationDate datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  RecLastModifiedDate datetime DEFAULT CURRENT_TIMESTAMP 
) 
GO
DROP TABLE IF EXISTS TradeBookingConfig;
GO
CREATE TABLE TradeBookingConfig (
  TradeBookingConfigId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  ProjectId int NOT NULL,
  ModuleId int NOT NULL,
  TestCaseBuildId int NOT NULL,
  SchedulerDate datetime DEFAULT NULL,
  SchedulerTime varchar(100) DEFAULT NULL,
  SchedulerFrequency varchar(150) DEFAULT NULL,
  SchedulerSet int DEFAULT '0',
  TradeInputPath varchar(300) DEFAULT NULL,
  FileNameFormat varchar(100) DEFAULT NULL,
  TargetSystemId int DEFAULT NULL,
  TradeInputSet int DEFAULT '0',
  CommandTypeId int DEFAULT NULL,
  CommandLine varchar(200) DEFAULT NULL,
  AdditionalInfo varchar(300) DEFAULT NULL,
  TargetPath varchar(300) DEFAULT NULL,
  CommandSet int DEFAULT '0',
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  RecCreationDate datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  RecLastModifiedDate datetime DEFAULT CURRENT_TIMESTAMP 
) 
GO
DROP TABLE IF EXISTS TradeUpload;
GO
CREATE TABLE TradeUpload (
  TradeUploadId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  TradeBatchNumber varchar(10) DEFAULT NULL,
  FilePath varchar(300) DEFAULT NULL,
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  RecCreationDate datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  RecLastModifiedDate datetime DEFAULT CURRENT_TIMESTAMP 
)
GO
DROP TABLE IF EXISTS TransactionUpload;
GO
CREATE TABLE TransactionUpload (
  TransactionUploadId int NOT NULL PRIMARY KEY IDENTITY(1,1),
  TransactionBatchNumber varchar(10) DEFAULT NULL,
  FilePath varchar(300) DEFAULT NULL,
  IsDeleted int DEFAULT '0',
  CreatedBy int DEFAULT NULL,
  RecCreationDate datetime DEFAULT CURRENT_TIMESTAMP,
  ModifiedBy int DEFAULT NULL,
  RecLastModifiedDate datetime DEFAULT CURRENT_TIMESTAMP 
)
GO
 -- Stored PROCEDURE
DROP PROCEDURE IF EXISTS [USP_SelectAllConfiguration] 
GO
CREATE  PROCEDURE USP_SelectAllConfiguration
AS
BEGIN
	SET NOCOUNT ON;  
	SELECT  
		ConfigurationId, 
		c.TestCaseBuildId, 
		tb.TestCaseBuildName, 
		c.ModuleId, 
		m.ModuleName, 
		c.ProjectId, 
		p.ProjectName  
	FROM Configuration c 
	INNER JOIN TestCaseBuild tb ON tb.TestCaseBuildId = c.TestCaseBuildId 
	INNER JOIN Module m ON m.ModuleId = tb.ModuleId 
	INNER JOIN project p ON p.ProjectId = tb.ProjectId
	WHERE c.IsDeleted = 0;
END
GO
DROP PROCEDURE IF EXISTS USP_SelectAllFileTypeConfiguration 
GO 
CREATE  PROCEDURE USP_SelectAllFileTypeConfiguration
AS
BEGIN
	SET NOCOUNT ON;  
	SELECT  
		FileTypeConfigurationId,  
		c.ModuleId, 
		m.ModuleName, 
		c.ProjectId, 
		p.ProjectName, 
		c.GeneratedFileTypeId  
	FROM FileTypeConfiguration c  
	INNER JOIN Module m ON m.ModuleId = c.ModuleId 
	INNER JOIN Project p ON p.ProjectId = c.ProjectId 
	WHERE c.IsDeleted = 0; 
END
GO
DROP PROCEDURE IF EXISTS USP_SelectAllModule 
GO  
CREATE  PROCEDURE USP_SelectAllModule
AS
BEGIN
	SET NOCOUNT ON;  
	SELECT  
		m.ModuleId, 
		m.ModuleName, 
		m.ProjectId, 
		p.ProjectName, 
		m.Description  
	FROM Module m 
	INNER JOIN Project p ON p.ProjectId = m.ProjectId 
	WHERE m.IsDeleted = 0; 
	END
GO 
DROP PROCEDURE IF EXISTS USP_SelectAllProcessExecution 
GO  
CREATE  PROCEDURE USP_SelectAllProcessExecution
AS
BEGIN
	SET NOCOUNT ON; 
	SELECT 
		pe.ProcessExecutionId, 
		pe.ModuleId,
		m.ModuleName,
		pe.ProjectId,
		p.ProjectName 
	FROM ProcessExecution pe 
	INNER JOIN Module m ON m.ModuleId = pe.ModuleId
	INNER JOIN Project p ON p.ProjectId = pe.ProjectId
	WHERE pe.IsDeleted = 0;
END
GO
DROP PROCEDURE IF EXISTS USP_SelectAllProject 
GO   
CREATE  PROCEDURE USP_SelectAllProject
AS
BEGIN
	SET NOCOUNT ON;  
	SELECT 
		ProjectId,
		ProjectName,
		Description 
	FROM Project
	WHERE IsDeleted = 0;
END
GO
DROP PROCEDURE IF EXISTS USP_SelectAllTestCaseBuild 
GO   
CREATE  PROCEDURE USP_SelectAllTestCaseBuild
AS
BEGIN
	SET NOCOUNT ON;  
	SELECT 
		tb.TestCaseBuildId,
		tb.TestCaseBuildName,
		tb.ModuleId,
		m.ModuleName,
		tb.ProjectId,
		p.ProjectName,
		tb.Description 
	FROM TestCaseBuild tb
	INNER JOIN Module m ON m.ModuleId = tb.ModuleId
	INNER JOIN Project p ON p.ProjectId = tb.ProjectId
	WHERE tb.IsDeleted = 0; 
END
GO 
DROP PROCEDURE IF EXISTS USP_SelectAllTestCaseBuildDev 
GO   
CREATE  PROCEDURE USP_SelectAllTestCaseBuildDev
AS
BEGIN
	SET NOCOUNT ON;  
	SELECT 
		tbd.TestCaseBuildDevId, 
		tbd.ModuleId,
		m.ModuleName,
		tbd.ProjectId,
		p.ProjectName,
		tbd.TestCaseBuildId,
		tb.TestCaseBuildName
	FROM TestCaseBuildDev tbd
	INNER JOIN TestCaseBuild tb ON tbd.TestCaseBuildId = tb.TestCaseBuildId
	INNER JOIN Module m ON m.ModuleId = tbd.ModuleId
	INNER JOIN Project p ON p.ProjectId = tbd.ProjectId
	WHERE tbd.IsDeleted = 0;
END
GO
DROP PROCEDURE IF EXISTS USP_SelectAllTradeBookingConfig 
GO   
CREATE  PROCEDURE USP_SelectAllTradeBookingConfig
AS
BEGIN
	SET NOCOUNT ON;  
	SELECT 
		tbc.TradeBookingConfigId, 
		tbc.ModuleId,
		m.ModuleName,
		tbc.ProjectId,
		p.ProjectName,
		tbc.TestCaseBuildId,
		tb.TestCaseBuildName
	FROM TradeBookingConfig tbc
	INNER JOIN TestCaseBuild tb ON tbc.TestCaseBuildId = tb.TestCaseBuildId
	INNER JOIN Module m ON m.ModuleId = tbc.ModuleId
	INNER JOIN Project p ON p.ProjectId = tbc.ProjectId
	WHERE tbc.IsDeleted = 0;
END
GO
DROP PROCEDURE IF EXISTS USP_SelectAllUser 
GO    
CREATE  PROCEDURE USP_SelectAllUser
AS
BEGIN
	SET NOCOUNT ON; 
	SELECT
		AppUserId,
		u.RoleId,
		r.RoleName, 
		UserName,	
		UserEmailId, 
		ActiveStatus
	FROM AppUser u
	JOIN Role r ON u.RoleId = r.RoleId 
	WHERE u.IsDeleted = 0
	ORDER BY AppUserId DESC;
END
GO
DROP PROCEDURE IF EXISTS USP_SelectAccessFormByUser 
GO    
CREATE  PROCEDURE USP_SelectAccessFormByUser(@userId INT)
AS
BEGIN
	SET NOCOUNT ON; 
	SELECT 
		f.FormName AS 'Forms',
		rf.IsWriteAccess
	FROM
		RoleFormAccess rf
		JOIN FormDetail f ON f.FormId IN (SELECT TRIM(value) FROM  STRING_SPLIT(rf.FormIds, ',' ))
	WHERE rf.RoleId IN (SELECT DISTINCT RoleId FROM AppUser a WHERE a.AppUserId = @userId); 
END
GO
DROP PROCEDURE IF EXISTS USP_SelectAccessMenuByUser 
GO    
CREATE  PROCEDURE USP_SelectAccessMenuByUser(@userId INT)
AS
BEGIN
	SET NOCOUNT ON; 
	SELECT 
		m.MenuName AS 'Menus',
		ra.IsWriteAccess
	FROM
		RoleFormAccess ra
		JOIN MenuDetail m ON m.MenuId IN (SELECT TRIM(value) FROM  STRING_SPLIT(ra.MenuIds, ',' ))
	WHERE ra.RoleId IN (SELECT DISTINCT RoleId FROM AppUser a WHERE a.AppUserId = @userId); 
END
GO
DROP PROCEDURE IF EXISTS USP_SelectAllRoleFormAccess 
GO    
CREATE PROCEDURE [dbo].[USP_SelectAllRoleFormAccess]
AS
BEGIN
	SET NOCOUNT ON;  
	SELECT  
		rf.RoleFormAccessId,  
		r.RoleName,  
		(SELECT STUFF((SELECT ', ' + m.MenuName FROM dbo.MenuDetail m
          WHERE m.MenuId IN (SELECT TRIM(value) FROM  STRING_SPLIT(rf.MenuIds, ',' ))
          FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '')) AS 'MenuNames', 
		(SELECT STUFF((SELECT ', ' + f.FormName FROM FormDetail f
          WHERE f.FormId IN (SELECT TRIM(value) FROM  STRING_SPLIT(rf.FormIds, ',' ))
          FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '')) AS 'FormNames', 
		rf.IsWriteAccess
	FROM RoleFormAccess rf   
	INNER JOIN Role r ON rf.RoleId = r.RoleId   
END
GO 
DROP PROCEDURE IF EXISTS USP_SelectAllFilesToUpload 
GO    
CREATE  PROCEDURE USP_SelectAllFilesToUpload
AS
BEGIN
SET NOCOUNT ON; 
	SELECT 
		c.ConfigurationId,
		c.InputFileTypeId AS FileTypeId,
		c.InputFilePath AS FilePath, 
		c.InputFileFormat AS FileFormat,
		1 AS IsInput,
		ftc.TableName AS TableName,
		(SELECT dbo.GROUP_CONCAT(ColumnName) FROM filecolumn fc WHERE FileTypeConfigurationId = c.InputFileTypeId) AS 'Columns'
	FROM Configuration c
	INNER JOIN FileTypeConfiguration ftc ON ftc.FileTypeConfigurationId = c.InputFileTypeId
	WHERE (GETDATE() BETWEEN CAST(CONCAT(CONVERT(DATE, SchedulerInputStartDate), ' ', SchedulerInputStartTime) AS DATETIME2(0)) AND CAST(CONCAT(CONVERT(DATE, SchedulerInputEndDate), ' ', SchedulerInputEndTime) AS DATETIME2(0))) 
	AND c.SchedulerInputSet = 1
	AND c.SchedulerInputLoaded = 0
	AND c.IsDeleted = 0 
	UNION
	SELECT 
		c.ConfigurationId,
		c.OutputFileTypeId AS FileTypeId,
		c.OutputFilePath AS FilePath, 
		c.OutputFileFormat AS FileFormat,
		0 AS IsInput,
		ftc.TableName AS TableName,
		(SELECT dbo.GROUP_CONCAT(ColumnName) FROM filecolumn fc WHERE FileTypeConfigurationId = c.OutputFileTypeId) AS 'Columns'
	FROM Configuration c
	INNER JOIN FileTypeConfiguration ftc ON ftc.FileTypeConfigurationId = c.OutputFileTypeId
	WHERE (GETDATE() BETWEEN CAST(CONCAT(CONVERT(DATE, SchedulerOutputStartDate), ' ', SchedulerOutputStartTime) AS DATETIME2(0)) AND CAST(CONCAT(CONVERT(DATE, SchedulerOutputEndDate), ' ', SchedulerOutputEndTime) AS DATETIME2(0))) 
	AND c.SchedulerOutputSet = 1
	AND c.SchedulerOutputLoaded = 0
	AND c.IsDeleted = 0; 
END
GO
DROP PROCEDURE IF EXISTS USP_ExecuteTestCase 
GO
CREATE PROCEDURE [dbo].[USP_ExecuteTestCase](@tcBuildId INT)
AS
BEGIN
	SET NOCOUNT ON;  
     
	DECLARE @v_TCBDevChildId INT = 0; 
	DECLARE @v_TableName VARCHAR(200); 
	DECLARE @v_ColumnName VARCHAR(200); 
	DECLARE @v_TargetTableName VARCHAR(200);
	DECLARE @v_TargetColumnName VARCHAR(200); 
	DECLARE @v_MatchCondition VARCHAR(300);  
	DECLARE @v_NewNo INT = 0;

	DECLARE cur_TestCase CURSOR FOR
	SELECT TestcaseBuildDevChildId, TableName, ColumnName, TargetTableName, TargetColumnName, MatchCondition FROM TestCaseBuildDevChild 
	WHERE TestCaseBuildDevId = (SELECT TOP 1 TestCaseBuildDevId FROM TestcaseBuildDev WHERE TestCaseBuildId = @tcBuildId);
	 
	CREATE TABLE TempSourceTable (TempSourceValue VARCHAR(200) NULL); 
	CREATE TABLE TempTargetTable (TempTargetValue VARCHAR(200) NULL); 		 
	CREATE TABLE TempConditionalTable (ConditionalValue VARCHAR(200) NULL); 

	SELECT @v_NewNo = IDENT_CURRENT('dbo.TestCaseResult');

	OPEN cur_TestCase	
		FETCH NEXT FROM cur_TestCase INTO @v_TCBDevChildId, @v_TableName, @v_ColumnName, @v_TargetTableName, @v_TargetColumnName, @v_MatchCondition;
			
		WHILE @@FETCH_STATUS = 0
		BEGIN 	 
			 
			DECLARE @v_rowNo INT = 0;
			DECLARE @v_SourceValue VARCHAR(200);     
				
			TRUNCATE TABLE TempSourceTable; 
			TRUNCATE TABLE TempTargetTable; 				

			DECLARE @srcsql_text nvarchar(1000)
			SET @srcsql_text = CONCAT('INSERT INTO TempSourceTable(TempSourceValue)SELECT ',@v_ColumnName,' FROM ',@v_TableName);  
			EXEC (@srcsql_text);

			DECLARE @trgsql_text nvarchar(1000)
			SET @trgsql_text = CONCAT('INSERT INTO TempTargetTable(TempTargetValue)SELECT ',@v_TargetColumnName,' FROM ',@v_TargetTableName);  
			EXEC (@trgsql_text);

			DECLARE cur_SourceTable CURSOR FOR SELECT TempSourceValue FROM TempSourceTable;
			OPEN cur_SourceTable;	 
				FETCH NEXT FROM cur_SourceTable INTO @v_SourceValue;

				WHILE @@FETCH_STATUS = 0
				BEGIN 	  
						TRUNCATE TABLE TempConditionalTable;

						DECLARE @consql_text nvarchar(2000)
						--SET @consql_text = CONCAT('INSERT INTO TempConditionalTable(ConditionalValue)SELECT ',@v_TableName,'.',@v_ColumnName,' FROM (SELECT ',@v_ColumnName,' FROM ',@v_TableName,' LIMIT ',@v_rowNo,',1) AS ',@v_TableName,' INNER JOIN (SELECT ',@v_TargetColumnName,' FROM ',@v_TargetTableName,' LIMIT ',@v_rowNo,',1) AS ',@v_TargetTableName,' ON ',@v_MatchCondition,'');  
						SET @consql_text = CONCAT('INSERT INTO TempConditionalTable(ConditionalValue)SELECT ',@v_TableName,'.',@v_ColumnName,' FROM (SELECT ',@v_ColumnName,' FROM (SELECT ROW_NUMBER() OVER (ORDER BY ',@v_ColumnName,' ASC) AS rownumber, ',@v_ColumnName,'  FROM ',@v_TableName,') AS x WHERE rownumber = ',@v_rowNo,') AS ',@v_TableName,' INNER JOIN (SELECT ',@v_TargetColumnName,' FROM (SELECT ROW_NUMBER() OVER (ORDER BY ',@v_TargetColumnName,' ASC) AS rownumber, ',@v_TargetColumnName,'  FROM ',@v_TargetTableName,') AS x WHERE rownumber = ',@v_rowNo,') AS ',@v_TargetTableName,' ON ',@v_MatchCondition,'');  
						EXEC (@consql_text);

						IF(SELECT COUNT(*) FROM TempConditionalTable) > 0 
						BEGIN
							INSERT INTO TestCaseResult(TestCaseResultNo,TestcaseBuildDevChildId,MatchCondition,SourceValue,TargetValue,MatchResult)
							VALUES(@v_NewNo,@v_TCBDevChildId,@v_MatchCondition,@v_SourceValue,(SELECT TempTargetValue FROM (SELECT ROW_NUMBER() OVER (ORDER BY TempTargetValue ASC) AS rownumber, TempTargetValue  FROM TempTargetTable) AS x WHERE rownumber = @v_rowNo),'Success');
						END
 						ELSE 
						BEGIN
							INSERT INTO TestCaseResult(TestCaseResultNo,TestcaseBuildDevChildId,MatchCondition,SourceValue,TargetValue,MatchResult)
							VALUES(@v_NewNo,@v_TCBDevChildId,@v_MatchCondition,@v_SourceValue,(SELECT TempTargetValue FROM (SELECT ROW_NUMBER() OVER (ORDER BY TempTargetValue ASC) AS rownumber, TempTargetValue  FROM TempTargetTable) AS x WHERE rownumber = @v_rowNo),'Failed');
							--VALUES(@v_NewNo,@v_TCBDevChildId,@v_MatchCondition,@v_SourceValue,(SELECT TempTargetValue FROM TempTargetTable LIMIT v_rowNo,1),'Failed');
							 
						END				
						SET @v_rowNo = @v_rowNo +1;		
						
						FETCH NEXT FROM cur_SourceTable INTO @v_SourceValue;
				END
			CLOSE cur_SourceTable;  
			DEALLOCATE  cur_SourceTable; 

			FETCH NEXT FROM cur_TestCase INTO @v_TCBDevChildId, @v_TableName, @v_ColumnName, @v_TargetTableName, @v_TargetColumnName, @v_MatchCondition;
		END	 
	CLOSE cur_TestCase; 
	DEALLOCATE  cur_TestCase; 

	DROP TABLE IF EXISTS TempConditionalTable;
	DROP TABLE IF EXISTS TempSourceTable;
	DROP TABLE IF EXISTS TempTargetTable;

END
GO


