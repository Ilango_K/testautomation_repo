﻿$(document).ready(function () {
    $(".menuitem li a.active").removeClass("active");
    $("#master-menu a").attr("aria-expanded", "true");
    $("#master-menu ul").addClass('in');
    $(".menuitem a#aTestCaseBuildDev").addClass('active');

    //To bind the module dropdown based on the project selection
    $(document).on("change", "#ProjectId,#CloneFromProjectId", function () {
        var control = $(this);
        if ($(control).val() > 0) {
            $.ajax({
                type: 'post',
                url: "../TestCaseBuildDev/GetModulesByProjectId",
                data: {
                    projectId: $(control).val()
                },
                async: false,
                success: function (data, item) {
                    if ($(control).attr("id") == "ProjectId") {
                        $('input[name=' + $(control).attr("id") + '').val($(control).val());
                        $("#ModuleId").empty();
                        $("#ModuleId").append('<option value="">Select Module</option>');
                        $("#select2-ModuleId-container").text("Select Module");
                        $.each(data.data, function (key, value) {
                            $("#ModuleId").append('<option value="' + key + '">' + value + '</option>');
                        });
                        $("#TestCaseBuildId").empty();
                        $("#TestCaseBuildId").append('<option value="">Select TestCase Build</option>');
                        $("#TestCaseBuildId").val('');
                        $("#select2-TestCaseBuildId-container").text("Select TestCase Build");
                    }
                    else if ($(control).attr("id") == "CloneFromProjectId") {
                        $("#CloneFromModuleId").empty();
                        $("#CloneFromModuleId").append('<option value="">Select Module</option>');
                        $("#select2-CloneFromModuleId-container").text("Select Module");
                        $.each(data.data, function (key, value) {
                            $("#CloneFromModuleId").append('<option value="' + key + '">' + value + '</option>');
                        });
                        $("#CloneFromTestCaseBuildId").empty();
                        $("#CloneFromTestCaseBuildId").append('<option value="">Select TestCase Build</option>');
                        $("#CloneFromTestCaseBuildId").val('');
                        $("#select2-CloneFromTestCaseBuildId-container").text("Select TestCase Build");
                    }
                },
                cache: false,
                error: function (e) {
                    if (e.status == 401) {
                        window.location.href = "../Login/UserLogin";
                    } else {
                        alert(e.responseText);
                    }
                }
            });
        }
        else {
            if ($(control).attr("id") == "ProjectId") {
                $("#ModuleId").empty();
                $("#ModuleId").append('<option value="">Select Module</option>');
                $("#ModuleId").val('');
                $("#select2-ModuleId-container").text("Select Module");
                $("#TestCaseBuildId").empty();
                $("#TestCaseBuildId").append('<option value="">Select TestCase Build</option>');
                $("#TestCaseBuildId").val('');
                $("#select2-TestCaseBuildId-container").text("Select TestCase Build");
            }
            else if ($(control).attr("id") == "CloneFromProjectId") {
                $("#CloneFromModuleId").empty();
                $("#CloneFromModuleId").append('<option value="">Select Module</option>');
                $("#CloneFromModuleId").val('');
                $("#select2-CloneFromModuleId-container").text("Select Module");
                $("#CloneFromTestCaseBuildId").empty();
                $("#CloneFromTestCaseBuildId").append('<option value="">Select TestCase Build</option>');
                $("#CloneFromTestCaseBuildId").val('');
                $("#select2-CloneFromTestCaseBuildId-container").text("Select TestCase Build");
            }
        }
    });

    //To bind the test case build dropdown based on the module selection
    $(document).on("change", "#ModuleId,#CloneFromModuleId", function () {
        var control = $(this);

        var projId = 0;
        if ($(control).attr("id") == "ModuleId") {
            projId = $("#ProjectId").val();
        }
        else if ($(control).attr("id") == "CloneFromModuleId") {
            projId = $("#CloneFromProjectId").val();
        }

        if ($(control).val() > 0) {
            $.ajax({
                type: 'post',
                url: "../TestCaseBuildDev/GetTestCaseBuildByFilter",
                data: {
                    projectId: projId,
                    moduleId: $(control).val(),
                },
                async: false,
                success: function (data, item) {
                    if ($(control).attr("id") == "ModuleId") {
                        $('input[name=' + $(control).attr("id") + '').val($(control).val());
                        $("#TestCaseBuildId").empty();
                        $("#TestCaseBuildId").append('<option value="">Select TestCase Build</option>');
                        $("#select2-TestCaseBuildId-container").text("Select TestCase Build");
                        $.each(data.data, function (key, value) {
                            $("#TestCaseBuildId").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                    else if ($(control).attr("id") == "CloneFromModuleId") {
                        $("#CloneFromTestCaseBuildId").empty();
                        $("#CloneFromTestCaseBuildId").append('<option value="">Select TestCase Build</option>');
                        $("#select2-CloneFromTestCaseBuildId-container").text("Select TestCase Build");
                        $.each(data.data, function (key, value) {
                            $("#CloneFromTestCaseBuildId").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                },
                cache: false,
                error: function (e) {
                    if (e.status == 401) {
                        window.location.href = "../Login/UserLogin";
                    } else {
                        alert(e.responseText);
                    }
                }
            });
        }
        else {
            if ($(control).attr("id") == "ModuleId") {
                $("#TestCaseBuildId").empty();
                $("#TestCaseBuildId").append('<option value="">Select TestCase Build</option>');
                $("#TestCaseBuildId").val('');
                $("#select2-TestCaseBuildId-container").text("Select TestCase Build");
            }
            else if ($(control).attr("id") == "CloneFromModuleId") {
                $("#CloneFromTestCaseBuildId").empty();
                $("#CloneFromTestCaseBuildId").append('<option value="">Select TestCase Build</option>');
                $("#CloneFromTestCaseBuildId").val('');
                $("#select2-CloneFromTestCaseBuildId-container").text("Select TestCase Build");
            }
        }
    });

    //To bind the hidden table dropdown based on the project & module selection
    $(document).on("change", "#ProjectId,#ModuleId", function () {
        if ($("#ProjectId").val() != '' && $("#ModuleId").val() != '') {
            if ($("#hdnCloneBind").val() == 0) {
                $.ajax({
                    type: 'post',
                    url: "../TestCaseBuildDev/GetTables",
                    data: {
                        projectId: $("#ProjectId").val(),
                        moduleId: $("#ModuleId").val()
                    },
                    async: false,
                    success: function (data, item) {
                        $(".drpTable").empty();
                        $(".drpTable").append('<option value="">Select</option>');
                        $.each(data.data, function (key, value) {
                            $(".drpTable").append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    cache: false,
                    error: function (e) {
                        if (e.status == 401) {
                            window.location.href = "../Login/UserLogin";
                        } else {
                            alert(e.responseText);
                        }
                    }
                });
            }
            else {
                $("#hdnCloneBind").val(0);
            }
        }
        else {
            $(".drpTable").empty();
            $(".drpTable").append('<option value="">Select</option>');
            $(".drpTable").val('');
        }
    });

    //To bind the column dropdown based on the table selection
    $(document).on("change", ".drpTable", function () {
        var control = $(this);
        if ($(control).val() != '') {
            $.ajax({
                type: 'post',
                url: "../TestCaseBuildDev/GetColumnsByTable",
                data: {
                    tableName: $(control).val()
                },
                async: false,
                success: function (data, item) {
                    $(control).closest('td').next("td").find(".drpColumn").empty();
                    $(control).closest('td').next("td").find(".drpColumn").append('<option value="">Select</option>');
                    $.each(data.data, function (key, value) {
                        $(control).closest('td').next("td").find(".drpColumn").append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                cache: false,
                error: function (e) {
                    if (e.status == 401) {
                        window.location.href = "../Login/UserLogin";
                    } else {
                        alert(e.responseText);
                    }
                }
            });
        }
        else {
            $(control).closest('td').next("td").find(".drpColumn").empty();
            $(control).closest('td').next("td").find(".drpColumn").append('<option value="">Select</option>');
            $(control).closest('td').next("td").find(".drpColumn").val('');
        }
    });

    //To remove the required class on change event
    $(document).on("change", ".inputreq", function () {
        if ($(this).val()) {
            if ($(this).hasClass("inputreqerror")) {
                $(this).removeClass("inputreqerror");
            }
        }
    });

    $(".testCaseTable").on('click', '.exprpop', function (e) {
        e.preventDefault();
        $('#exprModal').data('id', $(this).data('id'));
        var control = $(this);
        var empty = false;
        var select = $(control).parents("tr").find('select');
        select.each(function () {
            if (!$(this).val()) {
                $(this).addClass("inputreqerror");
                empty = true;
            } else {
                $(this).removeClass("inputreqerror");
            }
        });
        if (!empty) {

            //var srcField = "[" + $(control).parents('tr').find('.drpTable option:selected').html() + "].[" + $(control).parents('tr').find('.drpColumn option:selected').html() + "]";
            //var tarField = "[" + $(control).parents('tr').find('.drpTargetTable option:selected').html() + "].[" + $(control).parents('tr').find('.drpTargetColumn option:selected').html() + "]";

            var srcField = "" + $(control).parents('tr').find('.drpTable option:selected').html() + "." + $(control).parents('tr').find('.drpColumn option:selected').html() + "";
            var tarField = "" + $(control).parents('tr').find('.drpTargetTable option:selected').html() + "." + $(control).parents('tr').find('.drpTargetColumn option:selected').html() + "";


            $("#divSrc a").html(srcField);
            $("#divTar a").html(tarField);

        }
    });

    $(document).on('click', '.linkBtn', function (e) {
        var expr = $("#txtExpr").val() + $(this).html();
        $("#txtExpr").val(expr);
    });

    $('#exprModal').on('hidden.bs.modal', function (e) {
        $(this).find("#txtExpr").val("");
    });

    $('.btn-deleteok').on('click', function (e) {
        // $("#txtValue").val($("#txtExpr").val());
        $("#divTestCaseTable table tbody tr").eq(parseInt($('#exprModal').data('id')) - 1).find("#txtValue").val($("#txtExpr").val());
        $("#txtExpr").val('');
        $('#exprModal').modal('hide');
    });

    // Add sub type row on add button click
    $(document).on("keypress click", ".addTestCaseRow", function (evt) {
        var control = $(this);
        var empty = false;

        var input = $(control).parents("tr").find('input[type="text"]');
        input.each(function () {
            if ((!$(this).val() || $(this).val() == 0) && $(this).hasClass("inputreq")) {
                $(this).addClass("inputreqerror");
                empty = true;
            } else {
                $(this).removeClass("inputreqerror");
            }
        });

        var select = $(control).parents("tr").find('select');
        select.each(function () {
            if (!$(this).val()) {
                $(this).addClass("inputreqerror");
                empty = true;
            } else {
                $(this).removeClass("inputreqerror");
            }
        });
        if (!empty) {
            var typeSelect = $("#divTypeSelect").html();

            var tableSelect = "<select class='form-control pro-edt-select form-control-primary drpTable inputreq' id='HdnTableId' name='HdnTableId' style='cursor:pointer'><option value=''>Select</option>";
            var tarTableSelect = "<select class='form-control pro-edt-select form-control-primary drpTable drpTargetTable inputreq' id='HdnTableId' name='HdnTableId' style='cursor:pointer'><option value=''>Select</option>";
            $.ajax({
                type: 'post',
                url: "../TestCaseBuildDev/GetTables",
                data: {
                    projectId: $("#ProjectId").val(),
                    moduleId: $("#ModuleId").val()
                },
                async: false,
                success: function (data, item) {
                    $.each(data.data, function (key, value) {
                        tableSelect = tableSelect + "<option value='" + key + "'>" + value + "</option>";
                        tarTableSelect = tarTableSelect + "<option value='" + key + "'>" + value + "</option>";
                    });
                },
                cache: false,
                error: function (e) {
                    if (e.status == 401) {
                        window.location.href = "../Login/UserLogin";
                    } else {
                        alert(e.responseText);
                    }
                }
            });
            tableSelect = tableSelect + "</select>";
            tarTableSelect = tarTableSelect + "</select>";

            var rowIndex = $("#divTestCaseTable table tbody tr:last-child").index();

            var row = '<tr>' +
                '<td class="text-right childtd tdwidth20"><i class="fa fa-minus-circle text-danger removeTestCaseRow" aria-hidden="true" style="font-size: x-large;cursor: pointer;"></i></td>' +
                '<td class="childtd tdwidth110"><div class="review-content-section"><div class="mg-b-pro-edt" style="text-align:center;">' + parseInt(rowIndex + 2) + '</div></div></td>' +
                '<td class="childtd tdwidth110"><div class="review-content-section"><div class="mg-b-pro-edt">' + typeSelect + '</div></div></td>' +
                '<td class="childtd tdwidth80"><div class="review-content-section"><div class="mg-b-pro-edt">' + tableSelect + '</div></div></td>' +
                '<td class="childtd tdwidth80"><div class="review-content-section"><div class="mg-b-pro-edt"><select class="form-control pro-edt-select form-control-primary drpColumn inputreq" id="ColumnId" name="ColumnId" style="cursor:pointer;" tabindex="4"><option value="">Select</option></select></div></div></td>' +
                '<td class="childtd tdwidth80"><div class="review-content-section"><div class="mg-b-pro-edt">' + tarTableSelect + '</div></div></td>' +
                '<td class="childtd tdwidth80"><div class="review-content-section"><div class="mg-b-pro-edt"><select class="form-control pro-edt-select form-control-primary drpColumn drpTargetColumn inputreq" id="TargetColumnId" name="TargetColumnId" style="cursor:pointer;" tabindex="4"><option value="">Select</option></select></div></div></td>' +
                '<td class="childtd tdwidth80" style="vertical-align: baseline;"><div class="review-content-section"><div class="mg-b-pro-edt">' +
                        '<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" style="padding:0;"><input type="text" id="txtValue" class="form-control txtValue inputreq" placeholder="Value/ Match" title="Value/ Match"></div>' +
                        '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="padding:0;text-align:right;"><a href="#" data-id="' + parseInt(rowIndex + 2) + '" data-toggle="modal" title="Formula Builder" class="pd-setting-ed mg-t exprpop" data-target="#exprModal"><i aria-hidden="true">...</i></a></div>' +
                '</div></div></td>' +
                '<td class="childtd tdwidth20"><i class="fa fa-plus-circle text-success addTestCaseRow" aria-hidden="true" style="font-size: x-large; cursor: pointer"></i><input type="hidden" id="hdnBOQSubTypeId" value="0" /></td>' +
                '</tr>';

            $(control).parents("table.testCaseTable>tbody").append(row);
        }
    });

    // Delete sub type row on delete button click
    $(document).on("keypress click", ".removeTestCaseRow", function (evt) {
        var control = $(this);
        $(control).parents("table.testCaseTable>tbody>tr").remove();
    });

    //Button submit action - To save the child table records into the hidden string
    $(document).on("click", "#btnSubmit", function () {
        var testCaseList = new Array();
        $("#divTestCaseTable table tbody tr").each(function (i) {
            var tds = $(this).find("td:visible");
            if ($(tds[7]).find(".txtValue").val() != '') {
                var testCase = { TestCaseTypeId: $(tds[2]).find(".drpType").val(), TableName: $(tds[3]).find(".drpTable option:selected").html(), ColumnName: $(tds[4]).find(".drpColumn option:selected").html(), TargetTableName: $(tds[5]).find(".drpTargetTable option:selected").html(), TargetColumnName: $(tds[6]).find(".drpTargetColumn option:selected").html(), MatchCondition: $(tds[7]).find(".txtValue").val() };
                testCaseList.push(testCase);
            }
        });
        $('#strChildItemRefList').val(JSON.stringify(testCaseList));

        if (testCaseList.length == 0) {
            alert("Please select atleast one entry for test case.");
            return false;
        }
    });

    $(document).on("click", "#btnClone", function () {
        if ($("#CloneFromProjectId").val() == '') {
            alert("Please select the project for clone..");
            return false;
        }
        if ($("#CloneFromModuleId").val() == '') {
            alert("Please select the module for clone..");
            return false;
        }
        if ($("#CloneFromTestCaseBuildId").val() == '') {
            alert("Please select the test case build for clone..");
            return false;
        }

        if ($("#CloneFromProjectId").val() != '' && $("#CloneFromModuleId").val() != '' && $("#CloneFromTestCaseBuildId").val() != '') {
            $.ajax({
                type: 'post',
                url: "../TestCaseBuildDev/GetDetailsForClone",
                data: {
                    projectId: $("#CloneFromProjectId").val(),
                    moduleId: $("#CloneFromModuleId").val(),
                    testCaseId: $("#CloneFromTestCaseBuildId").val()
                },
                async: false,
                success: function (data) {
                    $("#hdnCloneBind").val(1);
                    $('#ProjectId').val($("#CloneFromProjectId").val()).trigger('change');
                    $('#ProjectId').select2({ disabled: 'readonly' });
                    $("#ModuleId").val($("#CloneFromModuleId").val()).trigger('change');
                    $('#ModuleId').select2({ disabled: 'readonly' });
                    $('#divTestCaseTable table tbody').html(data);
                },
                cache: false,
                error: function (e) {
                    if (e.status == 401) {
                        window.location.href = "../Login/UserLogin";
                    } else {
                        alert(e.responseText);
                    }
                }
            });
        }

    });
});