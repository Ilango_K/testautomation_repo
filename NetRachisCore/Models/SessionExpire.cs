﻿using Microsoft.AspNetCore.Http;
using NetRachisCore.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NetRachisCore.Models
{
    public class SessionExpire : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var sessionHelper = new SessionHelper(new HttpContextAccessor());
            if (sessionHelper.SessionLogOnInfo().UserName == null || sessionHelper.SessionLogOnInfo().UserName == "")
            {
                CommonHelper.SetCookieInfo(false, string.Empty, string.Empty);
                //FormsAuthentication.SignOut();
                //filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                                                                    //{
                                                                    //    { "action", "UserLogin" },
                                                                    //    { "controller", "Login" },
                                                                    //    { "returnUrl", filterContext.HttpContext.Request.RawUrl}
                                                                    // });

                return;
            }
        }
    }
}
