﻿
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace NetRachisCore.Models.BusinessObjects
{
    public class ProcessExecution
    {
        public int ProcessExecutionId { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public SelectList ProjectList { get; set; }
        public SelectList ModuleList { get; set; }

        public List<ProcessExecutionChild> ProcessExecutionChildRefList { get; set; }
        public string strProcessExecutionChildRefList { get; set; }
        public SelectList TestCaseBuildList { get; set; }
        public SelectList CommandList { get; set; }
    }
    public class ProcessExecutionChild
    {
        public int ProcessExecutionChildId { get; set; }
        public int ProcessExecutionId { get; set; }
        public int TestCaseBuildId { get; set; }
        public string Command { get; set; }
        public string ProcessStatus { get; set; }
        public SelectList TestCaseBuildList { get; set; }
        public SelectList CommandList { get; set; }

    }
}