﻿
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace NetRachisCore.Models.BusinessObjects
{
    public class XmlComparison
    {
        public int XmlComparisonId { get; set; }
        public string BatchNumber { get; set; }
        public string SourceFilePath { get; set; }
        public string DestinationFilePath { get; set; }
    }
}