﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetRachisCore.Models.BusinessObjects
{
    public class User
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string EmailId { get; set; }
        public bool RememberMe { get; set; }
        public string ConfirmPassword { get; set; }
        public bool ActiveStatus { get; set; }
        public List<Access> MenuAccessList { get; set; }
        public List<Access> FormAccessList { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public SelectList Roles { get; set; }
    }

    public class ChangePasswordModel
    {
        public string EmailId { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }

    public class Access
    {
        public string Name { get; set; }
        public string IsWriteAccess { get; set; }
    }
}
