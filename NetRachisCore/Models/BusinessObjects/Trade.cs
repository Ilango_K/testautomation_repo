﻿
using Microsoft.AspNetCore.Mvc.Rendering;

namespace NetRachisCore.Models.BusinessObjects
{
    public class Trade
    {
        public int TradeId { get; set; }
        public string TickNumber { get; set; }
        public string TradeDate { get; set; }
        public string SettleDate { get; set; }
        public string BuySell { get; set; }
        public string ExternSymbol { get; set; }
        public string Ticker { get; set; }
        public string SecType { get; set; }
        public string ClientCode { get; set; }
        public string Trader { get; set; }
        public string ExchangeGroup { get; set; }
        public string Broker { get; set; }
        public decimal AvgPrice { get; set; }
        public string PriceCurrency { get; set; }
        public string SettleCountryCode { get; set; }
        public decimal Shares { get; set; }
        public decimal NetCost { get; set; }
        public decimal SecFees { get; set; }
        public decimal Tax { get; set; }
        public decimal OtherCharges { get; set; }
        public decimal Commission { get; set; }
        public decimal AccruedInterest { get; set; }
        public string ElectronicallyMatched { get; set; }
        public string MatureDate { get; set; }
        public decimal AvgYield { get; set; }
        public string Strategy { get; set; }
        public decimal CouponRate { get; set; }
        public int TradeUploadID { get; set; }

        public int TradeTypeId { get; set; }
        public SelectList TradeTypeList { get; set; }
    }
}