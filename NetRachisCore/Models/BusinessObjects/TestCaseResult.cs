﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetRachisCore.Models.BusinessObjects
{
    public class TestCaseResult
    {
        public int TestCaseResultId { get; set; }
        public string TestCaseResultNo { get; set; }
        public int TestcaseBuildDevChildId { get; set; }
        public string MatchCondition { get; set; }
        public string SourceValue { get; set; }
        public string TargetValue { get; set; }
        public string MatchResult { get; set; }

        public string TestCaseBuildName { get; set; }
        public string TestCaseType { get; set; }
    }
}
