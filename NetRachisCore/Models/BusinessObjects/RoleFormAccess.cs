﻿
using Microsoft.AspNetCore.Mvc.Rendering;

namespace NetRachisCore.Models.BusinessObjects
{
    public class RoleFormAccess
    {
        public int RoleFormAccessId { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public SelectList Roles { get; set; }
        public bool ChkWriteAccess { get; set; }
        public string IsWriteAccess { get; set; }
        public string[] SelectedMenus { get; set; }
        public string[] SelectedForms { get; set; }
        public string FormNames { get; set; }
        public string MenuNames { get; set; }
        public MultiSelectList Menus { get; set; }
        public MultiSelectList Forms { get; set; }
    }
}