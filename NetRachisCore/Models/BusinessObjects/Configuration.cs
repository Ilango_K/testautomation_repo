﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace NetRachisCore.Models.BusinessObjects
{
    public class Configuration
    {
        public int ConfigurationId { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public SelectList ProjectList { get; set; }
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public SelectList ModuleList { get; set; }
        public int TestCaseBuildId { get; set; }
        public string TestCaseBuildName { get; set; }
        public SelectList TestCaseBuildList { get; set; }
        public int InputFileTypeId { get; set; }
        public string InputFileTypeName { get; set; }
        public SelectList InputFileTypeList { get; set; }
        public string InputFilePath { get; set; }
        public string InputFileFormat { get; set; }
        public bool InputFileSet { get; set; }
        public int OutputFileTypeId { get; set; }
        public string OutputFileTypeName { get; set; }
        public SelectList OutputFileTypeList { get; set; }
        public string OutputFilePath { get; set; }
        public string OutputFileFormat { get; set; }
        public bool OutputFileSet { get; set; }
        public DateTime SchedulerInputStartDate { get; set; }
        public string SchedulerInputStartTime { get; set; }
        public DateTime SchedulerInputEndDate { get; set; }
        public string SchedulerInputEndTime { get; set; }
        public string SchedulerInputFrequency { get; set; }
        public bool SchedulerInputSet { get; set; }
        public DateTime SchedulerOutputStartDate { get; set; }
        public string SchedulerOutputStartTime { get; set; }
        public DateTime SchedulerOutputEndDate { get; set; }
        public string SchedulerOutputEndTime { get; set; }
        public string SchedulerOutputFrequency { get; set; }
        public bool SchedulerOutputSet { get; set; }
        //public int TableTypeId { get; set; }
        //public string TableTypeName { get; set; }
        public SelectList TableTypeList { get; set; }
        public SelectList FileTypeList { get; set; }

        public List<InputOutputFile> InputFileList { get; set; }
        public string strInputFileList { get; set; }
        public List<InputOutputFile> OutputFileList { get; set; }
        public string strOutputFileList { get; set; }
        public List<InputOutputTable> InputTableList { get; set; }
        public string strInputTableList { get; set; }
        public List<InputOutputTable> OutputTableList { get; set; }
        public string strOutputTableList { get; set; }
    }

    public class InputOutputFile
    {
        public int ConfigurationFileId { get; set; }
        public int ConfigurationId { get; set; }
        public int FileTypeId { get; set; }
        public string FilePath { get; set; }
        public string FileNameFormat { get; set; }
        public bool Set { get; set; }
        public bool IsInput { get; set; }
        public SelectList FileTypeList { get; set; }
    }
    public class InputOutputTable
    {
        public int ConfigurationTableId { get; set; }
        public int ConfigurationId { get; set; }
        public int TableTypeId { get; set; }
        public string TableName { get; set; }
        public bool IsInput { get; set; }
        public SelectList IOTableTypeList { get; set; }
    }
}