﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace NetRachisCore.Models.BusinessObjects
{
    public class FileTypeConfiguration
    {
        public int FileTypeConfId { get; set; }
        public int ProjectId { get; set; }
        public int ModuleId { get; set; }
        public string GeneratedFileTypeId { get; set; }
        public string GeneratedFileTypeName { get; set; }
        public int FileExtnTypeId { get; set; }
        public int FileTypeId { get; set; }
        public int TableTypeId { get; set; }
        public string TableName { get; set; }
        public bool IsSet { get; set; }
        public string DataRowStart { get; set; }

        public string ProjectName { get; set; }
        public SelectList ProjectList { get; set; }
        public string ModuleName { get; set; }
        public SelectList ModuleList { get; set; }
        public string FileExtnTypeName { get; set; }
        public SelectList FileExtnTypeList { get; set; }
        public string FileTypeName { get; set; }
        public SelectList FileTypeList { get; set; }
        public string TableTypeName { get; set; }
        public SelectList TableTypeList { get; set; }

        public SelectList DataTypeList { get; set; }

        public List<FileColumn> FileColumnList { get; set; }
        public string strFileColumnList { get; set; }
        public List<TagColumn> TagColumnList { get; set; }
        public string strTagColumnList { get; set; }
    }

    public class FileColumn
    {
        public int FileTypeConfId { get; set; }
        public string ColumnName { get; set; }
        public int DataTypeId { get; set; }
        public string DataType { get; set; }
        public string ColumnNo { get; set; }
        public SelectList DataTypeList { get; set; }
    }
    public class TagColumn
    {
        public int FileTypeConfId { get; set; }
        public string Tag { get; set; }
        public int DataTypeId { get; set; }
        public string DataType { get; set; }
        public SelectList DataTypeList { get; set; }

    }
}