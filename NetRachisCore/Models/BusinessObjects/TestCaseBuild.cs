﻿
using Microsoft.AspNetCore.Mvc.Rendering;

namespace NetRachisCore.Models.BusinessObjects
{
    public class TestCaseBuild
    {
        public int TestCaseBuildId { get; set; }
        public string TestCaseBuildName { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public string Description { get; set; }
        public SelectList ProjectList { get; set; }
        public SelectList ModuleList { get; set; }
    }
}