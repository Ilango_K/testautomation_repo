﻿namespace NetRachisCore.Models.BusinessObjects
{
    public class FileTypes
    {
        public int FileTypeId { get; set; }
        public string FileTypeName { get; set; }
        public string Description { get; set; }
    }
}