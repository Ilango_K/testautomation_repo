﻿
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace NetRachisCore.Models.BusinessObjects
{
    public class DocumentUpload
    {
        public int UploadId { get; set; }
        public string BatchNumber { get; set; }
        public string FilePath { get; set; }
        public List<Trade> TradeList { get; set; }
    }
}