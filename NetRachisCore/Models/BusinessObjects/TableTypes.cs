﻿
namespace NetRachisCore.Models.BusinessObjects
{
    public class TableTypes
    {
        public int TableTypeId { get; set; }
        public string TableTypeName { get; set; }
        public string Description { get; set; }
    }
}