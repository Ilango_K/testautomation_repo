﻿
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace NetRachisCore.Models.BusinessObjects
{
    public class TestCaseBuildDev
    {
        public int TestCaseBuildDevId { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public int TestCaseBuildId { get; set; }
        public string TestCaseBuildName { get; set; }
        public SelectList ProjectList { get; set; }
        public SelectList ModuleList { get; set; }
        public SelectList TestCaseBuildList { get; set; }

        public List<TestCaseBuildDevChild> ChildItemRefList { get; set; }
        public string strChildItemRefList { get; set; }
        public SelectList TestCaseTypeList { get; set; }

        public int? CloneFromProjectId { get; set; }
        public int? CloneFromModuleId { get; set; }
        public int? CloneFromTestCaseBuildId { get; set; }

        public SelectList TableList { get; set; }
        public SelectList ColumnList { get; set; }
    }
    public class TestCaseBuildDevChild
    {
        public int TestCaseBuildDevChildId { get; set; }
        public int TestCaseBuildDevId { get; set; }
        public int TestCaseTypeId { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string TargetTableName { get; set; }
        public string TargetColumnName { get; set; }
        public string MatchCondition { get; set; }
        public SelectList TestCaseTypeList { get; set; }
        public SelectList TableList { get; set; }
        public SelectList ColumnList { get; set; }
        public SelectList TargetTableList { get; set; }
        public SelectList TargetColumnList { get; set; }

    }
}