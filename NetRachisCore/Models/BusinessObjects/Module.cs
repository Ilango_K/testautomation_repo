﻿
using Microsoft.AspNetCore.Mvc.Rendering;

namespace NetRachisCore.Models.BusinessObjects
{
    public class Module
    {
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public SelectList ProjectList { get; set; }
    }
}