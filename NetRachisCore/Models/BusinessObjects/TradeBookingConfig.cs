﻿
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace NetRachisCore.Models.BusinessObjects
{
    public class TradeBookingConfig
    {
        public int TradeBookingConfigId { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public SelectList ProjectList { get; set; }
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public SelectList ModuleList { get; set; }
        public int TestCaseBuildId { get; set; }
        public string TestCaseBuildName { get; set; }
        public SelectList TestCaseBuildList { get; set; }
        public DateTime SchedulerDate { get; set; }
        public string SchedulerTime { get; set; }
        public string SchedulerFrequency { get; set; }
        public bool SchedulerSet { get; set; }
        public string TradeInputPath { get; set; }
        public string FileNameFormat { get; set; }
        public int TargetSystemId { get; set; }
        public SelectList TargetSystemList { get; set; }
        public bool TradeInputSet { get; set; }
        public int CommandTypeId { get; set; }
        public SelectList CommandTypeList { get; set; }
        public string CommandLine { get; set; }
        public string AdditionalInfo { get; set; }
        public string TargetPath { get; set; }
        public bool CommandSet { get; set; }
    }
    
}