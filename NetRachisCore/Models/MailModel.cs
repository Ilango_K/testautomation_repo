﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using NetRachisCore.Helpers;
using Microsoft.AspNetCore.Http;

namespace NetRachisCore.Models
{
    public class EmailSettings
    {
        public string EmailId { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public bool UseSSL { get; set; }
    }
    public class MailRequest
    {       
        public string EmailTo { get; set; } 
        public string EmailFrom { get; set; } 
        public string EmailSubject { get; set; } 
        public string ViewName { get; set; }
        [System.Web.Mvc.AllowHtml]
        public string Body { get; set; }
        public string EmailToName { get; set; }
        public int? Id { get; set; }
        public string UrlPath { get; set; }
        public string Value { get; set; }
        public List<IFormFile> Attachments { get; set; }
    }
}
